<?php

return [
    '0'     => 'Outcast',
    '1'     => 'Novice',
    '10'    => 'Keeper',
    '100'   => 'Hobbyist',
    '500'   => 'Enthusiast',
    '1000'  => 'Professional',
    '5000'  => 'Expert',
    '10000' => 'Industry Leader'
];