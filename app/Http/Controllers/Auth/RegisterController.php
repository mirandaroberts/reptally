<?php

namespace App\Http\Controllers\Auth;

use App\Services\User\UserCodeService;
use Flash;
use App\Http\Controllers\Controller;
use App\Notifications\VerifyEmail;
use App\Services\User\UserService;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * @var UserService
     */
    protected $service;

    /**
     * RegisterController constructor.
     */
    public function __construct(UserService $service)
    {
        parent::__construct();

        $this->middleware('guest')->except('verify', 'resendVerification');
        $this->service = $service;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|string|max:255',
            'last_name'  => 'required|string|max:255',
            'email'      => 'required|string|email|max:255|unique:users',
            'password'   => 'required|string|min:6|confirmed',
            'agree'      => 'required'
        ], [
            'agree.required'      => 'You must agree to the Terms of Service to register.',
            'first_name.required' => 'Your first name is required.',
            'last_name.required'  => 'Your last name is required.',
            'email.unique'        => 'There is already an account registered with this email.'
        ]);
    }

    /**
     * Creates new user
     * @param array $data
     * @return mixed
     */
    protected function create(array $data)
    {
        return $this->service->create($data);
    }

    /**
     * Show registration forum
     * @param UserCodeService $codeService
     * @param null $code
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showRegistrationForm(UserCodeService $codeService, $code = null)
    {
        $codeData = $codeService->getByCode($code);
        $codeData ? $invite = $codeData->code : $invite = null;
        return view('app.auth.register', [
            'invite'    => $invite,
            'wallpaper' => $this->randomWallpaper()
        ]);
    }

    /**
     * Handle a registration request for the application.
     * @param UserCodeService $codeService
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(UserCodeService $codeService, Request $request)
    {
        // Validate code first
        if (!$code = $codeService->getByCode($request->code)) Flash::error('Invalid invite code.');
        else if ($code->email != $request->email || !$code->isValid) Flash::error('Invalid invite code.');
        else {
            $this->validator($request->all())->validate();
            event(new Registered($user = $this->create($request->all())));
            $codeService->invalidate($code);
            $this->guard()->login($user);
            return $this->registered($request, $user)
                ?: redirect($this->redirectPath());
        } return redirect('/register');
    }

    /**
     * Run any post-registration scripts
     * @param Request $request
     * @param $user
     */
    public function registered(Request $request, $user)
    {
        Flash::success('Thank you for registering! A verification link has been sent to your email.');
        $user->notify(new VerifyEmail($user));
    }

    /**
     * Verifies a user's email
     * @param $id
     * @param $token
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function verify($id, $token)
    {
        $user = $this->service->get($id);
        if ($user->settings->activation_token == $token) {
            if ($this->service->updateSettings($user, ['activation_token' => null])) Flash::success('You have successfully activated your account.');
            else Flash::error('There was an error verifying your account');
        } else {
            Flash::error('We could not verify your account, we have resent an email to ' . $user->email);
            $user->notify(new VerifyEmail($user));
        }
        return redirect($this->redirectTo);
    }

    /**
     * Resends verification link
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function resendVerification()
    {
        if (!$user = Auth::user()) abort(404);
        $user->notify(new VerifyEmail($user));
        Flash::success('We have sent your verification link to ' . $user->email);
        return redirect('/admin');
    }
}
