<?php

namespace App\Http\Controllers;

use App\Services\DropdownService;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Storage;

abstract class BaseController extends Controller
{
    /**
     * BaseController constructor.
     */
	public function __construct() {

	}

    /**
     * Get a random wallpaper from s3 bucket
     * @return mixed
     */
	public function randomWallpaper()
    {
        if ($wallpapers = Storage::allFiles('wallpapers')) return Storage::url($wallpapers[array_rand($wallpapers)]);
        else return Storage::url('wallpapers/i-would-encourage-3169541_1920.jpg');
    }

    /**
     * Retrieve a dropdown for rendering
     * @param Request $request
     * @return mixed
     */
    public function postDropdown(Request $request)
    {
        $request->validate(['key' => 'required|string|max:20']);
        $dropdownService = new DropdownService();
        $request->params ? $params = $request->params : $params = array();
        return $dropdownService->getDropdowns($request->key, $params);
    }
}