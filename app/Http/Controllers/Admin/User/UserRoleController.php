<?php

namespace App\Http\Controllers\Admin\User;

use Flash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Admin\Controller;
use App\Services\User\UserRoleService;
use Illuminate\Http\Request;

class UserRoleController extends Controller
{
    /**
     * @var
     */
    protected $service;

    /**
     * UserRoleController constructor.
     * @param UserRoleService $service
     */
    public function __construct(UserRoleService $service)
    {
        parent::__construct();
        $this->service = $service;
    }

    /**
     * Validation
     * @param array $data
     * @param null $id
     * @return mixed
     */
    protected function validator(array $data, $id = null)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:100|unique:user_roles' . $id ? ',' . $id : '',
        ]);
    }

    /**
     * Show index page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $roles = $this->service->getAll();
        return view('dashboard.users.roles.index', compact('roles'));
    }

    /**
     * Creates a user role
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postCreate(Request $request)
    {
        if ($validator = $this->validator($request->only(['name']))->passes()) {
            if (!$role = $this->service->create($request->all())) return response()->json(['error' => 'Error creating user role.']);
            else response()->json(['success' => 'Created new user role.']);
        } else return response()->json(['error' => $validator->errors()->all()]);
    }

    /**
     * Updates a user role
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postUpdate(Request $request)
    {
        if (!$role = $this->service->get($request->id)) return response()->json(['error' => 'Could not find user role.']);
        if ($validator = $this->validator($request->only(['name'], $role->id))->passes()) {
            if (!$this->service->update($role, $request->all())) return response()->json(['error' => 'Error updating user role.']);
            else return response()->json(['success' => 'Updated user role.']);
        } else return response()->json(['error' => $validator->errors()->all()]);
    }

    /**
     * Deletes a user role
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postDelete(Request $request)
    {
        if (!$role = $this->service->get($request->id)) return response()->json(['error' => 'Could not find user role.']);
        if (!$this->service->delete($role)) return response()->json(['error' => 'Error deleting user role.']);
        else return response()->json(['success' => 'Deleted user role.']);
    }
}
