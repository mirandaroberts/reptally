<?php

namespace App\Http\Controllers\Admin\User;

use App\Services\User\UserCodeService;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Admin\Controller;
use Illuminate\Http\Request;
use Flash;

class UserCodeController extends Controller
{
    /**
     * @var
     */
    protected $service;

    /**
     * UserCodeController constructor.
     * @param UserCodeService $service
     */
    public function __construct(UserCodeService $service)
    {
        parent::__construct();
        $this->service = $service;
    }

    /**
     * Validation
     * @param array $data
     * @return mixed
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|email',
        ]);
    }

    /**
     * Show index page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.users.codes.index', [
            'codes' => $this->service->getAll()
        ]);
    }

    /**
     * Creates a user plan
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postCreate(Request $request)
    {
        if ($validator = $this->validator($request->only(['email']))->passes()) {
            if (!$code = $this->service->create($request->all())) return response()->json(['error' => 'Error creating invite code.']);
            else response()->json(['success' => 'An invite has been sent to ' . $code->email . '.']);
        } else return response()->json(['error' => $validator->errors()->all()]);
    }

    /**
     * Updates a user plan
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postUpdate(Request $request)
    {
        if (!$code = $this->service->get($request->email)) return response()->json(['error' => 'Could not find invite code.']);
        if ($validator = $this->validator($request->only(['email'], $code->email))->passes()) {
            if (!$this->service->update($code, $request->all())) return response()->json(['error' => 'Error updating invite code.']);
            else return response()->json(['success' => 'Updated invite code.']);
        } else return response()->json(['error' => $validator->errors()->all()]);
    }

    /**
     * Deletes a user plan
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postDelete(Request $request)
    {
        if (!$code = $this->service->get($request->email)) return response()->json(['error' => 'Could not find invite code.']);
        if (!$this->service->delete($code)) return response()->json(['error' => 'Error deleting invite code.']);
        else return response()->json(['success' => 'Deleted invite code.']);
    }
}
