<?php

namespace App\Http\Controllers\Admin\User;

use Flash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Admin\Controller;
use App\Services\User\UserPlanService;
use Illuminate\Http\Request;

class UserPlanController extends Controller
{
    /**
     * @var
     */
    protected $service;

    /**
     * UserRoleController constructor.
     * @param UserPlanService $service
     */
    public function __construct(UserPlanService $service)
    {
        parent::__construct();
        $this->service = $service;
    }

    /**
     * Validation
     * @param array $data
     * @param null $id
     * @return mixed
     */
    protected function validator(array $data, $id = null)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:100|unique:user_plans' . $id ? ',' . $id : '',
        ]);
    }

    /**
     * Show index page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $plans = $this->service->getAll();
        return view('admin.users.plans.index', compact('plans'));
    }

    /**
     * Creates a user plan
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postCreate(Request $request)
    {
        if ($validator = $this->validator($request->only(['name']))->passes()) {
            if (!$plan = $this->service->create($request->all())) return response()->json(['error' => 'Error creating user plan.']);
            else response()->json(['success' => 'Created new user plan.']);
        } else return response()->json(['error' => $validator->errors()->all()]);
    }

    /**
     * Updates a user plan
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postUpdate(Request $request)
    {
        if (!$plan = $this->service->get($request->id)) return response()->json(['error' => 'Could not find user plan.']);
        if ($validator = $this->validator($request->only(['name'], $plan->id))->passes()) {
            if (!$this->service->update($plan, $request->all())) return response()->json(['error' => 'Error updating user plan.']);
            else return response()->json(['success' => 'Updated user plan.']);
        } else return response()->json(['error' => $validator->errors()->all()]);
    }

    /**
     * Deletes a user plan
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postDelete(Request $request)
    {
        if (!$plan = $this->service->get($request->id)) return response()->json(['error' => 'Could not find user plan.']);
        if (!$this->service->delete($plan)) return response()->json(['error' => 'Error deleting user plan.']);
        else return response()->json(['success' => 'Deleted user plan.']);
    }
}
