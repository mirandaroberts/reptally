<?php

namespace App\Http\Controllers\Admin\User;

use Flash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Services\User\UserService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * @var
     */
    protected $service;

    /**
     * UserController constructor.
     * @param UserService $service
     */
    public function __construct(UserService $service)
    {
        parent::__construct();
        $this->service = $service;
    }

    /**
     * Validation
     * @param array $data
     * @param null $id
     * @return mixed
     */
    protected function validator(array $data, $id = null)
    {
        return Validator::make($data, [
            'email'      => 'required|string|max:255|unique:users' . $id ? ',' . $id : '',
            'first_name' => 'required|string|max:255',
            'last_name'  => 'required|string|max:255'
        ]);
    }

    /**
     * Show index page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.users.index');
    }

    /**
     * Fetch all data using AJAX
     * @return mixed
     */
    public function postFetchAll(Request $request)
    {
        return $this->service->getDataTables($this->service->getAll());
    }

    /**
     * Fetch one using AJAX
     * @param Request $request
     * @return mixed
     */
    public function postFetch(Request $request)
    {
        $user = $this->service->get($request->id);
        $data = [
            'dropdowns' => [],
            'fields'    => [
                'first_name' => $user->first_name,
                'last_name'  => $user->last_name,
                'email'      => $user->email,
                'role'       => $user->role->name,
                'plan'       => $user->plan->name,
                'reputation' => $user->reputation
            ],
            'tables' => []
        ];
        return $this->service->getOneDataTables(collect($data));
    }

    /**
     * Updates existing
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postUpdate(Request $request)
    {
        if (!$user = $this->service->get($request->id)) return response()->json(['error' => 'Could not find user.']);
        if ($validator = $this->validator($request->only(['email', 'first_name', 'last_name'], $user->id))) {
            if (!$this->service->update($user, $request->all())) return response()->json(['error' => 'Error updating user.']);
            else return response()->json(['success' => 'Updated user.']);
        } else return response()->json(['error' => $validator->errors()->all()]);
    }

    /**
     * Show user settings page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getSettings()
    {
        return view('admin.users.settings');
    }
}
