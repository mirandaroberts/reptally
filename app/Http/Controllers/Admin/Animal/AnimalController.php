<?php

namespace App\Http\Controllers\Admin\Animal;

use App\Services\Animal\AnimalService;
use App\Services\DropdownService;
use Flash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Admin\Controller;
use App\Services\Animal\AnimalSpeciesService;
use Illuminate\Http\Request;

class AnimalController extends Controller
{
    /**
     * @var AnimalService
     */
    protected $service;

    /**
     * @var AnimalSpeciesService
     */
    protected $speciesService;

    /**
     * @var DropdownService
     */
    protected $dropdownService;

    /**
     * AnimalController constructor.
     * @param AnimalSpeciesService $speciesService
     * @param AnimalService $service
     * @param DropdownService $dropdownService
     */
    public function __construct(AnimalSpeciesService $speciesService, AnimalService $service, DropdownService $dropdownService)
    {
        parent::__construct();
        $this->service         = $service;
        $this->speciesService  = $speciesService;
        $this->dropdownService = $dropdownService;
    }

    /**
     * Validator
     * @param array $data
     * @param null $id
     * @return mixed
     */
    protected function validator(array $data, $id = null)
    {
        return Validator::make($data, [
            'name'            => 'required|string|max:100|unique:animal_species' . $id ? ',' . $id : '',
            'scientific_name' => 'required|string|max:100',
            'description'     => 'max:10000',
            'status'          => 'in:NE,DD,LC,NT,VU,EN,CR,EW,EX',
            'venomous'        => 'boolean'
        ]);
    }

    /**
     * Show index page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.animals.index', [
            'list_species' => json_encode($this->dropdownService->getDropdowns('allSpecies')),
        ]);
    }

    /**
     * Fetch data using AJAX
     * @return mixed
     */
    public function postFetch(Request $request)
    {
        return $this->service->getDataTables();
    }

    /**
     * Fetch one species using AJAX
     * @param Request $request
     * @return mixed
     */
    public function postFetchOne(Request $request)
    {
        return $this->service->getOneDataTables($request->id);
    }

    /**
     * Creates a new species
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postCreate(Request $request)
    {
        if ($validator = $this->validator($request->only(['name', 'scientific_name', 'description', 'status', 'venomous']))) {
            if (!$species = $this->service->create($request->all())) return response()->json(['error' => 'Error creating animal species.']);
            else {
                $this->service->updateChangelog($species, auth()->user(), 'Created');
                return response()->json(['success' => 'Created new animal species.']);
            }
        } else return response()->json(['error' => $validator->errors()->all()]);
    }

    /**
     * Updates a species
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postUpdate(Request $request)
    {
        $fields = $request->fields;
        if (!$species = $this->service->get($fields['id'])) return response()->json(['error' => 'Could not find species.']);
        if ($validator = $this->validator(['name' => $fields['name'], 'scientific_name' => $fields['scientific_name'],
            'description' => $fields['description'], 'status' => $fields['status'], 'venomous' => $fields['venomous']], $species->id)) {
            if (!$this->service->update($species, $fields)) return response()->json(['error' => 'Error updating animal species.']);
            else {
                if ($request->tables) $this->service->updateSubTables($request->tables);
                $this->service->updateChangelog($species, auth()->user(), 'Updated');
                return response()->json(['success' => 'Updated animal species.']);
            }
        } else return response()->json(['error' => $validator->errors()->all()]);
    }

    /**
     * Deletes a species
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postDelete(Request $request)
    {
        if (!$species = $this->service->get($request->id)) return response()->json(['error' => 'Could not find species.']);
        if (!$this->service->delete($species)) return response()->json(['error' => 'Error deleting animal species.']);
        else {
            $this->service->updateChangelog($species, auth()->user(), 'Deleted');
            return response()->json(['success' => 'Deleted animal species.']);
        }
    }
}
