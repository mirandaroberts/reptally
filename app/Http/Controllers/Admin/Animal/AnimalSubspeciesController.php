<?php

namespace App\Http\Controllers\Admin\Animal;

use App\Services\Animal\AnimalSpeciesService;
use App\Services\Animal\AnimalSubspeciesService;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Admin\Controller;
use Illuminate\Http\Request;
use Flash;

class AnimalSubspeciesController extends Controller
{
    /**
     * @var AnimalSubspeciesService
     */
    protected $service;

    /**
     * @var AnimalSpeciesService
     */
    protected $speciesService;

    /**
     * @var array
     */
    protected $fields;

    /**
     * AnimalMorphController constructor.
     * @param AnimalSubspeciesService $service
     * @param AnimalSpeciesService $speciesService
     */
    public function __construct(AnimalSubspeciesService $service, AnimalSpeciesService $speciesService)
    {
        parent::__construct();

        $this->service = $service;
        $this->speciesService = $speciesService;
    }

    /**
     * Validator
     * @param array $data
     * @return mixed
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'            => 'required|string|max:100',
            'species_id'      => 'required|exists:animal_species',
            'description'     => 'string',
            'scientific_name' => 'required|string|max:100',
            'status'          => 'in:NE,DD,LC,NT,VU,EN,CR,EW,EX',
        ]);
    }

    /**
     * Create Subspecies
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postCreateSubspecies(Request $request)
    {
        if ($validator = $this->validator($request->only($this->fields))) {
            if (!$subspecies = $this->service->create($request->all())) return response()->json(['error' => 'Error creating subspecies.']);
            else {
                $species = $this->speciesService->get($subspecies->species_id);
                $this->speciesService->updateChangelog($species, auth()->user(), 'Created Subspecies ' . $subspecies->name);
                return response()->json(['success' => 'Created new subspecies.']);
            }
        } else return response()->json(['error' => $validator->errors()->all()]);
    }
}