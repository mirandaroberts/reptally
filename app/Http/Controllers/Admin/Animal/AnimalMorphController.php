<?php

namespace App\Http\Controllers\Admin\Animal;

use App\Services\Animal\AnimalSpeciesService;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Admin\Controller;
use App\Services\Animal\AnimalMorphService;
use Illuminate\Http\Request;
use Flash;

class AnimalMorphController extends Controller
{
    /**
     * @var AnimalMorphService
     */
    protected $service;

    /**
     * @var AnimalSpeciesService
     */
    protected $speciesService;

    /**
     * @var array
     */
    protected $fields;

    /**
     * AnimalMorphController constructor.
     * @param AnimalMorphService $service
     * @param AnimalSpeciesService $speciesService
     */
    public function __construct(AnimalMorphService $service, AnimalSpeciesService $speciesService)
    {
        parent::__construct();

        $this->service = $service;
        $this->speciesService = $speciesService;
    }

    /**
     * Validator
     * @param array $data
     * @return mixed
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'            => 'required|string|max:100',
            'species_id'      => 'required|exists:animal_species',
            'inheritance'     => 'required|in:Recessive,Dominant,Incomplete-Dominant|Codominant|Line Bred|Unknown',
            'lethal'          => 'boolean|nullable',
            'deformities'     => 'string|nullable|max:255',
            'discovered_by'   => 'string|nullable|max:255',
            'discovered_id'   => 'integer|nullable|exists:users',
            'discovered_date' => 'date|nullable',
            'description'     => 'string',
            'complex_id'      => 'integer|nullable|exists:animal_morph_complexes',
        ]);
    }

    /**
     * Create Morph
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postCreateMorph(Request $request)
    {
        if ($validator = $this->validator($request->only($this->fields))) {
            if (!$morph = $this->service->create($request->all())) return response()->json(['error' => 'Error creating morph.']);
            else {
                $species = $this->speciesService->get($morph->species_id);
                $this->speciesService->updateChangelog($species, auth()->user(), 'Created Morph ' . $morph->name);
                return response()->json(['success' => 'Created new morph.']);
            }
        } else return response()->json(['error' => $validator->errors()->all()]);
    }
}