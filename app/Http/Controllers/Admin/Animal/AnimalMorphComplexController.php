<?php

namespace App\Http\Controllers\Admin\Animal;

use App\Services\Animal\AnimalMorphComplexService;
use App\Services\Animal\AnimalSpeciesService;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Admin\Controller;
use Illuminate\Http\Request;
use Flash;

class AnimalMorphComplexController extends Controller
{
    /**
     * @var AnimalMorphComplexService
     */
    protected $service;

    /**
     * @var AnimalSpeciesService
     */
    protected $speciesService;

    /**
     * @var array
     */
    protected $fields;

    /**
     * AnimalMorphController constructor.
     * @param AnimalMorphComplexService $service
     * @param AnimalSpeciesService $speciesService
     */
    public function __construct(AnimalMorphComplexService $service, AnimalSpeciesService $speciesService)
    {
        parent::__construct();

        $this->service = $service;
        $this->speciesService = $speciesService;
    }

    /**
     * Validator
     * @param array $data
     * @return mixed
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'                => 'required|string|max:100',
            'species_id'          => 'required|exists:animal_species'
        ]);
    }

    /**
     * Create Morph Complex
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postCreateMorphComplex(Request $request)
    {
        if ($validator = $this->validator($request->only($this->fields))) {
            if (!$complex = $this->service->create($request->all())) return response()->json(['error' => 'Error creating morph complex.']);
            else {
                // TODO add morphs to complex
                $species = $this->speciesService->get($complex->species_id);
                $this->speciesService->updateChangelog($species, auth()->user(), 'Created Morph Complex ' . $complex->name);
                return response()->json(['success' => 'Created new morph complex.']);
            }
        } else return response()->json(['error' => $validator->errors()->all()]);
    }
}