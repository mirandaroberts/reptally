<?php

namespace App\Http\Controllers\Admin\Animal;

use Flash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Admin\Controller;
use App\Services\Animal\AnimalSpeciesService;
use Illuminate\Http\Request;

class AnimalSpeciesController extends Controller
{
    /**
     * @var AnimalSpeciesService
     */
    protected $service;

    /**
     * @var array
     */
    protected $fields;

    /**
     * AnimalSpeciesController constructor.
     * @param AnimalSpeciesService $service
     */
    public function __construct(AnimalSpeciesService $service)
    {
        parent::__construct();

        $this->service  = $service;
        $this->fields   = ['name', 'scientific_name', 'description', 'status', 'venomous',
            'availability', 'avg_length', 'avg_weight', 'lifespan', 'diet', 'habitat', 'care_level'];
    }

    /**
     * Validator
     * @param array $data
     * @param null $id
     * @return mixed
     */
    protected function validator(array $data, $id = null)
    {
        return Validator::make($data, [
            'name'            => 'required|string|max:100|unique:animal_species' . $id ? ',' . $id : '',
            'scientific_name' => 'required|string|max:100',
            'description'     => 'max:10000',
            'status'          => 'in:NE,DD,LC,NT,VU,EN,CR,EW,EX',
            'venomous'        => 'boolean',
            'avg_length'      => 'string|max:255',
            'avg_weight'      => 'string|max:255',
            'care_level'      => 'in:Unknown,Beginner,Intermediate,Advanced',
            'availability'    => 'in:Common,Uncommon,Rare,Very Rare',
            'diet'            => 'string|max:255',
            'habitat'         => 'string|max:255',
        ]);
    }

    /**
     * Show index page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.animals.species.index');
    }

    /**
     * Fetch Data for datatables
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function fetch()
    {
        return $this->service->getDataTables($this->service->getForDataTables());
    }

    /**
     * Get create form
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        $species = $this->service->model;
        return view('admin.animals.species.edit', compact('species'));
    }

    /**
     * Get morphs page
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getMorphs($id)
    {
        $species = $this->service->get($id);
        return view('admin.animals.species.morphs', compact('species'));
    }

    /**
     * Creates a new species
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postCreate(Request $request)
    {
        if ($validator = $this->validator($request->only(['name', 'scientific_name', 'description', 'status', 'venomous']))) {
            if (!$species = $this->service->create($request->all())) return response()->json(['error' => 'Error creating animal species.']);
            else {
                $this->service->updateChangelog($species, auth()->user(), 'Created');
                return response()->json(['success' => 'Created new animal species.']);
            }
        } else return response()->json(['error' => $validator->errors()->all()]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEdit($id)
    {
        if (!$species = $this->service->get($id)) abort(404);
        else return view('admin.animals.species.edit', compact('species'));
    }


    public function postEdit($id, Request $request)
    {
        if (!$species = $this->service->get($id)) abort(404);
        if (!isset($request->venomous)) $request->request->add(['venomous' => 0]);
        if ($validator = $this->validator($request->only($this->fields))) {
            if ($this->service->update($species, $validator->data())) {
                $this->service->updateChangelog($species, auth()->user(), 'Updated');
                Flash::success('You have updated the species ' . $species->name);
            } else {
                Flash::error('There was an error updating ' . $species->name);
            }
            return redirect(route('admin.animals.species.edit', ['id' => $species->id]));
        }
    }

    /**
     * Deletes a species
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postDelete(Request $request)
    {
        if (!$species = $this->service->get($request->id)) return response()->json(['error' => 'Could not find species.']);
        if (!$this->service->delete($species)) return response()->json(['error' => 'Error deleting animal species.']);
        else {
            $this->service->updateChangelog($species, auth()->user(), 'Deleted');
            return response()->json(['success' => 'Deleted animal species.']);
        }
    }
}
