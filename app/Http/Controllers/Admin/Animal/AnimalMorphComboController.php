<?php

namespace App\Http\Controllers\Admin\Animal;

use App\Services\Animal\AnimalMorphComboService;
use App\Services\Animal\AnimalSpeciesService;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Admin\Controller;
use Illuminate\Http\Request;
use Flash;

class AnimalMorphComboController extends Controller
{
    /**
     * @var AnimalMorphComboService
     */
    protected $service;

    /**
     * @var AnimalSpeciesService
     */
    protected $speciesService;

    /**
     * @var array
     */
    protected $fields;

    /**
     * AnimalMorphController constructor.
     * @param AnimalMorphComboService $service
     * @param AnimalSpeciesService $speciesService
     */
    public function __construct(AnimalMorphComboService $service, AnimalSpeciesService $speciesService)
    {
        parent::__construct();

        $this->service = $service;
        $this->speciesService = $speciesService;
    }

    /**
     * Validator
     * @param array $data
     * @return mixed
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'                => 'required|string|max:100',
            'species_id'          => 'required|exists:animal_species',
            'lethal'              => 'boolean|nullable',
            'deformities'         => 'string|nullable|max:255',
            'first_produced_by'   => 'string|nullable|max:255',
            'first_produced_id'   => 'integer|nullable|exists:users',
            'first_produced_date' => 'date|nullable',
            'description'         => 'string',
            'combo'               => 'required',
        ]);
    }

    /**
     * Create Morph Combo
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postCreateMorphCombo(Request $request)
    {
        if ($validator = $this->validator($request->only($this->fields))) {
            if (!$combo = $this->service->create($request->all())) return response()->json(['error' => 'Error creating morph combo.']);
            else {
                $species = $this->speciesService->get($combo->species_id);
                $this->speciesService->updateChangelog($species, auth()->user(), 'Created Morph Combo ' . $combo->name);
                return response()->json(['success' => 'Created new morph combo.']);
            }
        } else return response()->json(['error' => $validator->errors()->all()]);
    }
}