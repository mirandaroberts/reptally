<?php

namespace App\Http\Controllers\Admin\Animal;

use App\Services\Animal\AnimalLocalityService;
use App\Services\Animal\AnimalSpeciesService;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Admin\Controller;
use Illuminate\Http\Request;
use Flash;

class AnimalLocalityController extends Controller
{
    /**
     * @var AnimalLocalityService
     */
    protected $service;

    /**
     * @var AnimalSpeciesService
     */
    protected $speciesService;

    /**
     * @var array
     */
    protected $fields;

    /**
     * AnimalMorphController constructor.
     * @param AnimalLocalityService $service
     * @param AnimalSpeciesService $speciesService
     */
    public function __construct(AnimalLocalityService $service, AnimalSpeciesService $speciesService)
    {
        parent::__construct();

        $this->service = $service;
        $this->speciesService = $speciesService;
    }

    /**
     * Validator
     * @param array $data
     * @return mixed
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'            => 'required|string|max:100',
            'species_id'      => 'required|exists:animal_species',
            'description'     => 'string'
        ]);
    }

    /**
     * Create Locality
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postCreateLocality(Request $request)
    {
        if ($validator = $this->validator($request->only($this->fields))) {
            if (!$locality = $this->service->create($request->all())) return response()->json(['error' => 'Error creating locality.']);
            else {
                $species = $this->speciesService->get($locality->species_id);
                $this->speciesService->updateChangelog($species, auth()->user(), 'Created Locality ' . $locality->name);
                return response()->json(['success' => 'Created new locality.']);
            }
        } else return response()->json(['error' => $validator->errors()->all()]);
    }
}