<?php

namespace App\Http\Controllers\Admin;

use App\Services\DropdownService;
use Auth;
use Illuminate\Http\Request;
use Spatie\Analytics\AnalyticsFacade as Analytics;
use Spatie\Analytics\Period;

class AdminController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Show index page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $pageviewsThisMonth    = Analytics::fetchVisitorsAndPageViews(Period::months(1));
        $mostVisitedThisMonth  = Analytics::fetchMostVisitedPages(Period::months(1));
        $totalViewsThisWeek    = Analytics::fetchTotalVisitorsAndPageViews(Period::days(7));
        $totalViewsThisMonth   = Analytics::fetchTotalVisitorsAndPageViews(Period::months(1));
        $topBrowsersThisMonth  = Analytics::fetchTopBrowsers(Period::months(1));
        $topReferrersThisMonth = Analytics::fetchTopReferrers(Period::months(1));

        $analyticsData = collect([
            'pageviewsThisMonth'    => $pageviewsThisMonth,
            'totalViewsThisWeek'    => $totalViewsThisWeek,
            'totalViewsThisMonth'   => $totalViewsThisMonth,
            'topBrowsersThisMonth'  => $topBrowsersThisMonth,
            'mostVisitedThisMonth'  => $mostVisitedThisMonth,
            'topReferrersThisMonth' => $topReferrersThisMonth
        ]);

        return view('admin.index', compact('analyticsData'));
    }
}
