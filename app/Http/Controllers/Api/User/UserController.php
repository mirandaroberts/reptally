<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Admin\Controller;
use App\Services\User\User\UserService;
use App\Transformers\UserTransformer;
use EllipseSynergie\ApiResponse\Laravel\Response;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * RESTful response
     * @var
     */
    protected $response;

    /**
     * Service
     * @var
     */
    protected $service;

    /**
     * UserController constructor.
     * @param Response $response
     */
    public function __construct(Response $response, UserService $service)
    {
        $this->response = $response;
        $this->service  = $service;
    }

    /**
     * Returns all users
     * @return mixed
     */
    public function index()
    {
        $users = $this->service->getAll();
        return $this->response->withCollection($users, new UserTransformer());
    }

    /**
     * Return one user
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        $user = $this->service->get($id);
        if (!$user) return $this->response->errorNotFound('User not found.');
        else return $this->response->withItem($user, new UserTransformer());
    }

    /**
     * Delete a user
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $user = $this->service->get($id);
        if (!$user) return $this->response->errorNotFound('User not found.');
        if (!$this->service->delete($user)) return $this->response->errorInternalError('Error deleting user.');
        else return $this->response->withItem($user, new UserTransformer());
    }

    /**
     * Update or create a user
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        if ($request->isMethod('put')) {
            $user = $this->service->get($request->id);
            if (!$user) return $this->response->errorNotFound('User not found.');
            if (!$this->service->update($user, $request->all())) return $this->response->errorInternalError('Error updating user.');
        } else {
            if (!$this->service->create($request->all())) return $this->response->errorInternalError('Error creating user.');
        }
        return $this->response->withItem($user, new UserTransformer());
    }
}