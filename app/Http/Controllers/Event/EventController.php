<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;

class EventController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Show index page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCalendar()
    {
        return view('app.events.calendar');
    }

    /**
     * Show create event form
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        return view('app.events.create');
    }
}
