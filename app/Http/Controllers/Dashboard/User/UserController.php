<?php

namespace App\Http\Controllers\Dashboard\User;

use Flash;
use App\Http\Controllers\Controller;
use App\Services\User\UserService;

class UserController extends Controller
{
    /**
     * @var
     */
    protected $service;

    /**
     * UserController constructor.
     * @param UserService $service
     */
    public function __construct(UserService $service)
    {
        parent::__construct();
        $this->service = $service;
    }

    /**
     * Show user settings page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getSettings()
    {
        return view('dashboard.users.settings');
    }
}
