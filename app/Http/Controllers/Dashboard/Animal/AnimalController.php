<?php

namespace App\Http\Controllers\Dashboard\Animal;

use App\Services\Animal\AnimalService;
use Flash;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AnimalController extends Controller
{
    /**
     * @var
     */
    protected $service;

    /**
     * AnimalController constructor.
     * @param AnimalService $service
     */
    public function __construct(AnimalService $service)
    {
        parent::__construct();
        $this->service = $service;
    }

    /**
     * Validator
     * @param array $data
     * @return mixed
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'item_number'     => 'required|string|max:100',
            'name'            => 'required|string|max:100',
            'description'     => 'max:10000',
            'public'          => 'boolean'
        ]);
    }

    /**
     * Return animal index
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $tutorial = false;
        if (!count(auth()->user()->animals)) $tutorial = true;
        return view('dashboard.animals.index', compact('tutorial'));
    }

    /**
     * Fetch all data using AJAX
     * @return mixed
     */
    public function postFetchAll()
    {
        return $this->service->getDataTables($this->service->getAll(), ['user_id', 'description', 'sale_price']);
    }

    /**
     * Fetch one using AJAX
     * @param Request $request
     * @return mixed
     */
    public function postFetch(Request $request)
    {
        $animal = $this->service->get($request->id);
        $data = [
            'dropdowns' => [],
            'fields'    => [
                'id'          => $animal->id,
                'item_number' => $animal->item_number,
                'name'        => $animal->name,
                'species'     => $animal->species->name,
                'subspecies'  => $animal->subspecies->name,
                'locality'    => $animal->locality->name,
                'morphs'      => $animal->morphs,
                'public'      => $animal->public,
                'image'       => $animal->defaultImageIcon,
                'gender'      => $animal->gender,
                'birth_date'  => $animal->birth_date,
            ],
            'tables' => []
        ];
        return $this->service->getOneDataTables(collect($data));
    }

    /**
     * Create an animal
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postCreate(Request $request) {
        if ($validator = $this->validator($request->only($this->service->required))) {
            if (!$species = $this->service->create($request->all())) return response()->json(['error' => 'Error creating animal.']);
            else return response()->json(['success' => 'Successfully added animal.']);
        } else return response()->json(['error' => $validator->errors()->all()]);
    }
}
