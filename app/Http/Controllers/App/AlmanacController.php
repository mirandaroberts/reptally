<?php

namespace App\Http\Controllers\App;

use App\Services\Animal\AnimalLocalityService;
use App\Services\Animal\AnimalMorphComboService;
use App\Services\Animal\AnimalMorphService;
use App\Services\Animal\AnimalSpeciesService;
use App\Services\Animal\AnimalSubspeciesService;
use App\Http\Controllers\Controller;

class AlmanacController extends Controller
{
    /**
     * @var AnimalSpeciesService
     */
    protected $speciesService;

    /**
     * @var AnimalMorphService
     */
    protected $morphService;

    /**
     * @var AnimalMorphComboService
     */
    protected $comboService;

    /**
     * @var AnimalLocalityService
     */
    protected $localityService;

    /**
     * @var AnimalSubspeciesService
     */
    protected $subspeciesService;

    /**
     * AlmanacController constructor.
     * @param AnimalSpeciesService $speciesService
     * @param AnimalMorphService $morphService
     * @param AnimalMorphComboService $comboService
     * @param AnimalLocalityService $localityService
     * @param AnimalSubspeciesService $subspeciesService
     */
    public function __construct(AnimalSpeciesService $speciesService, AnimalMorphService $morphService,
                                AnimalMorphComboService $comboService, AnimalLocalityService $localityService,
                                AnimalSubspeciesService $subspeciesService)
    {
        parent::__construct();
        $this->speciesService      = $speciesService;
        $this->morphService        = $morphService;
        $this->comboService        = $comboService;
        $this->localityService     = $localityService;
        $this->subspeciesService   = $subspeciesService;
    }

    /**
     * Show index page for alamanc.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('app.almanac.index', [
            'speciesList' => $this->speciesService->getAll(),
        ]);
    }

    //A query can be passed through: species-name+morph/combos/subspecies/etc
    public function page($query)
    {
        $data = explode('+', $query);
        $species = $this->speciesService->getByName($data[0]);
        array_shift($data);
        return view('app.almanac.page', [
            'species' => $species
        ]);
    }
}
