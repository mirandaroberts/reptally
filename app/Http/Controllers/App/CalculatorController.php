<?php

namespace App\Http\Controllers\App;

use App\Services\Animal\AnimalMorphComboService;
use App\Services\Animal\AnimalMorphService;
use App\Services\Animal\AnimalSpeciesService;
use App\Http\Controllers\Controller;
use App\Services\Calculator\CalculatorService;

class CalculatorController extends Controller
{
    /**
     * Services
     * @var
     */
    protected $speciesService;
    protected $morphService;
    protected $comboService;
    protected $service;

    /**
     * CalculatorController constructor.
     * @param AnimalSpeciesService      $speciesService
     * @param AnimalMorphService        $morphService
     * @param AnimalMorphComboService   $comboService
     * @param CalculatorService         $service
     */
    public function __construct(AnimalSpeciesService $speciesService, AnimalMorphService $morphService,
                                AnimalMorphComboService $comboService, CalculatorService $service)
    {
        parent::__construct();
        $this->speciesService = $speciesService;
        $this->morphService   = $morphService;
        $this->comboService   = $comboService;
        $this->service        = $service;
    }

    /**
     * Shows calculator page with optional parameters for easy linking
     * @param null $species
     * @param null $parent1
     * @param null $parent2
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($species = null, $parent1 = null, $parent2 = null)
    {
        $results = null;
        if ($species && $parent1 && $parent2) {
            $species = $this->speciesService->getByName($species);
            $results = $this->calculate($species, $parent1, $parent2);
        }
        return view('app.calculator', [
            'speciesList' => $this->speciesService->getAll(),
            'results'     => $results,
            'parent1'     => $parent1,
            'parent2'     => $parent2,
            'species'     => $species
        ]);
    }

    /**
     * Return offspring data from parent data
     * @param $s
     * @param $p1
     * @param $p2
     * @return array
     */
    protected function calculate($s, $p1, $p2)
    {
        if (!$species = $s) abort(404);
        $results = [];
        $parent1 = $this->parseString($species, $p1);
        $parent2 = $this->parseString($species, $p2);
        $geneMap = $this->service->getChildPossibilities($parent1, $parent2);
        $totalCombinations = $geneMap['total'];
        foreach($geneMap['genetics'] as $key => $value) {
            $percentage = $value['count'] / $totalCombinations * 100;
            $genes = $this->parseResults($value['genes'], $species);
            $results[] = [
                'genes'   => $genes,
                'combo'   => $this->comboService->getFromGenes($genes),
                'percent' => number_format($percentage,2)
            ];
        }
        return $results;
    }

    /**
     * Parse results into usable array
     * @param $genes
     * @param $species
     * @return array
     */
    protected function parseResults($genes, $species)
    {
        $return = [];
        foreach ($genes as $gene) foreach ($gene as $g) {
            if ($morph = $this->morphService->getByName($g->name, $species->id)) {
                $name = $morph->name;
                // Name modifiers
                switch ($morph->inheritance) {
                    case 'Recessive' :
                        if ($g->count === 1) $name = 'Het ' . $morph->name;
                        break;
                    case 'Incomplete-Dominant' :
                        if ($g->count === 2) $name = 'Super ' . $morph->name;
                        break;
                }
                $return[$name] = $morph;
            }
        }
        return $return;
    }

    /**
     * Extracts data from string to compile array of morphs.
     * Expects data to be a string of morph names or combos separated by +
     * @param $species
     * @param $data
     * @return array
     */
    protected function parseString($species, $data)
    {
        $morphs = array();
        $morphArray = explode('+', $data);
        foreach ($morphArray as $morph) {
            if ($m = $this->morphService->getByName($morph, $species->id)) {
                if ($m->inheritance != 'Recessive') $morphs[$m->name] = 1;
                else $morphs[$m->name] = 2;
            }
            else if ($combo = $this->comboService->getByName($morph, $species->id)) {
                foreach ($this->comboService->separate($combo) as $m) {
                    if ($m->inheritance != 'Recessive') $morphs[$m->name] = 1;
                    else $morphs[$m->name] = 2;
                }
            } else if (substr($morph, 0, 6) === "super ") {
                $m = $this->morphService->getByName(substr($morph, 6), $species->id);
                if ($m && $m->inheritance == 'Incomplete-Dominant') $morphs[$m->name] = 2;
            } else if (substr($morph, 0, 4) === "het ") {
                $m = $this->morphService->getByName(substr($morph, 4), $species->id);
                if ($m && $m->inheritance == 'Recessive') $morphs[$m->name] = 1;
            }
        }
        return array_filter($morphs);
    }
}
