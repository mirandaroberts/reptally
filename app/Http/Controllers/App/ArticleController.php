<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;

class ArticleController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Show index page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('app.articles.index');
    }
}
