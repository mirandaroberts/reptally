<?php

namespace App\Http\Controllers\Chatter;

use Auth;
use Flash;
use App\Services\Chatter\ChatterService as Helper;
use App\Models\Chatter\Models;
use App\Http\Controllers\Controller;

class ChatterController extends Controller
{
    /**
     * @param string $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($slug = '')
    {
        $pagination_results = config('chatter.paginate.num_of_results');
        
        $discussions = Models::discussion()->with('user')->with('post')->with('postsCount')->with('category')->orderBy(config('chatter.order_by.discussions.order'), config('chatter.order_by.discussions.by'));
        if (isset($slug)) {
            $category = Models::category()->where('slug', '=', $slug)->first();
            
            if (isset($category->id)) {
                $current_category_id = $category->id;
                $discussions = $discussions->where('chatter_category_id', '=', $category->id);
            } else {
                $current_category_id = null;
            }
        }
        
        $discussions = $discussions->paginate($pagination_results);
        
        $categories = Models::category()->get();
        $categoriesMenu = Helper::categoriesMenu(array_filter($categories->toArray(), function ($item) {
            return $item['parent_id'] === null;
        }));
        
        $chatter_editor = config('chatter.editor');
        
        if ($chatter_editor == 'simplemde') {
            // Dynamically register markdown service provider
            \App::register('GrahamCampbell\Markdown\MarkdownServiceProvider');
        }
        
        return view('app.chatter.home', compact('discussions', 'categories', 'categoriesMenu', 'chatter_editor', 'current_category_id'));
    }

    /**
     * @return mixed
     */
    public function login()
    {
        if (!Auth::check()) {
            Flash::error('You must login or register an account with us first!');
            return redirect('/login');
        }
    }

    /**
     * @return mixed
     */
    public function register()
    {
        if (!Auth::check()) {
            Flash::error('You must register an account first!');
            return redirect('/register');
        }
    }
}
