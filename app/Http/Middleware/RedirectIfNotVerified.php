<?php

namespace App\Http\Middleware;

use Flash;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfNotVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check() && !Auth::user()->isVerified) {
            Flash::warning('You must verify your email before doing this.');
            return redirect('/admin');
        }
        return $next($request);
    }
}
