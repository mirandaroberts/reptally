<?php

namespace App\Http\Middleware;

use Flash;
use Closure;

class VerifyRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $roles
     * @return mixed
     */
    public function handle($request, Closure $next, $roles)
    {
        if (auth()->check() && !auth()->user()->hasRole(explode("|", $roles))) {
            Flash::error('You don\'t have permission to do this.');
            return redirect('/admin');
        }
        return $next($request);
    }
}
