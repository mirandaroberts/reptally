<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class ReputationLog extends Model
{
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'user_id', 'reputation', 'url', 'action'
    ];

    /**
     * Has many users
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User\User');
    }
}
