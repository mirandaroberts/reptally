<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class UserCode extends Model
{
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'email', 'code', 'used', 'used_at'
    ];

    /**
     * @var string
     */
    protected $table = 'user_invite_codes';

    /**
     * Checks if a code is still valid or has been used
     * @return bool
     */
    public function getIsValidAttribute()
    {
        if ($this->used) return false;
        else return true;
    }
}
