<?php

namespace App\Models\User;

use Illuminate\Support\Facades\Config;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Events\UserAmended;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * Dispatches an event that clears the cache
     * @var array
     */
    protected $dispatchesEvents = [
        'saved'    => UserAmended::class,
        'deleted'  => UserAmended::class,
        'restored' => UserAmended::class,
    ];

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Belongs to a user role.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role()
    {
        return $this->belongsTo('App\Models\User\UserRole', 'role_id');
    }

    /**
     * Has user settings
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function settings()
    {
        return $this->hasOne('App\Models\User\UserSetting');
    }

    /**
     * Belongs to user plan
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function plan()
    {
        return $this->belongsTo('App\Models\User\UserPlan');
    }

    /**
     * Check if a user is verified
     * @return bool
     */
    public function getIsVerifiedAttribute()
    {
        if ($this->settings->activation_token == null) return true;
        else return false;
    }

    /**
     * Check if user belongs to $role
     * @param array $roles
     * @return bool
     */
    public function hasRole(array $roles)
    {
        if (in_array(strtolower($this->role->name), $roles)) return true;
        else return false;
    }

    /**
     * Return the user's reputation rank
     * @return \Illuminate\Config\Repository|mixed
     */
    public function getReputationRankAttribute()
    {
        foreach (config('reputation') as $i => $rank) $amt[$i] = abs($i - $this->reputation);
        asort($amt);
        return config('reputation.' . $amt[0]);
    }
}
