<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class UserPlan extends Model
{
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'name', 'monthly_cost', 'animals', 'active_listings', 'annual_cost'
    ];

    /**
     * Mutator: Monthly Cost
     * @param $value
     * @return string
     */
    public function getMonthlyCostAttribute($value)
    {
        return '$' . money_format('%i', $value);
    }

    /**
     * Mutator: Annual Cost
     * @param $value
     * @return string
     */
    public function getAnnualCostAttribute($value)
    {
        return '$' . money_format('%i', $value);
    }

    /**
     * Mutator: Animals
     * @param $value
     * @return string
     */
    public function getAnimalsAttribute($value)
    {
        if ($value) return $value;
        else return 'Unlimited';
    }

    /**
     * Mutator: Animals
     * @param $value
     * @return string
     */
    public function getActiveListingsAttribute($value)
    {
        if ($value) return $value;
        else return 'Unlimited';
    }
}
