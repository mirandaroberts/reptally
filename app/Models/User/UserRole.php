<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'name', 'superuser', 'can_create_store', 'can_create_animal', 'can_list_animal', 'can_purchase_animal',
        'can_approve_morphs'
    ];

    /**
     * Has many users
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany('App\Models\User\User', 'role_id');
    }
}
