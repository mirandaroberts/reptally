<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;
use Laravel\Cashier\Billable;

class UserSetting extends Model
{
    use Billable;

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'ip_address', 'user_id', 'activation_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Primary key
     * @var string
     */
    protected $primaryKey = 'user_id';

    /**
     * Belongs to a user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User\User');
    }
}
