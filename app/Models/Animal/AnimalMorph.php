<?php

namespace App\Models\Animal;

use Illuminate\Database\Eloquent\Model;

class AnimalMorph extends Model
{
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'name', 'species_id', 'inheritance', 'lethal', 'deformities', 'discovered_by', 'discovered_id', 'discovered_id',
        'description', 'complex_id'
    ];

    /**
     * Ger species
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function species()
    {
        return $this->belongsTo('App\Models\Animal\AnimalSpecies', 'species_id');
    }
}
