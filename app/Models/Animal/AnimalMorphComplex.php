<?php

namespace App\Models\Animal;

use Illuminate\Database\Eloquent\Model;

class AnimalMorphComplex extends Model
{
    /**
     * Table name
     * @var string
     */
    protected $table = 'animal_morph_complexes';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'species_id', 'name'
    ];

    /**
     * Ger species
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function species()
    {
        return $this->belongsTo('App\Models\Animal\AnimalSpecies', 'species_id');
    }
}
