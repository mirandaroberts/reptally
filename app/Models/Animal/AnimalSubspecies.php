<?php

namespace App\Models\Animal;

use Illuminate\Database\Eloquent\Model;

class AnimalSubspecies extends Model
{
    /**
     * Table name
     * @var string
     */
    protected $table = 'animal_subspecies';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'name', 'scientific_name', 'species_id', 'status', 'description'
    ];

    /**
     * Ger species
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function species()
    {
        return $this->belongsTo('App\Models\Animal\AnimalSpecies', 'species_id');
    }
}
