<?php

namespace App\Models\Animal;

use Illuminate\Database\Eloquent\Model;

class AnimalSpeciesChangelog extends Model
{
    /**
     * Table name
     * @var string
     */
    protected $table = 'animal_species_changelog';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'name', 'species_id', 'user_id', 'change'
    ];

    /**
     * Get species
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function species()
    {
        return $this->belongsTo('App\Models\Animal\AnimalSpecies', 'species_id');
    }

    /**
     * Get user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User\User', 'user_id');
    }
}
