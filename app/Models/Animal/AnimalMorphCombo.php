<?php

namespace App\Models\Animal;

use Illuminate\Database\Eloquent\Model;

class AnimalMorphCombo extends Model
{
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'species_id', 'name', 'combo', 'first_produced_by', 'first_produced_id', 'first_produced_id', 'description'
    ];

    /**
     * Ger species
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function species()
    {
        return $this->belongsTo('App\Models\Animal\AnimalSpecies', 'species_id');
    }
}
