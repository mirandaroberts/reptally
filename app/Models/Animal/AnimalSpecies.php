<?php

namespace App\Models\Animal;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class AnimalSpecies extends Model
{
    /**
     * Table name
     * @var string
     */
    protected $table = 'animal_species';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'name', 'scientific_name', 'venomous', 'status', 'description', 'avg_length', 'avg_length', 'lifespan', 'diet',
        'habitat', 'care_level'
    ];

    /**
     * Has many subspecies
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subspecies()
    {
        return $this->hasMany('App\Models\Animal\AnimalSubspecies', 'species_id');
    }

    /**
     * Has many morphs
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function morphs()
    {
        return $this->hasMany('App\Models\Animal\AnimalMorph', 'species_id');
    }

    /**
     * Has many morph combos
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function combos()
    {
        return $this->hasMany('App\Models\Animal\AnimalMorphCombo', 'species_id');
    }

    /**
     * Has many localities
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function localities()
    {
        return $this->hasMany('App\Models\Animal\AnimalLocality', 'species_id');
    }

    /**
     * Has many logs
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function changelog()
    {
        return $this->hasMany('App\Models\Animal\AnimalSpeciesChangelog', 'species_id');
    }

    /**
     * Has many complexes
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function complexes()
    {
        return $this->hasMany('App\Models\Animal\AnimalMorphComplex', 'species_id');
    }

    /**
     * Format created at date
     * @param $value
     * @return mixed
     */
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('m-d-Y');
    }

    /**
     * Format updated at date
     * @param $value
     * @return mixed
     */
    public function getUpdatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('m-d-Y');
    }
}
