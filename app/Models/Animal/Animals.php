<?php

namespace App\Models\Animal;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Animal extends Model
{
    /**
     * Table name
     * @var string
     */
    protected $table = 'animals';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'name', 'item_number', 'species_id', 'gender', 'description', 'morphs', 'sale_price', 'public'
    ];

    /**
     * Belongs to species
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function species()
    {
        return $this->belongsTo('App/Models/Animal/AnimalSpecies', 'species_id');
    }

    /**
     * Belongs to user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App/Models/User/User', 'user_id');
    }

    /**
     * Format created at date
     * @param $value
     * @return mixed
     */
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('m-d-Y');
    }

    /**
     * Format updated at date
     * @param $value
     * @return mixed
     */
    public function getUpdatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('m-d-Y');
    }

    /**
     * Format birth year
     * @return string
     */
    public function getBirthYearAttribute()
    {
        if ($this->birth_date) return Carbon::parse($this->birth_date)->format('Y');
        else return 'Unknown';
    }
}
