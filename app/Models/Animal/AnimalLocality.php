<?php

namespace App\Models\Animal;

use Illuminate\Database\Eloquent\Model;

class AnimalLocality extends Model
{
    /**
     * Table name
     * @var string
     */
    protected $table = 'animal_localities';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'name', 'species_id', 'description'
    ];

    /**
     * Ger species
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function species()
    {
        return $this->belongsTo('App\Models\Animal\AnimalSpecies', 'species_id');
    }
}
