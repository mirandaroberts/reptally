<?php

namespace App\Listeners;

use App\Events\UserAmended;
use Illuminate\Contracts\Cache\Repository;

class ClearUserId
{
    /**
     * Create the event listener.
     * @return void
     */
    public function __construct(Repository $cache)
    {
        $this->cache = $cache;
    }

    /**
     * Clear user from cache
     * @param UserAmended $event
     */
    public function handle(UserAmended $event)
    {
        $this->cache->tags(get_class($event->model))->forget('users_' . $event->model->id);
    }
}