<?php

namespace App\Providers;

use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Contracts\Cache\Repository;
use Illuminate\Contracts\Hashing\Hasher as HasherContract;

class CachingUserProvider extends EloquentUserProvider
{
    /**
     * The cache instance.
     * @var Repository
     */
    protected $cache;

    /**
     * Create a new database user provider.
     * @param  \Illuminate\Contracts\Hashing\Hasher  $hasher
     * @param  string  $model
     * @param  Repository  $cache
     * @return void
     */
    public function __construct(HasherContract $hasher, $model, Repository $cache)
    {
        $this->model  = $model;
        $this->hasher = $hasher;
        $this->cache  = $cache;
    }

    /**
     * Retrieve a user by their unique id.
     * @param  mixed  $id
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveById($id)
    {
        return $this->cache->tags(['users'])->remember('users_' . $id, 60, function () use ($id) {
            $model = $this->createModel();
            return $model->newQuery()
                ->with('role', 'settings')
                ->where('id', $id)
                ->first();
        });
    }
}