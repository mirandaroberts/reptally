<?php

namespace App\Traits;

trait Datatable
{
    /**
     * Returns all rows for Datatables
     * @param $data
     * @param array $removedColumns
     * @param array $addedColumns
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function getDataTables($data, $removedColumns = array(), $addedColumns = array())
    {
        $datatables = datatables($data)->removeColumn('created_at')->removeColumn('updated_at');

        // Remove Columns
        foreach ($removedColumns as $column) $datatables->removeColumn($column);

        // Add Columns
        foreach ($addedColumns as $name => $column) $datatables->addColumn($name, function ($column) { return $column; });

        return $datatables->make(true);
    }
}