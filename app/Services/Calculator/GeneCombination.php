<?php

namespace App\Services\Calculator;

class GeneCombination
{
    /**
     * @var Gene[] $genes
     */
    public $genes;

    /**
     * GeneCombination constructor.
     */
    public function __construct()
    {
        $this->genes = [];
    }

    /**
     * Get genes in combination
     * @return array|Gene[]
     */
    public function getGenes() {
        return $this->genes;
    }

    /**
     * Add a Gene
     * @param Gene $gene
     */
    public function addGene(Gene $gene) {
        if($gene->isNull()) return;
        $newGene = clone $gene;
        foreach($this->genes as $existingGene) {
            if($existingGene->getName() == $gene->getName()) {
                $existingGene->setCount($existingGene->getCount() + $newGene->getCount());
                return;
            }
        }
        $this->genes[] = $newGene;
        usort($this->genes, function($a, $b) {
            return strcmp($a->getName(), $b->getName());
        });
    }

    /**
     * Checks if empty gene pool
     * @return bool
     */
    public function isEmpty() {
        return count($this->genes) === 0;
    }

    /**
     * Return a string value
     * @return string
     */
    public function __toString()
    {
        return "{ " . implode($this->genes, ",") . " }";
    }
}