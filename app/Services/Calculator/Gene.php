<?php

namespace App\Services\Calculator;

class Gene
{
    /**
     * Hold the gene name
     * @var
     */
    public $name;

    /**
     * Holds the number of copies
     * @var
     */
    public $count;

    /**
     * Creates empty gene object
     * @return Gene
     */
    static function MakeNullGene() {
        return new Gene("", 0);
    }

    /**
     * Gene constructor.
     * @param $name
     * @param $count
     */
    public function __construct($name, $count) {
        $this->setName($name);
        $this->setCount($count);
    }

    /**
     * Set gene name
     * @param $name
     */
    protected function setName($name) {
        if(is_null($name)) throw new InvalidArgumentException("Name can not be null");
        $this->name = $name;
    }

    /**
     * Set gene count
     * @param $count
     */
    public function setCount($count) {
        if($count < 0 || $count > 2) throw new InvalidArgumentException("Count can only be 0, 1, or 2");
        $this->count = $count;
    }

    /**
     * Returns a gene's name
     * @return mixed
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Return a gene's copy count
     * @return mixed
     */
    public function getCount() {
        return $this->count;
    }

    /**
     * Check if a gene is set
     * @return bool
     */
    public function isNull() {
        return $this->name === "" && $this->count === 0;
    }

    /**
     * Return a string value
     * @return string
     */
    public function __toString()
    {
        if ($this->isNull()) return "";
        return "{{$this->name}:{$this->count}}";
    }
}