<?php

namespace App\Services\Calculator;

/**
 * Class CalculatorService
 * @package App\Services\Calculator
 *
 * Special thanks to Andrey Melkinov
 *
 * Each input is an array with the following elements
 * [
 *   "gene name" => 0 (or 1 or 2),
 *   "another gene name" => 0 (or 1 or 2)
 * ]
 * Result is an array with
 * [
 *   "total" => 3 (a count of all different combinations)
 *   "genetics" => [ //an array where each entry has...
 *      [
 *          "genes" => (an instance of GeneCombination),
 *          "count" => (number of times the gene combination occurs)
 *      ]
 *   ]
 * ]
 */

class CalculatorService {
    /**
     * @param $parentAGenes
     * @param $parentBGenes
     * @return array
     */
    public function getChildPossibilities($parentAGenes, $parentBGenes) {
        /**
         * Get all possibilities
         * @var Gene[] $allGenes
         */
        $allGenes = $this->mergeGenes($parentAGenes, $parentBGenes);
        $result = $this->generatePossibilities($allGenes);
        $stack = [$result];
        $resultingMap = [];
        $totalCombinations = 0;
        while (count($stack) > 0) {
            $node = array_shift($stack);
            $children = $node->getChildren();
            if (count($children) > 0) foreach ($children as $child) $stack[] = $child;
            else {
                $temp = $node;
                $geneCombination = new GeneCombination();
                while (!is_null($temp)) {
                    $geneCombination->addGene($temp->getElement());
                    $temp = $temp->getParent();
                }
                $totalCombinations++;
                $hash = hash("md5", $geneCombination);
                if (array_key_exists($hash, $resultingMap)) $resultingMap[$hash]['count'] += 1;
                else {
                    $resultingMap[$hash] = [
                        "genes" => $geneCombination,
                        "count" => 1
                    ];
                }
            }
        }
        return [
            "total" => $totalCombinations,
            "genetics" => $resultingMap
        ];
    }

    /**
     * Calculate genetic combinations
     * @param $allGenes
     * @return Node
     */
    protected function generatePossibilities($allGenes) {
        $root = new Node(null, Gene::MakeNullGene());
        $leaves = [$root];
        while (count($allGenes) > 0) {
            $gene = array_shift($allGenes);
            $childGenes = $this->getGenePossibilities($gene);
            $newLeaves = [];
            foreach ($childGenes as $childGene) {
                foreach($leaves as $leaf) {
                    $newChild = new Node($leaf, $childGene);
                    $leaf->addChild($newChild);
                    $newLeaves[] = $newChild;
                }
            }
            $leaves = $newLeaves;
        }
        return $root;
    }

    /**
     * To simplify the calculation, we put all of the genes into a single array
     * @param $genesA
     * @param $genesB
     * @return array
     */
    protected function mergeGenes($genesA, $genesB) {
        $allGenes = [];
        foreach ($genesA as $geneName => $geneCount) $allGenes[] = new Gene($geneName, $geneCount);
        foreach ($genesB as $geneName => $geneCount) $allGenes[] = new Gene($geneName, $geneCount);
        return $allGenes;
    }

    /**
     * @param Gene $gene
     * @return array
     */
    protected function getGenePossibilities(Gene $gene) {
        if ($gene->getCount() === 0) return [];
        if ($gene->getCount() === 1) return [Gene::MakeNullGene(), new Gene($gene->getName(), 1)];
        if ($gene->getCount() === 2) return [new Gene($gene->getName(), 1), new Gene($gene->getName(), 1)];
        return [];
    }
}