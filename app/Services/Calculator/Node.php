<?php

namespace App\Services\Calculator;

class Node
{
    /**
     * @var
     */
    private $parent;

    /**
     * @var
     */
    private $element;

    /**
     * @var array
     */
    private $children;

    /**
     * Node constructor.
     * @param $parent
     * @param $element
     */
    public function __construct($parent, $element)
    {
        $this->parent = $parent;
        $this->element = $element;
        $this->children = [];
    }

    /**
     * Gets parent
     * @return mixed
     */
    public function getParent() {
        return $this->parent;
    }

    /**
     * Get element
     * @return mixed
     */
    public function getElement() {
        return $this->element;
    }

    /**
     * Add child
     * @param $child
     */
    public function addChild($child) {
        $this->children[] = $child;
    }

    /**
     * Get Children
     * @return array
     */
    public function getChildren() {
        return $this->children;
    }
}