<?php

namespace App\Services\User;

use App\Models\User\ReputationLog;
use App\Notifications\ReputationDeducted;
use App\Notifications\ReputationGained;
use App\Services\Service;
use App\Models\User\User;
use App\Models\User\UserSetting;
use App\Traits\Datatable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Request;

class UserService extends Service
{
    use Datatable;

    /**
     * Sets model and cache tag
     */
    public function beforeConstruct()
    {
        $this->model         = new User();
        $this->cache         = 'users';
        $this->relationships = ['role', 'settings', 'plan'];
        $this->required      = ['first_name', 'last_name', 'email'];
    }

    /**
     * Create user
     * @param $data
     * @return mixed
     */
    public function create(array $data)
    {
        $this->flushCache();
        $user = User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password'])
        ]);
        UserSetting::create([
            'user_id' => $user->id,
            'ip_address' => Request::ip(),
            'activation_token' => md5(time() . rand())
        ]);
        return $user;
    }

    /**
     * Update a user's settings
     * @param $user
     * @param $data
     * @return bool
     */
    public function updateSettings($user, $data)
    {
        $this->flushCache();
        if ($user->settings->update($data)) return true;
        else return false;
    }

    /**
     * Grants a user reputation
     * @param $user
     * @param $amt
     * @param $action
     * @param null $url
     */
    public function gainReputation($user, $amt, $action, $url = null)
    {
        $this->update($user, ['reputation' => $user->reputation + $amt]);
        $user->notify(new ReputationGained($amt, $action, $url));
        ReputationLog::create([
            'user_id'    => $user->id,
            'reputation' => $amt,
            'action'     => $action,
            'url'        => $url
        ]);
    }

    /**
     * Removes user reputation
     * @param $user
     * @param $amt
     * @param $action
     * @param null $url
     */
    public function deductReputation($user, $amt, $action, $url = null)
    {
        $this->update($user, ['reputation' => $user->reputation - $amt]);
        $user->notify(new ReputationDeducted($amt, $action, $url));
        ReputationLog::create([
            'user_id'    => $user->id,
            'reputation' => -$amt,
            'action'     => $action,
            'url'        => $url
        ]);
    }
}