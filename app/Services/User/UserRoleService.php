<?php

namespace App\Services\User;

use App\Services\Service;
use App\Models\User\UserRole;

class UserRoleService extends Service
{
    /**
     * Sets model and cache tag
     */
    public function beforeConstruct()
    {
        $this->model         = new UserRole();
        $this->cache         = 'user_roles';
        $this->required      = ['name', 'superuser', 'can_create_store', 'can_create_animal', 'can_list_animal',
            'can_purchase_animal', 'can_approve_morphs'];
    }
}