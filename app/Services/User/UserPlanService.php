<?php

namespace App\Services\User;

use App\Services\Service;
use App\Models\User\UserPlan;

class UserPlanService extends Service
{
    /**
     * Sets model and cache tag
     */
    public function beforeConstruct()
    {
        $this->model         = new UserPlan();
        $this->cache         = 'user_plans';
        $this->required      = ['name', 'monthly_cost', 'animals', 'active_listings', 'annual_cost'];
    }
}