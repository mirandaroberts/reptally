<?php

namespace App\Services\User;

use App\Notifications\InviteEmail;
use App\Services\Service;
use App\Models\User\UserCode;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Notification;

class UserCodeService extends Service
{
    /**
     * Sets model and cache tag
     */
    public function beforeConstruct()
    {
        $this->model = new UserCode();
    }

    /**
     * Uses an invite code
     * @param $code
     * @return mixed
     */
    public function invalidate($code)
    {
        return $code->update([
            'used'    => 1,
            'used_at' => Carbon::now()
        ]);
    }
}