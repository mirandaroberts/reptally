<?php

namespace App\Services;

use App\Services\Animal\AnimalMorphComplexService;
use App\Services\Animal\AnimalMorphService;
use App\Services\Animal\AnimalSpeciesService;

class DropdownService extends Service
{
    /**
     * Get a list of dropdowns by key
     * @param $key
     * @param array $params
     * @return mixed
     */
    public function getDropdowns($key, array $params = [])
    {
        return call_user_func_array(array($this, $key), $params);
    }

    /**
     * Get a dropdown of all species
     * @return array
     */
    public function allSpecies()
    {
        $speciesService = new AnimalSpeciesService();
        $species = $speciesService->getAll('name', 'asc');
        $return = array();

        if ($species) foreach ($species as $s) {
            $return[$s->id] = $s->name;
        }

        return $return;
    }

    /**
     * Get Species Dropdowns
     * @param $params
     * @return array
     */
    public function species($params)
    {
        return [
            'status'      => $this->statuses(),
            'care_level'  => $this->careLevels(),
            'inheritance' => $this->morphInheritances(),
            'complexes'   => $this->morphComplexes($params),
            'morphs'      => $this->morphs($params),
        ];
    }

    /**
     * Return dropdown array
     * @return array
     */
    public function statuses()
    {
        return [
            'data' => [
                'NE' => 'Not Evaluated (NE)',
                'DD' => 'Data Deficient (DD)',
                'LC' => 'Least Concern (LC)',
                'NT' => 'Not Threatened (NT)',
                'VU' => 'Vulnerable (VU)',
                'EN' => 'Endangered (EN)',
                'CR' => 'Critically Endangered (CR)',
                'EW' => 'Extinct in Wild (EW)',
                'EX' => 'Extinct (EX)'
            ],
            'default' => 'NE'
        ];
    }

    /**
     * Return care level dropdown array
     * @return array
     */
    public function careLevels()
    {
        return [
            'data' => [
                'Beginner'     => 'Beginner',
                'Intermediate' => 'Intermediate',
                'Advanced'     => 'Advanced',
                'Unknown'      => 'Unknown'
            ],
            'default' => 'Unknown'
        ];
    }

    /**
     * Return morph inheritance dropdown array
     * @return array
     */
    public function morphInheritances()
    {
        return [
            'data' => [
                'Recessive'            => 'Recessive',
                'Dominant'             => 'Dominant',
                'Incomplete-Dominant'  => 'Incomplete-Dominant',
                'Codominant'           => 'Codominant',
                'Line Bred'            => 'Line Bred',
                'Unknown'              => 'Unknown',
            ],
            'default' => 'Unknown'
        ];
    }

    /**
     * Get morph complex dropdown array
     * @param $id
     * @return array
     */
    public function morphComplexes($id)
    {
        $complexService = new AnimalMorphComplexService();
        $complexes = $complexService->getWhere([['species_id', $id]]);
        $return = array();

        if ($complexes) foreach ($complexes as $complex) {
            $return[$complex->id] = $complex->name;
        }

        return $return;
    }

    /**
     * Get morphs dropdown array
     * @param $id
     * @return array
     */
    public function morphs($id)
    {
        $morphService = new AnimalMorphService();
        $morphs = $morphService->getWhere([['species_id', $id]]);
        $return = array();

        if ($morphs) foreach ($morphs as $morph) {
            $return[$morph->id] = $morph->name;
        }

        return $return;
    }
}