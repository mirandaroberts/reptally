<?php

namespace App\Services\Flash;
use Session;

class Flash {
    /**
     * Push a flash message to the session
     * @param $type
     * @param $message
     */
    public function push($type, $message) 
    {
        Session::push('flash_message', array('type' => $type, 'message' => $message));
    }

    /**
     * Success messages
     * @param $message
     */
    public function success($message) 
    {
        $this->push('success', $message);
    }

    /**
     * Error messages
     * @param $message
     */
    public function error($message) 
    {
        $this->push('error', $message);
    }

    /**
     * Warning messages
     * @param $message
     */
    public function warning($message) 
    {
        $this->push('warning', $message);
    }

    /**
     * Info messages
     * @param $message
     */
    public function info($message) 
    {
        $this->push('info', $message);
    }

    /**
     * Pull flash messages from session
     * @return mixed
     */
    public function flush() {
        return Session::pull('flash_message');
    }
}
