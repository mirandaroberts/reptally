<?php

namespace App\Services\Animal;

use App\Models\Animal\AnimalMorph;
use App\Services\Service;
use Illuminate\Support\Facades\Cache;

class AnimalMorphService extends Service
{
    /**
     * Sets model and cache tag
     */
    public function beforeConstruct()
    {
        $this->model         = new AnimalMorph();
        $this->cache         = 'animal_morphs';
        $this->relationships = ['species'];
        $this->required      = ['name', 'species_id', 'inheritance', 'lethal', 'deformities', 'discovered_by', 'discovered_id',
            'discovered_date', 'description', 'complex_id'];
    }
}