<?php

namespace App\Services\Animal;

use App\Models\Animal\AnimalSubspecies;
use App\Services\Service;
use Illuminate\Support\Facades\Cache;

class AnimalSubspeciesService extends Service
{
    /**
     * Sets model and cache tag
     */
    public function beforeConstruct()
    {
        $this->model         = new AnimalSubspecies();
        $this->cache         = 'animal_subspecies';
        $this->relationships = ['species'];
        $this->required      = ['name', 'species_id', 'scientific_name', 'description', 'status'];
    }
}