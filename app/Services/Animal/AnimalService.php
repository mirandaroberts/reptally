<?php

namespace App\Services\Animal;

use App\Models\Animal\Animal;
use App\Traits\Datatable;
use Auth;
use App\Services\Service;

class AnimalService extends Service
{
    use Datatable;

    /**
     * Sets model and cache tag
     */
    public function beforeConstruct()
    {
        $this->model         = new Animal();
        $this->cache         = 'mutations';
        $this->relationships = ['complex', 'morphs', 'combo', 'species', 'user', 'subspecies', 'locality'];
        $this->required      = ['item_number'];
    }

    /**
     * Get one by item number
     * @param $itemNumber
     * @return mixed
     */
    public function getByItemNumber($itemNumber)
    {
        return $this->get($itemNumber, 'item_number');
    }
}