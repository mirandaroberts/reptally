<?php

namespace App\Services\Animal;

use App\Models\Animal\AnimalMorphComplex;
use App\Services\Service;

class AnimalMorphComplexService extends Service
{
    /**
     * Sets model and cache tag
     */
    public function beforeConstruct()
    {
        $this->model         = new AnimalMorphComplex();
        $this->cache         = 'animal_morph_complex';
        $this->relationships = ['species'];
        $this->required      = ['name', 'species_id'];
    }
}