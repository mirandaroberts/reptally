<?php

namespace App\Services\Animal;

use App\Models\Animal\AnimalLocality;
use App\Services\Service;

class AnimalLocalityService extends Service
{
    /**
     * Sets model and cache tag
     */
    public function beforeConstruct()
    {
        $this->model         = new AnimalLocality();
        $this->cache         = 'animal_locality';
        $this->relationships = ['species'];
        $this->required      = ['name', 'species_id', 'description'];
    }
}