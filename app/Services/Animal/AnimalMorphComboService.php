<?php

namespace App\Services\Animal;

use App\Models\Animal\AnimalMorphCombo;
use App\Services\Service;

class AnimalMorphComboService extends Service
{
    /**
     * Sets model and cache tag
     */
    public function beforeConstruct()
    {
        $this->model         = new AnimalMorphCombo();
        $this->cache         = 'animal_morph_combo';
        $this->relationships = ['species'];
        $this->required      = ['name', 'species_id', 'description', 'combo', 'lethal', 'deformities',
            'first_produced_by', 'first_produced_id', 'first_produced_date'];
    }

    /**
     * Get combo from gene array, if no combo exists, return a combination of morph names
     * @param $genes
     * @return string
     */
    public function getFromGenes($genes)
    {
        $array = array();
        foreach ($genes as $name => $gene) {
            if ($name === $gene->name) array_push($array, $gene->id);
            else if ($name === 'Super ' . $gene->name) {
                array_push($array, $gene->id);
                array_push($array, $gene->id);
            }
        }
        $combo = $this->get('{"morphs":[' . implode(',', $array) . ']}', 'combo');
        if ($combo) return $combo->name;
        else {
            $array = array();
            foreach ($genes as $name => $gene) array_push($array, $name);
            return implode(' ', $array);
        }
    }
}