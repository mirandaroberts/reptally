<?php

namespace App\Services\Animal;

use App\Models\Animal\AnimalSpecies;
use App\Models\Animal\AnimalSpeciesChangelog;
use App\Services\Service;
use App\Traits\Datatable;
use Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

class AnimalSpeciesService extends Service
{
    use Datatable;

    /**
     * @var AnimalMorphComplexService
     */
    public $complexService;

    /**
     * @var AnimalLocalityService
     */
    public $localityService;

    /**
     * @var AnimalMorphService
     */
    public $morphService;

    /**
     * @var AnimalMorphComboService
     */
    public $comboService;

    /**
     * @var AnimalSubspeciesService
     */
    public $subspeciesService;

    /**
     * AnimalSpeciesService constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->complexService    = new AnimalMorphComplexService();
        $this->localityService   = new AnimalLocalityService();
        $this->morphService      = new AnimalMorphService();
        $this->comboService      = new AnimalMorphComboService();
        $this->subspeciesService = new AnimalSubspeciesService();
    }

    /**
     * Sets model and cache tag
     */
    public function beforeConstruct()
    {
        $this->model         = new AnimalSpecies();
        $this->cache         = 'animal_species';
        $this->relationships = ['subspecies', 'morphs', 'combos', 'localities', 'changelog', 'complexes'];
        $this->required      = ['name', 'species_id', 'scientific_name', 'description', 'status'];
    }

    /**
     * Updates the change log
     * @param $species
     * @param $user
     * @param $method
     * @return mixed
     */
    public function updateChangelog($species, $user, $method)
    {
        return AnimalSpeciesChangelog::create([
            'species_id' => $species->id,
            'user_id'    => $user->id,
            'change'     => $method
        ]);
    }

    /**
     * Update subtables
     * @param $tableList
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateSubTables($tableList)
    {
        foreach ($tableList as $name => $tables) {
            switch ($name) {
                case 'complexes':
                    foreach ($tables as $table) {
                        if (!$complex = $this->complexService->get($table['id'])) return response()->json(['error' => 'Cannot find complex data.']);
                        $validator = Validator::make($table, ['name' => 'required|max:100', 'species_id' => 'required',]);
                        if ($validator->fails()) return response()->json(['error' => $validator->errors()->all()]);
                        if (!$this->complexService->update($complex, $table)) return response()->json(['error' => 'Could not update complex.']);
                    }
                    break;
                case 'localities':
                    foreach ($tables as $table) {
                        if (!$locality = $this->localityService->get($table['id'])) return response()->json(['error' => 'Cannot find locality data.']);
                        $validator = Validator::make($table, ['name' => 'required|max:100', 'species_id' => 'required',]);
                        if ($validator->fails()) return response()->json(['error' => $validator->errors()->all()]);
                        if (!$this->localityService->update($locality, $table)) return response()->json(['error' => 'Could not update locality.']);
                    }
                    break;
                case 'morphs':
                    foreach ($tables as $table) {
                        if (!$morph = $this->morphService->get($table['id'])) return response()->json(['error' => 'Cannot find morph data.']);
                        $validator = Validator::make($table, ['name' => 'required|max:100', 'species_id' => 'required',]);
                        if ($validator->fails()) return response()->json(['error' => $validator->errors()->all()]);
                        if (!$this->localityService->update($morph, $table)) return response()->json(['error' => 'Could not update morph.']);
                    }
                    break;
                case 'subspecies':
                    foreach ($tables as $table) {
                        if (!$subspecies = $this->subspeciesService->get($table['id'])) return response()->json(['error' => 'Cannot find subspecies data.']);
                        $validator = Validator::make($table, ['name' => 'required|max:100', 'species_id' => 'required',]);
                        if ($validator->fails()) return response()->json(['error' => $validator->errors()->all()]);
                        if (!$this->subspeciesService->update($subspecies, $table)) return response()->json(['error' => 'Could not update subspecies.']);
                    }
                    break;
                case 'combos':
                    foreach ($tables as $table) {
                        if (!$combo = $this->comboService->get($table['id'])) return response()->json(['error' => 'Cannot find combo data.']);
                        $validator = Validator::make($table, ['name' => 'required|max:100', 'species_id' => 'required',]);
                        if ($validator->fails()) return response()->json(['error' => $validator->errors()->all()]);
                        if (!$this->comboService->update($combo, $table)) return response()->json(['error' => 'Could not update combo.']);
                    }
                    break;
                default:
                    return response()->json(['error' => 'Invalid data sent.']);
                    break;
            }
        }
        $this->flushCache();
    }

    /**
     * Select for data tables
     * @return mixed
     */
    public function getForDataTables()
    {
        return Cache::tags($this->cache)->rememberForever('datatables', function () {
            return $this->model->select('id', 'name', 'scientific_name', 'status', 'habitat', 'care_level')->get();
        });
    }
}