<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ReputationDeducted extends Notification
{
    use Queueable;

    /**
     * @var
     */
    protected $amt;
    protected $action;
    protected $url = null;

    /**
     * ReputationGained constructor.
     * @param $amt
     * @param $action
     * @param null $url
     */
    public function __construct($amt, $action, $url = null)
    {
        $this->amt    = $amt;
        $this->action = $action;
        $this->url    = $url;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('The introduction to the notification.')
            ->action('Notification Action', url('/'))
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'amt'    => $this->amt,
            'action' => $this->action,
            'url'    => $this->url,
        ];
    }

    /**
     *
     * @param $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'amt'    => $this->amt,
            'action' => $this->action,
            'url'    => $this->url,
        ]);
    }
}
