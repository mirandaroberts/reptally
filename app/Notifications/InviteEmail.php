<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class InviteEmail extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var
     */
    protected $code;

    /**
     * Create a new notification instance.
     * @return void
     */
    public function __construct($code)
    {
        $this->code = $code;
    }

    /**
     * Get the notification's delivery channels.
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = url('/register/' . $this->code->code);
        return (new MailMessage)
            ->from('noreply@reptally.com')
            ->subject('You have been invited to join Reptally')
            ->line('Congratulations! You have been invited to try Reptally\'s closed beta membership. Click the 
                    button below to register your free account today.')
            ->action('Register for free now', $url)
            ->line('We hope to see you soon!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
