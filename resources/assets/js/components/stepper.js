$.widget('reptally.stepper', {

	options: {
		steps: null, // array - of step objects
	},

	templates: {
		stepper: {
			$html: $('<div class="stepper flex flow-vertical"></div>'),
			selectors: {
				ul: "ul"
			}
		},
		step: {
			$html:
				$('<div class="step flex flow-vertical">' +
					'<div class="step-header flex align-middle padded">' +
						'<div><div class="step-number badge blue"></div></div>' +
						'<div><div class="step-title h4">Default Step Title</div></div>' +
					'</div>' +
					'<div class="step-content padding"></div>' +
					'<div class="step-footer">' +
						'<div class="flex padded">' +
							'<div class="step-footer-prev"><a class="button btnPrev">Back</a></div>' +
							'<div calss="step-footer-next"><a class="button btnNext">Next</a></div>' +
						'</div>' +
					'</div>' +
				'</div>'),
			selectors: {
				stepNumber: ".step-number",
				stepTitle: ".step-title",
				stepContent: ".step-content",
				btnPrev: ".btnPrev",
				btnNext: ".btnNext",
			}
		}
	},

	elements: {
		$stepper: null
	},

	_create: function () {
		var $this = this;
		$this._render();
	},

	_render: function () {
		var $this = this;
		// Clear elements, for rerender
		$this.element.children().remove();
		$this.elements.$stepper = $this.templates.stepper.$html.clone();
		$this._renderSteps();
		$this.element.append($this.elements.$stepper);
	},

	_renderSteps: function ($stepper) {
		var $this = this;
		$.each($this.options.steps, function(index, step) {
			$this._renderStep(step, index + 1);
		});
	},

	_renderStep: function (step, stepNumber) {
		var $this = this;
		var $step = $this.templates.step.$html.clone();
		var elements = {
			$stepNumber: $step.find($this.templates.step.selectors.stepNumber),
			$stepTitle: $step.find($this.templates.step.selectors.stepTitle),
			$stepContent: $step.find($this.templates.step.selectors.stepContent)
		};
		elements.$stepNumber.text(stepNumber);
		elements.$stepTitle.text(step.title);
		elements.$stepContent.append(step.content);
		if (step.active) {
			$step.addClass('active');
		}
		$this.elements.$stepper.append($step);
	}

});