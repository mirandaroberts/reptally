$.widget('reptally.menu', {

	options: {
		drilldown: false,
	},

	_create: function() {
		var $this = this;
		$this._render();
	},

	_render: function() {
		var $this = this;
		var $lis = $this.element.find('li');
		var $ulRoot = $this.element.children('ul');
		
		if ($this.options.drilldown) {
			$this.element.addClass("drilldown");
			var width = $ulRoot.outerWidth();
			var height = $ulRoot.outerHeight();
			$this.element.width(width);
			$this.element.height(height);
		}

		$lis.click(function() {
			var $li = $(this);
			var $ul = $li.children('ul');
			$li.toggleClass('active');

			if ($this.options.drilldown) {
				var width = $ul.outerWidth();
				var height = $ul.outerHeight();
				$this.element.width(width);
				$this.element.height(height);
			}

			var position = $ul.position().left;
			$ulRoot.css('left', -position);
			
			if ($ul.children('.btnBack').length == 0) {
				$ul.prepend('<a class="button btnBack square">' + icons.chevron + '<span>Back</span></a>');
			}
		});
	},

});