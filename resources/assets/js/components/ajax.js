function ajax(overrideOptions) {

	var options = {
		url: null,
		data: null,
		success: null,
	};

	$.extend(options, overrideOptions);

	$.ajax({
	    url: options.url,
	    method: "POST",
	    data: options.data,
	    success: function (data) {
	    	if (options.success !== null) {
	    		options.success(data);
	    	}
	    },
	    error: function () {
	    	$('<div class="modalAjaxError">' +
	    		'<h4>Web Service Error</h4>' +
	    		'<p>Wut-wohw looks like someone made a programming error in a web service method or ajax request.</p>' +
	    	'</div>');
	    },
	    beforeSend: function (request) {
	        request.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
	    }
	});

}