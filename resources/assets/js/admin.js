// Components
require('./components/icons');
require('./components/input');
require('./components/modal');
require('./components/images');
require('./components/menu');
require('./components/stepper');

window.$loader = $('<div class="loader-wrapper"><div class="loader"><svg viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle class="path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle></svg></div></div>');

window.camelCase = function (str) {
  return str.replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, function(match, index) {
    if (+match === 0) return ""; // or if (/\s+/.test(match)) for white spaces
    return index == 0 ? match.toLowerCase() : match.toUpperCase();
  });
}

window.upperCase = function (str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
}

$(function() {
	$('.input').each(function() { $(this).input(); });
	$('.tabs').tabs();
	$(document).tooltip();
	// $('select').selectize({
	// 	onItemAdd: function () {
	// 		this.$wrapper.addClass('hasItemSelected');
	// 	},
	// 	onItemRemove: function() {
	// 		this.$wrapper.removeClass('hasItemSelected');
	// 	}
	// });
});
