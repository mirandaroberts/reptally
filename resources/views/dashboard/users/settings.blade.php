@extends('layouts.dashboard')

@section('title')
    Account Settings - Reptally
@endsection

@section('content')
    <div class="section">
        <div class="max">
            <h1>Account Settings</h1>
            <h2>Update your account settings</h2>

            <div class="max overflow-hidden padding-top-x4">
                <form method="post" action="">
                    @csrf
                    <div class="flex padded-x4">
                        <div>
                            <div class="flex padded flow-vertical">
                                <div>
                                    <div class="input text block">
                                        <label>First Name</label>
                                        <input type="text" class="text" value="{{ Auth::user()->first_name }}" />
                                    </div>
                                </div>
                                <div>
                                    <div class="input text block">
                                        <label>Last Name</label>
                                        <input type="text" class="text" value="{{ Auth::user()->last_name }}" />
                                    </div>
                                </div>
                                <div>
                                    <div class="input text block">
                                        <label>Email</label>
                                        <input type="email" class="text" value="{{ Auth::user()->email }}" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection