@extends('layouts.dashboard')

@section('title')
    Dashboard - Reptally
@endsection

@section('content')
    @if (Auth::user()->isVerified)
        <div>
            <div class="padding shadow relative z-1">
                <div class="max">
                    <div class="flex padded align-middle">
                        <div class="box">
                            <div class="h4">Edit Animal</div>
                        </div>
                        <div>
                            <div class="dropdown">
                                <a class="button secondary">
                                    <svg class="icon-ellipsis rotate-90"><use xlink:href="#icon-ellipsis"></use></svg>
                                </a>
                                <div class="menu">
                                    <ul>
                                        <li>
                                            <a>
                                                <svg class="icon-upload"><use xlink:href="#icon-upload"></use></svg>
                                                <span>Export</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a>
                                                <svg class="icon-download"><use xlink:href="#icon-download"></use></svg>
                                                <span>Import</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div>
                            <a class="button">
                                <svg class="icon-save"><use xlink:href="#icon-save"></use></svg>
                                <span>Save</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="padding-bottom-x4">
                <div class="tabs">
                    <div class="max">
                        <ul>
                            <li><a href="#tabDetails">Details</a></li>
                            <li><a href="#tabPhotos">Photos</a></li>
                            <li><a href="#tabDescription">Description</a></li>
                        </ul>
                    </div>
                    <div id="tabDetails">
                        <div class="max overflow-hidden padding-top-x4">
                        <div class="flex padded-x4">
                            <div>
                                <div class="flex padded flow-vertical">
                                    <div>
                                        <div class="input text block">
                                            <label>Nickname</label>
                                            <input type="text" class="text" value="Andrew" />
                                        </div>
                                    </div>
                                    <div>
                                        <div class="input text block">
                                            <label>Item Number</label>
                                            <input type="text" class="text" value="123" />
                                        </div>
                                    </div>
                                    <div>
                                        <div class="input select">
                                            <label>Species</label>
                                            <select>
                                                <option></option>
                                                <option selected value="1">Python</option>
                                                <option value="2">Boa</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="input select">
                                            <label>Morphs</label>
                                            <select multiple>
                                                <option></option>
                                                <option selected value="1">Albino</option>
                                                <option selected value="2">Pied</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="flex padded flow-vertical">
                                    <div>
                                        <div class="input text block">
                                            <label>Price</label>
                                            <input type="text" class="text" value="$100.00" />
                                        </div>
                                    </div>
                                    <div>
                                        <div class="input select">
                                            <label>Gender</label>
                                            <select>
                                                <option></option>
                                                <option value="1" selected>Male</option>
                                                <option value="2">Female</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="input text">
                                            <label>Birthdate</label>
                                            <input type="text" class="text date" value="1/1/2017" />
                                        </div>
                                    </div>
                                    <div>
                                        <div class="input select">
                                            <label>Father</label>
                                            <select>
                                                <option></option>
                                                <option selected value="1">Tom (#123)</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="input select">
                                            <label>Mother</label>
                                            <select>
                                                <option></option>
                                                <option selected value="1">Luicy (#321)</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="input toggle">
                                <input type="checkbox" id="cb1" />
                                <label for="cb1">Toggle</label>
                            </div>
                        </div>
                        <div>
                            <div class="input checkbox">
                                <input type="checkbox" id="cb2" />
                                <label for="cb2">Checkbox</label>
                            </div>
                        </div>
                        <div>
                            <div class="input radio">
                                <input type="radio" id="rb1" name="rb1" />
                                <label for="rb1">Radio 1</label>
                            </div>
                            <div class="input radio">
                                <input type="radio" id="rb2" name="rb1" />
                                <label for="rb2">Radio 2</label>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div id="tabPhotos">
                        <div class="max overflow-hidden padding-top-x4">
                            <div class="imageGrid">
                            </div>
                            <script>$(function() {$('.imageGrid').images();});</script>
                        </div>
                    </div>
                    <div id="tabDescription">
                        Description
                    </div>
                </div>
            </div>
            <div class="padding shadow relative z-1">
                <div class="max overflow-hidden padding-top-x4">
                    <div class="flex padded align-middle">
                        <div class="box">
                            <div class="h2">Animals</div>
                        </div>
                        <div>
                            <div class="dropdown anchor-right">
                                <a class="button secondary">
                                    <svg class="icon-ellipsis rotate-90"><use xlink:href="#icon-ellipsis"></use></svg>
                                </a>
                                <div class="menu">
                                    <ul>
                                        <li>
                                            <a>
                                                <svg class="icon-upload"><use xlink:href="#icon-upload"></use></svg>
                                                <span>Export</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a>
                                                <svg class="icon-download"><use xlink:href="#icon-download"></use></svg>
                                                <span>Import</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div>
                            <a class="button">
                                <svg class="icon-plus"><use xlink:href="#icon-plus"></use></svg>
                                <span>New Animal</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="max padding-top-x4">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Photo</th>
                        <th>Name</th>
                        <th>Tags</th>
                        <th>ID</th>
                        <th class="td-static"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @for ($i = 0; $i < 10; $i++)
                        <tr>
                            <td><img src="http://placehold.it/48x48" /></td>
                            <td>Snek</td>
                            <td><div class="tag bg-green color-white">Albino</div> <div class="tag bg-purple color-white">Pied</div></td>
                            <td>1</td>
                            <td class="td-static"><a class="button"><svg class="icon-edit"><use xlink:href="#icon-edit"></use></svg></a>
                        </tr>
                    @endfor
                    </tbody>
                </table>
            </div>
        </div>
    @else
        <div>
            <div class="padding shadow fullscreen relative z-1">
                <div class="max">
                    <h1>Verify your account</h1>
                    <p>
                        You must verify your email before you can use Reptally. An email has been sent to
                        <b>{{ Auth::user()->email }}</b> with your activation code. If you are having problems receiving
                        your activation email, you can instead email us at support@reptally.com from the email registered
                        on your account.
                    </p>

                    <a href="/verify/resend" class="button">Resend Verification Email</a>
                </div>
            </div>
        </div>
    @endif
@endsection