@extends('layouts.admin')

@section('title')
    Animals - Reptally
@endsection

@section('content')
    <div class="z-1 body">
        <div class="flex">
            <div class="box relative">
                <div class="padding shadow relative z-1">
                    <div class="max">
                        <div class="flex padded align-middle">
                            <div class="box">
                                <div class="h4">Animals</div>
                            </div>
                            <div>
                                <div class="dropdown anchor-right">
                                    <a class="button">
                                        {!! $icon['gear'] !!}
                                    </a>
                                </div>
                            </div>
                            <div>
                                <a class="button btnAddAnimal">
                                    {!! $icon['add'] !!}
                                    <span>Add Animal</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="padding">
                    <div class="max padding-top-x2 padding-bottom-x2">
                        @if ($tutorial)
                            No animals have been made yet, so we will show a cool tutorial here
                        @endif
                        <table class="table" id="tblAnimals">
                            <thead>
                            <tr>
                                <th>Item Number</th>
                                <th>Name</th>
                                <th>Species</th>
                                <th>Subspecies</th>
                                <th>Locality</th>
                                <th>Morphs</th>
                                <th>Gender</th>
                                <th>Birth Date</th>
                                <th>Public</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // Set up the form
        var form = {
            type: "animals",
            tabs: [
                {
                    fields: [
                        {
                            label: "Item Number",
                            type: "text",
                            required: true,
                            id: "item_number",
                            options: null
                        }, {
                            label: "Name",
                            type: "text",
                            required: false,
                            id: "name",
                            options: null
                        }, {
                            label: "Species",
                            type: "select",
                            required: true,
                            id: "species",
                            options: null
                        }, {
                            label: "Subspecies",
                            type: "select",
                            required: false,
                            id: "subspecies",
                            options: null
                        }, {
                            label: "Locality",
                            type: "select",
                            required: false,
                            id: "locality",
                            options: null
                        }, {
                            label: "Morphs",
                            type: "select",
                            required: false,
                            id: "morphs",
                            options: null
                        }, {
                            label: "Gender",
                            type: "select",
                            required: true,
                            id: "gender",
                            options: null
                        }, {
                            label: "Birth Date",
                            type: "date",
                            required: false,
                            id: "birth_date",
                            options: null
                        }, {
                            label: "Public?",
                            type: "checkbox",
                            required: false,
                            id: "public",
                            options: null
                        }
                    ],
                    text: "Details",
                    id: "details",
                    icon: null,
                    template: (
                        '<div class="padding-x2">' +
                        '<div class="flex padded flow-vertical">' +
                        '<div>' +
                        '<field id="item_number" />' +
                        '</div>' +
                        '<div>' +
                        '<field id="name" />' +
                        '</div>' +
                        '<div>' +
                        '<field id="species" />' +
                        '</div>' +
                        '<div>' +
                        '<field id="subspecies" />' +
                        '</div>' +
                        '<div>' +
                        '<field id="locality" />' +
                        '</div>' +
                        '<div>' +
                        '<field id="morphs" />' +
                        '</div>' +
                        '<div>' +
                        '<field id="birth_date" />' +
                        '</div>' +
                        '<div>' +
                        '<field id="gender" />' +
                        '</div>' +
                        '<div>' +
                        '<field id="public" />' +
                        '</div>' +
                        '</div>' +
                        '</div>'
                    )
                }, {
                    fields: [
                        {
                            label: "Sale Price",
                            type: "text",
                            required: false,
                            id: "sale_price",
                            options: null,
                            default: '0'
                        }, {
                            label: "Description",
                            type: "wysiwyg",
                            required: false,
                            id: "description",
                            options: null,
                        },
                    ],
                    text: "Sale Info",
                    id: "sale_info",
                    icon: null,
                    template: (
                        '<div class="padding-x4">' +
                        '<field id="description" />' +
                        '</div>'
                    )
                },

            ]
        };

        // Retrieves data for the table
        var $tblAnimals = $('#tblAnimals');
        $tblAnimals.table({
            url: window.location.href + "/fetch-all",
            expandUrl: window.location.href + "/fetch",
            expandForm: form,
            columns: [
                { "data": "item_number", "name": "item_number" },
                { "data": "name", "name": "name" },
                { "data": "species", "name": "species" },
                { "data": "subspecies", "name": "subspecies" },
                { "data": "locality", "name": "locality" },
                { "data": "morphs", "name": "morphs" },
                { "data": "gender", "name": "gender" },
                { "data": "birth_date", "name": "birth_date" },
                { "data": "public", "name": "public" },
                // { "data": "image", "name": "image" },
            ],
            type: "animals",
        });

        // Set up the add button
        $('.btnAddAnimal').click(function() {
            $.ajax({
                "url": "/dropdowns",
                "method": "POST",
                "data": { key: "allSpecies" },
                "success": function (data) {
                    var statuses = data;
                    var $modal = $('<div><div id="formAddAnimal"></div></div>');
                    var $form = $modal.find("#formAddAnimal");
                    $form.form({
                        type: "animals",
                        form: form
                    });
                    $modal.modal({
                        title: "Add Animal",
                        padding: false,
                        dialogClass: 'medium',
                        buttons: [
                            {
                                text: "Cancel",
                                "class": "btn-cancel button secondary",
                                click: function() {
                                    $(this).dialog('close');
                                }
                            },
                            {
                                text: "Save",
                                "class": "btn-save button",
                                click: function() {
                                    var data = $form.form('getData').fields;
                                    if (data.valid) {
                                        console.log(data);
                                        data._token = $('meta[name="csrf-token"]').attr('content');
                                        $.ajax({
                                            "url": window.location.href + "/create",
                                            "method": "POST",
                                            "data": data,
                                            "success": function (response) {
                                                console.log(response);
                                                $tblSpecies.table('addRow', data);
                                            },
                                            "beforeSend": function (request) {
                                                request.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
                                            }
                                        });
                                        $(this).dialog('close');
                                    }
                                }
                            }
                        ]
                    });
                },
                "beforeSend": function (request) {
                    request.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
                }
            });
        });
    </script>
@endsection