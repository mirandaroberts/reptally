<!DOCTYPE html>
<!--[if lte IE 6]><html class="preIE7 preIE8 preIE9"><![endif]-->
<!--[if IE 7]><html class="preIE8 preIE9"><![endif]-->
<!--[if IE 8]><html class="preIE9"><![endif]-->
<!--[if gte IE 9]><!--><html><!--<![endif]-->
<head>

    <!-- Meta data -->
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="author" content="Reptally">
    <meta name="description" content="Reptally is a reptile husbandry management software designed to streamline the
    record-keeping and sales process for breeders and offer free resources for hobbyists of all levels. Whether you are
    still shopping for your first pet or are a veteran breeder, Reptally will offer tools to make animal husbandry and
    sales easier.">
    <meta name="keywords" content="reptile,herp,breeding,reptally,snake,morphs,python,boa,classifieds">
    <title>@yield('title')</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="/favicon.ico" type="image/vnd.microsoft.icon">

    <!-- Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-114048180-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-114048180-1');
    </script>

    <!-- Style sheets -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" type="text/css">
    <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="//cdnjs.cloudflare.com/ajax/libs/froala-editor/2.7.6/css/froala_editor.min.css" rel="stylesheet">

    <meta name="google-site-verification" content="qrnu6IoRxsDFNCRyvIigBRlKuD5Uz-tSLhNT1b2o8Wg" />

    @yield('header')

</head>
<body>

    <div id="app">
        @include('layouts.partials._nav')
        @include('layouts.partials._alerts')
        @yield('content')
        @include('layouts.partials._footer')
    </div>

    <!-- External javascript libraries -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.5.3/modernizr.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/js/standalone/selectize.min.js" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/grapesjs/0.14.6/grapes.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/froala-editor/2.7.6/js/froala_editor.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/pluralize/7.0.0/pluralize.min.js"></script>

    <!-- Internal javascript library -->
    <script src="{{ asset('js/admin.js') }}"></script>

    <!-- Global inline scripts -->
    <script>
        $('#menuSidebar').menu({drilldown: true});
    </script>

    <!-- Page specific scripts -->
    @yield('scripts')

</body>
</html>