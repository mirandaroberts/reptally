@if (Auth::check())
    <div id="header" class="header shadow padding z-3 relative">
        <div class="max">
            <div class="flex nowrap padded align-middle">
                <div>
                    <a href="/"><img class="block" src="https://static.reptally.com/logo-bar-lt.png" alt="Reptally - Robust reptile tracking solution" width="200"/></a>
                </div>
                <div class="box">
                    <nav id="menuHeader1" class="menu horizontal">
                        <ul>
                            <li><a href="/almanac">Almanac</a></li>
                            <li><a href="/articles">Articles</a></li>
                            <li><a href="/calculator">Calculator</a></li>
                            <li><a href="/listings">Browse Animals</a></li>
                            <li><a href="/forums">Forums</a></li>
                        </ul>
                    </nav>
                </div>
                <div>
                    <nav id="menuHeader2" class="menu horizontal">
                        <ul>
                            @if(Auth::check())
                                <li>
                                    <div class="dropdown anchor-right tip-alt">
                                        <div class="profile-image">
                                            <img src="http://placehold.it/44x44" />
                                        </div>
                                        <div class="menu">
                                            <div class="bg-alt padding-x2 round-top">
                                                <div class="flex padded align-middle nowrap">
                                                    <div>
                                                        <div class="profile-image" style="width: 64px;">
                                                            <img src="http://placehold.it/64x64" />
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="flex flow-vertical">
                                                            <div class="h5">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</div>
                                                            <div class="subtext">{{ Auth::user()->email }}</div>
                                                            <div class="subtext">{{ Auth::user()->reputation_rank }} ({{ Auth::user()->reputation }})</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <ul>
                                                <li><a href="/dashboard">{!! $icon['gear'] !!}<span>Dashboard</span></a></li>
                                                <li><a href="/notifications">{!! $icon['gear'] !!}<span>Notifications</span></a></li>
                                                <li><a href="/settings">{!! $icon['gear'] !!}<span>Account Settings</span></a></li>
                                                <li><a href="/profile">{!! $icon['user'] !!}<span>Edit Profile</span></a></li>
                                                @if (Auth::user()->hasRole(['administrator', 'moderator']))
                                                    <li><a href="/admin">{!! $icon['user'] !!}<span>Admin Panel</span></a></li>
                                                @endif
                                                <li><hr></li>
                                                <li><a href="/logout">{!! $icon['logout'] !!}<span>Logout</span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            @else
                                <li><a href="/login">Login</a></li>
                                <li><a class="button" href="/register">Register</a></li>
                            @endif
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
@endif