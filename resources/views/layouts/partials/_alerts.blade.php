@if ($messages = Flash::flush())
    @foreach($messages as $message)
        @if(is_array($message['message']))
            @foreach($message['message'] as $m)
                <div class="alert {{ $message['type'] }} margin-10px">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ $m }}
                </div>
            @endforeach
        @else
            <div class="alert {{ $message['type'] }} margin-10px">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                {{ $message['message'] }}
            </div>
        @endif
    @endforeach
@endif

@if(session('flash'))
    <p>{{ session('flash') }}</p>
@endif