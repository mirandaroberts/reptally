<div class="footer padding-x2">
	<div class="max">
		<div class="flex align-middle">
			<div class="width-4">
				&copy; <script>document.write(new Date().getFullYear())</script> | <a href="/tos">Terms of Service</a>
				| <a href="/privacy">Privacy Policy</a> | <a href="/contact">Contact</a>
			</div>
			<div class="width-4">
				<a href="/"><img class="block center" src="https://static.reptally.com/logo-bar-lt.png" alt="Reptally - Robust reptile tracking solution" width="100"/></a>
			</div>
			<div class="width-4 align-right">
				<span class="inline-block align-middle">Made with</span>
				<svg class="icon-heart fill-red-hover" title="I ♥ You Miranda" style="height: 1rem; width: 1rem;"><use xlink:href="#icon-heart"></use></svg>
			</div>
		</div>
	</div>
</div>