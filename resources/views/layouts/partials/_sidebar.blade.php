<div class="bg-alt z-2 relative shadow overflow-hidden">
    <div class="menu" id="menuSidebar">
        <ul>
            <li><a>{!! $icon['folder'] !!}<span>Animals</span>{!! $icon['chevron'] !!}</a>
                <ul>
                    <li><a href="/admin/animals">{!! $icon['folder'] !!}<span>Manage Animals</span></a></li>
                    <li><a href="/admin/listings">{!! $icon['folder'] !!}<span>Manage Listings</span></a></li>
                    <li><a href="/admin/racks">{!! $icon['folder'] !!}<span>Manage Racks</span></a></li>
                    <li><a href="/admin/incubator">{!! $icon['folder'] !!}<span>Incubator</span></a></li>
                    @if (Auth::user()->hasRole(['administrator'])) <li><a href="/admin/animals/species">{!! $icon['folder'] !!}<span>Species</span></a></li> @endif
                    @if (Auth::user()->hasRole(['administrator','moderator'])) <li><a href="/admin/animals/updates">{!! $icon['folder'] !!}<span>Species Change Requests</span></a></li> @endif
                </ul>
            </li>

            @if (Auth::user()->hasRole(['administrator']))
                <li><a>{!! $icon['users'] !!}<span>Users</span>{!! $icon['chevron'] !!}</a>
                    <ul>
                        <li><a href="/admin/users">{!! $icon['users'] !!}<span>Manage Users</span></a></li>
                        <li><a href="/admin/users/plans">{!! $icon['users'] !!}<span>Manage User Plans</span></a></li>
                        <li><a href="/admin/users/roles">{!! $icon['users'] !!}<span>Manage User Roles</span></a></li>
                        <li><a href="/admin/users/invoicing">{!! $icon['users'] !!}<span>Invoicing</span></a></li>
                    </ul>
                </li>
            @endif
            <li><a>{!! $icon['gear'] !!}<span>Settings</span>{!! $icon['chevron'] !!}</a>
                <ul>
                    <li><a><span>Settings 1</span></a></li>
                    <li><a><span>Settings 2</span></a></li>
                    <li><a><span>Settings 3</span></a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>