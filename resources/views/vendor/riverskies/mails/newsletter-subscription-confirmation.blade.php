@component('mail::message')
# Happy days!

Thank you for subscribing to Reptally's newsletter.

If you wish to unsubscribe, you can do that [here]({{ url($subscription->unsubscribeUrl) }}) or by copying the below URL into your browser:

{{ url($subscription->unsubscribeUrl) }}

Thanks,<br>
Reptally
@endcomponent
