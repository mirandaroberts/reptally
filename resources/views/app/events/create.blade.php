@extends('layouts.app')

@section('title')
    Create Event - Reptally
@endsection

@section('content')
    <div class="section align-center">
        <h2>Create Event</h2>
        <p>Host a reptile event in your area</p>
    </div>
    <div class="section">
        <form method="POST" action="{{ route('register') }}" class="align-left">
            @csrf

            <div class="max">
                <div id="createEventStepper"></div>
            </div>
            <script>
                $("#createEventStepper").stepper({
                    steps: [
                        {
                            title: "Event Details",
                            content: "<p>Lorem ipsum</p>",
                            active: true,
                        },
                        {
                            title: "Event Validation",
                            content: "<p>Lorem ipsum</p>",
                        },
                        {
                            title: "Event Description",
                            content: "<p>Lorem ipsum</p>",
                        },
                        {
                            title: "Terms of Service",
                            content: "<p>Lorem ipsum</p>",
                        }
                    ],
                });
            </script>

            <!-- <h2>Step 1: Event Details</h2>
            <div>
                <div class="flex flow-vertical padded-x2">
                    <div>
                        <div class="input width-12 text {{ $errors->has('name') ? ' error' : '' }}">
                            <label>Event Name</label>
                            <input id="name" type="text" name="name" value="{{ old('name') }}" required autofocus>

                            @if ($errors->has('name'))
                                <span class="error">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div>
                            <div class="flex padded-x2">
                                <div>
                                    <div class="input date {{ $errors->has('start_date') ? ' error' : '' }}">
                                        <label>Start Date</label>
                                        <input id="start_date" type="date" name="start_date" value="{{ old('start_date') }}" required autofocus>

                                        @if ($errors->has('start_date'))
                                            <span class="error">
                                                <strong>{{ $errors->first('start_date') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div>
                                    <div class="input width-12 date {{ $errors->has('end_date') ? ' error' : '' }}">
                                        <label>End Date</label>
                                        <input id="end_date" type="date" name="start_date" value="{{ old('end_date') }}" required autofocus>

                                        @if ($errors->has('end_date'))
                                            <span class="error">
                                                <strong>{{ $errors->first('end_date') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="flex padded-x2">
                                <div>
                                    <div class="input time {{ $errors->has('start_time') ? ' error' : '' }}">
                                        <label>Start Time</label>
                                        <input id="start_time" type="time" name="start_time" value="{{ old('start_time') }}" required autofocus>

                                        @if ($errors->has('start_date'))
                                            <span class="error">
                                                <strong>{{ $errors->first('start_time') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div>
                                    <div class="input width-12 date {{ $errors->has('end_time') ? ' error' : '' }}">
                                        <label>End Time</label>
                                        <input id="end_time" type="time" name="end_time" value="{{ old('end_time') }}" required autofocus>

                                        @if ($errors->has('end_time'))
                                            <span class="error">
                                                <strong>{{ $errors->first('end_time') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="input width-12 text {{ $errors->has('address') ? ' error' : '' }}">
                            <label>Address</label>
                            <input id="address" type="text" name="address" value="{{ old('address') }}" required autofocus>

                            @if ($errors->has('address'))
                                <span class="error">
                                    <strong>{{ $errors->first('address') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div>
                            <div class="flex padded-x2">
                                <div>
                                    <div class="input text {{ $errors->has('zip_code') ? ' error' : '' }}">
                                        <label>Zip Code</label>
                                        <input id="zip_code" type="text" name="zip_code" value="{{ old('zip_code') }}" required autofocus>

                                        @if ($errors->has('zip_code'))
                                            <span class="error">
                                                <strong>{{ $errors->first('zip_code') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div>
                                    <div class="input text {{ $errors->has('state') ? ' error' : '' }}">
                                        <label>State</label>
                                        <select id="state" name="state" required autofocus>
                                            <option selected disabled>Select a State</option>
                                            <option value="AL" @if (old('state') == 'AL') selected @endif>Alabama</option>
                                            <option value="AK" @if (old('state') == 'AK') selected @endif>Alaska</option>
                                            <option value="AZ" @if (old('state') == 'AZ') selected @endif>Arizona</option>
                                            <option value="AR" @if (old('state') == 'AR') selected @endif>Arkansas</option>
                                            <option value="CA" @if (old('state') == 'CA') selected @endif>California</option>
                                            <option value="CO" @if (old('state') == 'CO') selected @endif>Colorado</option>
                                            <option value="CT" @if (old('state') == 'CT') selected @endif>Connecticut</option>
                                            <option value="DE" @if (old('state') == 'DE') selected @endif>Delaware</option>
                                            <option value="DC" @if (old('state') == 'DC') selected @endif>District Of Columbia</option>
                                            <option value="FL" @if (old('state') == 'FL') selected @endif>Florida</option>
                                            <option value="GA" @if (old('state') == 'GA') selected @endif>Georgia</option>
                                            <option value="HI" @if (old('state') == 'HI') selected @endif>Hawaii</option>
                                            <option value="ID" @if (old('state') == 'ID') selected @endif>Idaho</option>
                                            <option value="IL" @if (old('state') == 'IL') selected @endif>Illinois</option>
                                            <option value="IN" @if (old('state') == 'IN') selected @endif>Indiana</option>
                                            <option value="IA" @if (old('state') == 'IA') selected @endif>Iowa</option>
                                            <option value="KS" @if (old('state') == 'KS') selected @endif>Kansas</option>
                                            <option value="KY" @if (old('state') == 'KY') selected @endif>Kentucky</option>
                                            <option value="LA" @if (old('state') == 'LA') selected @endif>Louisiana</option>
                                            <option value="ME" @if (old('state') == 'ME') selected @endif>Maine</option>
                                            <option value="MD" @if (old('state') == 'MD') selected @endif>Maryland</option>
                                            <option value="MA" @if (old('state') == 'MA') selected @endif>Massachusetts</option>
                                            <option value="MI" @if (old('state') == 'MI') selected @endif>Michigan</option>
                                            <option value="MN" @if (old('state') == 'MN') selected @endif>Minnesota</option>
                                            <option value="MS" @if (old('state') == 'MS') selected @endif>Mississippi</option>
                                            <option value="MO" @if (old('state') == 'MO') selected @endif>Missouri</option>
                                            <option value="MT" @if (old('state') == 'MT') selected @endif>Montana</option>
                                            <option value="NE" @if (old('state') == 'NE') selected @endif>Nebraska</option>
                                            <option value="NV" @if (old('state') == 'NV') selected @endif>Nevada</option>
                                            <option value="NH" @if (old('state') == 'NH') selected @endif>New Hampshire</option>
                                            <option value="NJ" @if (old('state') == 'NJ') selected @endif>New Jersey</option>
                                            <option value="NM" @if (old('state') == 'NM') selected @endif>New Mexico</option>
                                            <option value="NY" @if (old('state') == 'NY') selected @endif>New York</option>
                                            <option value="NC" @if (old('state') == 'NC') selected @endif>North Carolina</option>
                                            <option value="ND" @if (old('state') == 'ND') selected @endif>North Dakota</option>
                                            <option value="OH" @if (old('state') == 'OH') selected @endif>Ohio</option>
                                            <option value="OK" @if (old('state') == 'OK') selected @endif>Oklahoma</option>
                                            <option value="OR" @if (old('state') == 'OR') selected @endif>Oregon</option>
                                            <option value="PA" @if (old('state') == 'PA') selected @endif>Pennsylvania</option>
                                            <option value="RI" @if (old('state') == 'RI') selected @endif>Rhode Island</option>
                                            <option value="SC" @if (old('state') == 'SC') selected @endif>South Carolina</option>
                                            <option value="SD" @if (old('state') == 'SD') selected @endif>South Dakota</option>
                                            <option value="TN" @if (old('state') == 'TN') selected @endif>Tennessee</option>
                                            <option value="TX" @if (old('state') == 'TX') selected @endif>Texas</option>
                                            <option value="UT" @if (old('state') == 'UT') selected @endif>Utah</option>
                                            <option value="VT" @if (old('state') == 'VT') selected @endif>Vermont</option>
                                            <option value="VA" @if (old('state') == 'VA') selected @endif>Virginia</option>
                                            <option value="WA" @if (old('state') == 'WA') selected @endif>Washington</option>
                                            <option value="WV" @if (old('state') == 'WV') selected @endif>West Virginia</option>
                                            <option value="WI" @if (old('state') == 'WI') selected @endif>Wisconsin</option>
                                            <option value="WY" @if (old('state') == 'WY') selected @endif>Wyoming</option>
                                        </select>

                                        @if ($errors->has('state'))
                                            <span class="error">
                                                <strong>{{ $errors->first('state') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="input width-12 text {{ $errors->has('price') ? ' error' : '' }}">
                            <label>Price of Admission</label>
                            <input id="price" type="text" name="price" value="{{ old('price') }}" required autofocus>

                            @if ($errors->has('price'))
                                <span class="error">
                                    <strong>{{ $errors->first('price') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <hr>

            <h2>Step 2: Event Validation</h2>
            <div>
                <p class="justify">
                    In order to maintain quality, protect our users from spam or misleading events, and prevent duplicate events,
                    Reptally requires that event holders provide documentation that proves that they are the genuine event manager.
                    Any documents provided will not be shared or used beyond this event acceptance process. <b>Do not submit an
                    event in which you are not the owner or manager. Instead, direct the event manager to do so.</b>
                </p>

                <div>
                    <b>Acceptable Validation:</b>
                    <ol>
                        <li>Scan of valid state ID and proof of venue reservation</li>
                        <li>An email from an official event email address (must be verifiable) to events@reptally.com</li>
                    </ol>
                </div>

                <div class="input width-12 file {{ $errors->has('files') ? ' error' : '' }}">
                    <label>Document Upload</label>
                    <input id="files" type="file" name="files" value="{{ old('files') }}">

                    @if ($errors->has('files'))
                        <span class="error">
                            <strong>{{ $errors->first('files') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="input textarea {{ $errors->has('validation') ? ' error' : '' }}">
                    <label>Additional Comments</label>
                    <textarea id="validation" name="validation">{{ old('validation') }}</textarea>
                    <small>Ex. Email sent from event@reptile-expo.com</small>

                    @if ($errors->has('validation'))
                        <span class="error">
                            <strong>{{ $errors->first('validation') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <hr>

            <h3>Step 3: Event Description</h3>
            <div>
                <div class="wysiwyg" id="description"></div>
            </div>

            <hr>

            <h3>Step 4: Terms of Service</h3>
            <div>
                <h5>By submitting you agree that...</h5>
                <ul>
                    <li>This event was created or managed by you.</li>
                    <li>This event is reptile related and compliant with our <a href="/tos">Terms of Service</a>.</li>
                    <li>This event is not misleading, exploitative, or unlawful in any way.</li>
                </ul>

                <div>
                    <button type="submit" class="button">
                        Agree and submit
                    </button>
                </div>
            </div> -->
        </form>
    </div>
@endsection