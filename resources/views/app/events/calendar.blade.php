@extends('layouts.app')

@section('title')
    Events Calendar - Reptally
@endsection

@section('header')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="section align-center">
        <h2>Events Calendar</h2>
        <p>Find reptile events and expos near you</p>
    </div>
    <div class="section" id="calculator">
        @if (auth()->check())
            <div class="align-right">
                <a href="/events/create" class="button">Create Event</a>
            </div>
        @endif
        <div class="max align-center">
            <div id='calendar'></div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.js'></script>

    <script>
        $(function() {

            // page is now ready, initialize the calendar...

            $('#calendar').fullCalendar({
                // put your options and callbacks here
            })

        });
    </script>
@endsection