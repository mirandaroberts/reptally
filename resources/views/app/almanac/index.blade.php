@extends('layouts.app')

@section('title')
    Browse reptile almanac - Reptally
@endsection

@section('content')
    <div class="bg-alt section">
        <div class="max align-center">
            <h2>Almanac</h2>
        </div>
    </div>

    <div class="max">
        <div class="flex">
            <div>
                <div class="menu">
                    <ul>
                        @foreach ($speciesList as $species)
                            <li>
                                <a href="/almanac/{{ $species->name }}">
                                    <span>{{ $species->name }}</span>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="box border-left padding-x4">
                The sidebar could contain thinks like a list of random almanac pages (linked like above) and recently updated
                 and other like things to keep users clicking on more stuff. Maybe the search lives here too idc.
            </div>
        </div>
    </div>
@endsection