@extends('layouts.app')

@section('title')
    {{ $species->name }} - Reptally
@endsection

@section('content')
	<div class="section">
		<div class="max">
			<h2>{{ $species->name }}</h2>
			<a href="/almanac">Back to almanac</a>
		</div>
	</div>
	<div class="section">
		<div class="max">
			<div class="flex padded-x4 flow-vertical">
				<div>
					<div class="flex padded">
						<div>
							<img src="http://placehold.it/200x200" />
						</div>
						<div class="box">
							<h4>Lorem Ipsum</h4>
				    		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				    		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				    		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				    		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				    		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				    		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				    	</div>
				    </div>
				</div>
				<div>
					<h4>Lorem Ipsum</h4>
		    		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		    		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		    		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		    		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		    		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		    		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		    		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		    		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		    		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		    		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		    		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		    		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		    	</div>
			</div>
		</div>
	</div>
@endsection