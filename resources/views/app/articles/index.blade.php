@extends('layouts.app')

@section('title')
    Browse reptile articles - Reptally
@endsection

@section('content')
    <div class="bg-alt section">
        <div class="max align-center">
            <h2>Rainbow Boa {!! $icon['male'] !!}</h2>
        </div>
    </div>
    <div class="section">
        <div class="max">
            <div class="flex padded align-middle flow-vertical">
                <div>
                    <div class="flex padded">
                        <div>
                            <div class="flex padded flow-vertical">
                                <div>
                                    <img src="http://placehold.it/400x400" />
                                </div>
                                <div>
                                    <div class="flex padded">
                                        <div>
                                            <img src="http://placehold.it/48x48" />
                                        </div>
                                        <div>
                                            <img src="http://placehold.it/48x48" />
                                        </div>
                                        <div>
                                            <img src="http://placehold.it/48x48" />
                                        </div>
                                        <div>
                                            <img src="http://placehold.it/48x48" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="flex padded flow-vertical">
                                <div>
                                    <div class="input text">
                                        <label>Species</label>
                                        <input type="text" value="Rainbow Boa" />
                                    </div>
                                </div>
                                <div>
                                    <div class="input select">
                                        <label>Morphs</label>
                                        <select multiple><option selected>Albino</option><option selected>Pastel</option></select>
                                    </div>
                                </div>
                                <div>
                                    <div class="input text">
                                        <label>Birth</label>
                                        <input type="text" value="1/1/2018" />
                                    </div>
                                </div>
                                <div>
                                    <div class="input text">
                                        <label>Weight</label>
                                        <input type="text" value="100g" />
                                    </div>
                                </div>
                                <div>
                                    <div class="input text">
                                        <label>Prey</label>
                                        <input type="text" value="Live Rat" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section">
        <div class="max">
            <div class="flex padded-x2">
                <div>
                    <h4>More From <a href="#">MiraculousMorphs</a></h4>
                </div>
                <div>
                    <div class="flex padded-x2">
                        @for ($i = 0; $i < 4; $i++)
                            <div class="xlarge-3">
                                <div class="shadow round">
                                    <div class="round-top cover" style="height: 150px; background-image: url('http://placehold.it/400x200');" class="cover"></div>
                                    <div class="bg-white padding-x2 round-bottom">
                                        <div class="flex padded">
                                            <div class="width-12">
                                                <div class="flex padded">
                                                    <div class="box"><h5>Snake Example One</h5></div>
                                                    <div>{!! $icon['male'] !!}</div>
                                                </div>
                                            </div>
                                            <div class="width-12">
                                                <div class="flex padded">
                                                    <div class="box"><a href="#">Morphmon</a></div>
                                                    <div><span class="money">$250.00</span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endfor
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bg-alt section" id="articles">
        <div class="max">
            <div class="flex padded-x2 flow-vertical">
                <div>
                    <h2>Animal List</h2>
                </div>
                <div>
                    <div class="flex padded align-end">
                        <div class="box">
                            <div class="input text">
                                <label>Search</label>
                                <input type="text" />
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="flex padded-x2">
                        @for ($i = 0; $i < 6; $i++)
                            <div class="xlarge-3">
                                <div class="shadow round">
                                    <div class="round-top cover" style="height: 150px; background-image: url('http://placehold.it/400x200');" class="cover"></div>
                                    <div class="bg-white padding-x2 round-bottom">
                                        <div class="flex padded">
                                            <div class="width-12">
                                                <div class="flex padded">
                                                    <div class="box"><h5>Snake Example One</h5></div>
                                                    <div>{!! $icon['male'] !!}</div>
                                                </div>
                                            </div>
                                            <div class="width-12">
                                                <div class="flex padded">
                                                    <div class="box"><a href="#">Morphmon</a></div>
                                                    <div><span class="money">$250.00</span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endfor
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section bg-alt">
        <div class="max">
            <div class="flex align-center">
                <div>

                </div>
            </div>
        </div>
    </div>
    <div class="section bg-alt" id="create-article">
        <div class="max">
            <h2>Create Article</h2>
            <div class="flex padded flow-vertical">
                <div>
                    <div class="input text block">
                        <label>Article Title</label>
                        <input type="text" class="text" />
                    </div>
                </div>
                <div>
                    <div class="input select">
                        <label>Species (optional)</label>
                        <select>
                            <option></option>
                            <option selected value="1">Python</option>
                            <option value="2">Boa</option>
                        </select>
                    </div>
                </div>
                <div>
                    <div class="input select">
                        <label>Tags</label>
                        <select multiple>
                            <option></option>
                            <option selected value="1">Husbandry</option>
                            <option selected value="2">Ball Pythons</option>
                        </select>
                    </div>
                </div>
                <div>
                    <div class="input wysiwyg width-12">
                        <label>Body</label>
                        <div class="wysiwyg">wysiwyg editor here with image and video functionality.</div>
                    </div>
                </div>
                <div>
                    <div class="input checkbox">
                        <input type="checkbox" id="cb2" />
                        <label for="cb2">This article is reptile-focused and compliant with our <a href="">Terms of Service</a></label>
                    </div>
                </div>
                <div>
                    <a class="button" href="">Submit Article</a>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-alt section" id="articles">
        <div class="max">
            <div class="flex padded-x2 flow-vertical">
                <div>
                    <h2>Articles</h2>
                </div>
                <div>
                    <div class="flex padded align-end">
                        <div class="box">
                            <div class="input text">
                                <label>Search</label>
                                <input type="text" />
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="flex padded-x2">
                        @for ($i = 0; $i < 6; $i++)
                            <div class="xlarge-4">
                                <div class="shadow round">
                                    <div class="round-top cover" style="height: 150px; background-image: url('http://placehold.it/400x200');" class="cover"></div>
                                    <div class="bg-white padding-x2 round-bottom">
                                        <h5><a href="#">Example article title</a></h5>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat.</p>
                                    </div>
                                </div>
                            </div>
                        @endfor
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')  
    <script>
        $(function() {
            $('.wysiwyg').froalaEditor();
        });
    </script>
@endsection