@extends('layouts.app')

@section('title')
    Login to Reptally
@endsection

@section('content')
    <div class="fullscreen">
        <div class="wallpaper" style="background-image: url('{{ $wallpaper }}');"></div>
        <div class="absolute center-middle align-center">
            <div class="bg-white shadow round padding-x4 align-left">
                <img src="https://static.reptally.com/logo-box.png" alt="Reptally logo" width="200">
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="flex flow-vertical padded-x2">
                        <div>
                            <div class="input text">
                                <label>Email address</label>
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div>
                            <div class="input text">
                                <label>Password</label>
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div>
                            <div class="input checkbox">
                                <input id="cbRememberMe" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                <label for="cbRememberMe">Remember Me</label>
                            </div>
                        </div>
                        <div>
                            <button type="submit" class="button">
                                Login
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <a class="sub margin-top block color-white" href="{{ route('password.request') }}">
                Forgot Your Password?
            </a>
        </div>
    </div>
@endsection
