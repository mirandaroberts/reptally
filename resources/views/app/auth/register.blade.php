@extends('layouts.app')

@section('title')
    Create your free Reptally account
@endsection

@section('navbar')
    @include('layouts.partials._nav')
@endsection

@section('content')
    <div class="fullscreen">
        <div class="wallpaper" style="background-image: url('{{ $wallpaper }}');"></div>
        <div class="absolute center-middle align-center">
            <div class="bg-white shadow round padding-x4">
                <img src="https://static.reptally.com/logo-box.png" alt="Reptally logo" width="200">
                <form method="POST" action="{{ route('register') }}" class="align-left">
                    @csrf
                    <div class="flex flow-vertical padded-x2">
                        <div>
                            <div class="input width-12 text {{ $errors->has('code') ? ' error' : '' }}">
                                <label>Invite Code</label>
                                <input id="code" type="text" name="code" value="{{ old('code', $invite) }}" required autofocus>

                                @if ($errors->has('code'))
                                    <span class="error">
                                        <strong>{{ $errors->first('code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div>
                            <div class="input width-12 text {{ $errors->has('email') ? ' error' : '' }}">
                                <label>Email</label>
                                <input id="email" type="email" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="error">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div>
                            <div class="flex padded-x2">
                                <div>
                                    <div class="input text {{ $errors->has('first_name') ? ' error' : '' }}">
                                        <label>First Name</label>
                                        <input id="first_name" type="text" name="first_name" value="{{ old('first_name') }}" required autofocus>

                                        @if ($errors->has('first_name'))
                                            <span class="error">
                                                <strong>{{ $errors->first('first_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div>
                                    <div class="input text {{ $errors->has('last_name') ? ' error' : '' }}">
                                        <label>Last Name</label>
                                        <input id="last_name" type="text" name="last_name" value="{{ old('last_name') }}" required autofocus>

                                        @if ($errors->has('last_name'))
                                            <span class="error">
                                                <strong>{{ $errors->first('last_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="flex padded-x2">
                                <div>
                                    <div class="input text {{ $errors->has('password') ? ' error' : '' }}">
                                        <label>Password</label>
                                        <input id="password" type="password" name="password" required autofocus>

                                        @if ($errors->has('password'))
                                            <span class="error">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div>
                                    <div class="input text {{ $errors->has('password_confirmation') ? ' error' : '' }}">
                                        <label>Confirm Password</label>
                                        <input id="password_confirmation" type="password" name="password_confirmation" required autofocus>

                                        @if ($errors->has('password_confirmation'))
                                            <span class="error">
                                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="input checkbox">
                                <input id="agree" type="checkbox" name="agree">
                                <label for="agree">I agree to Reptally's <a id="btnTos">Terms of Service</a></label>

                                @if ($errors->has('agree'))
                                    <span class="error">
                                        <strong>{{ $errors->first('agree') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div>
                            <button type="submit" class="button">
                                Register
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <a class="sub margin-top block color-white" href="{{ route('login') }}">
                Already have an account? Log in here.
            </a>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function() {
            $('#btnTos').click(function(e) {
                e.preventDefault();
                $('<div id="modalTos"><p>Lorem Ipsum</p></div>').dialog({
                    title: 'Terms of Service',
                    modal: true
                });
            });
        });
    </script>
@endsection