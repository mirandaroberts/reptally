@extends('layouts.app')

@section('title')
    Privacy Policy - Reptally
@endsection

@section('content')
    <div class="section align-center">
        <h2>Privacy Policy</h2>
    </div>
    <div class="section max justify">
        <ol>
            <li>
                <h4>INTRODUCTION</h4>

                <ol>
                    <li>
                        We are committed to the privacy and security of our Users.
                    </li>
                    <li>
                        "Personal data" is defined in Article 4(1) of the GDPR:

                        <p>
                            "(1) 'personal data' means any information relating to an identified or identifiable
                            natural person ('data subject'); an identifiable natural person is one who can be identified,
                            directly or indirectly, in particular by reference to an identifier such as a name, an
                            identification number, location data, an online identifier or to one or more factors
                            specific to the physical, physiological, genetic, mental, economic, cultural or social
                            identity of that natural person".
                        </p>
                    </li>
                    <li>
                        This policy applies where we are acting as a data controller with respect to the
                        personal data of our Users; in other words, where we determine the purposes and means
                        of the processing of that personal data.
                    </li>
                    <li>
                        We use cookies on our website. Insofar as those cookies are not strictly necessary
                        for the provision of our website and services, we will ask you to consent to our use
                        of cookies when you first visit our website.
                    </li>
                    <li>
                        Our website incorporates privacy controls which effect how we will process your
                        personal data. By using the privacy controls, you can specify whether you would
                        like to receive direct messages and limit the publication of your information.
                        You can access the privacy controls from your account <a href="/settings">settings</a>.
                    </li>
                    <li>
                        In this policy, "we", "us" and "our" refer to Reptally and its websites.
                    </li>
                </ol>
            </li>
            <li>
                <h4>CREDIT</h4>

                <ol>
                    <li>
                        This document was created using a template from <a href="https://seqlegal.com">SEQ Legal</a>.
                    </li>
                </ol>
            </li>
            <li>
                <h4>HOW WE USE YOUR PERSONAL DATA</h4>

                <ol>
                    <li>
                        We may process data about your use of our website and services ("usage data"). The
                        usage data may include your IP address, geographical location, browser type and version,
                        operating system, referral source, length of visit, page views, and website navigation paths,
                        as well as information about the timing, frequency and pattern of your service use. The source
                        of the usage data is Google Analytics. This usage data may be processed for the purpose of
                        analyzing the use of the website and services. The legal basis for this processing is our
                        legitimate interests, namely monitoring and improving our website and services.
                    </li>
                    <li>
                        We may process your account data ("account data"). The account data may include, but is not limited to, your
                        first and last name and email address. The account data may be processed for
                        the purpose of operating our website and services, maintaining back-ups of our database
                        and communicating with you. The legal basis for processing is our legitimate interests,
                        namely proper administration of our website and services.
                    </li>
                    <li>
                        We may process your information included in your personal profile on our website ("profile data").
                        The profile data may include, but is not limited to, your name, address, business name, telephone
                        number, email address, profile and animal pictures, gender, date of birth, interests and hobbies,
                        and business details. The profile data may be processed for the purposes of enabling and
                        monitoring your use of our website and services. The legal basis for this processing is
                        our legitimate interests, namely proper administration of our website and services.
                    </li>
                    <li>
                        We may process your personal data that is provided in the course of the use of our services ("service data").
                        The service data may include, but is not limited to, animal husbandry practices, collection size, interactions with other
                        Users on the site, and breeding logs. The service data may be processed for the purpose of operating
                        our website, providing our services, ensuring the security of our website and services, maintaining
                        backups of our database and communicating with you. The legal basis for this processing is
                        our legitimate interests, namely proper administration of our website and services.
                    </li>
                    <li>
                        We may process information that you post for publication on our website or through our services
                        ("publication data"). The publication data may be processed for the purposes of enabling such
                        publication and administering our website and services. The legal basis for this processing is
                        our legitimate interests, namely the proper administration of our website and business.
                    </li>
                    <li>
                        We may process information contained in any enquiry you submit to us or our hosted sites
                        regarding goods and/or services ("enquiry data"). The enquiry data may be processed
                        for the purposes of offering, marketing and selling relevant goods and/or services to you.
                        The legal basis for this processing is consent.
                    </li>
                    <li>
                        We may process information relating to transactions, including purchases of goods and services,
                        that you enter into with us and/or through our website ("transaction data"). The transaction
                        data may include your contact details, your card details and the transaction details.
                        The transaction data may be processed for the purpose of supplying the purchased goods
                        and services and keeping proper records of those transactions. The legal basis for
                        this processing is the performance of a contract between you and us and/or taking steps,
                        at your request, to enter into such a contract and our legitimate interests, namely the proper
                        administration of our website and business.
                    </li>
                    <li>
                        We may process information that you provide to us for the purpose of subscribing to our
                        email notifications and/or newsletters ("notification data"). The notification data may be
                        processed for the purposes of sending you the relevant notifications and/or newsletters. The
                        legal basis for this processing is the performance of a contract between you and us and/or
                        taking steps, at your request, to enter into such a contract.
                    </li>
                    <li>
                        We may process information contained in or relating to any communication that you send to us
                        ("correspondence data"). The correspondence data may include the communication content and
                        metadata associated with the communication. Our website will generate the metadata associated
                        with communications made using the website contact forms. The correspondence data may be
                        processed for the purposes of communicating with you and record-keeping. The legal basis
                        for this processing is our legitimate interests, namely the proper administration of
                        our website and business and communications with users.
                    </li>
                    <li>
                        We may process any of your personal data identified in this policy where necessary for
                        the establishment, exercise or defense of legal claims, whether in court proceedings or in an
                        administrative or out-of-court procedure. The legal basis for this processing is our
                        legitimate interests, namely the protection and assertion of our legal rights, your
                        legal rights and the legal rights of others.
                    </li>
                    <li>
                        We may process any of your personal data identified in this policy where necessary for the
                        purposes of obtaining or maintaining insurance coverage, managing risks, or obtaining
                        professional advice. The legal basis for this processing is our legitimate interests,
                        namely the proper protection of our business against risks.
                    </li>
                    <li>
                        In addition to the specific purposes for which we may process your personal data set out
                        in this Section 3, we may also process any of your personal data where such processing is
                        necessary for compliance with a legal obligation to which we are subject, or in order
                        to protect your vital interests or the vital interests of another natural person.
                    </li>
                    <li>
                        Please do not supply any other person's personal data to us, unless we prompt you to do so.
                    </li>
                </ol>
            </li>
            <li>
                <h4>PROVIDING YOUR PERSONAL DATA TO OTHERS</h4>

                <ol>
                    <li>
                        We may disclose your personal data to any member of our group of companies (this means our
                        subsidiaries, our ultimate holding company and all its subsidiaries) insofar as reasonably
                        necessary for the purposes, and on the legal bases, set out in this policy.
                    </li>
                    <li>
                        We may disclose your personal data to our insurers and/or professional advisers insofar
                        as reasonably necessary for the purposes of obtaining or maintaining insurance coverage,
                        managing risks, obtaining professional advice, or the establishment, exercise or defense of
                        legal claims, whether in court proceedings or in an administrative or out-of-court procedure.
                    </li>
                    <li>
                        Financial transactions relating to our website and services may be handled by our payment
                        services providers, Paypal and Braintree. We will share transaction data with our payment
                        services providers only to the extent necessary for the purposes of processing your payments,
                        refunding such payments and dealing with complaints and queries relating to such payments and
                        refunds. You can find information about the payment services providers' privacy policies and
                        practices in their respective privacy policies.
                    </li>
                    <li>
                        In addition to the specific disclosures of personal data set out in this Section,
                        we may disclose your personal data where such disclosure is necessary for compliance with a
                        legal obligation to which we are subject, or in order to protect your vital interests or the
                        vital interests of another natural person. We may also disclose your personal data where such
                        disclosure is necessary for the establishment, exercise or defense of legal claims, whether
                        in court proceedings or in an administrative or out-of-court procedure.
                    </li>
                </ol>
            </li>
            <li>
                <h4>RETAINING AND DELETING PERSONAL DATA</h4>

                <ol>
                    <li>
                        This Section sets our our data recension policies and procedures, which are designed
                        to help ensure that we comply with our legal obligations in relation to the retention
                        and deletion of personal data.
                    </li>
                    <li>
                        Personal data that we process for any purpose or purposes shall not be kept for longer
                        than is necessary for that purpose or those purposes.
                    </li>
                    <li>
                        We will retain your personal data as follows:
                        <ol>
                            <li>
                                Account data, enquiry data, profile data, service data, notification data, and correspondence data
                                will be retained for a minimum period of one year after the user has discontinued use
                                of Reptally's services.
                            </li>
                            <li>
                                Other personal data, including but not limited to, transaction data and usage data, will be
                                retained for a minimum period of five years after the transaction or usage took place.
                            </li>
                        </ol>
                    </li>
                    <li>
                        Notwithstanding the other provisions of this Section, we may retain your personal data where
                        such retention is necessary for compliance with a legal obligation to which we are subject,
                        or in order to protect your vital interests or the vital interests of another natural person.
                    </li>
                </ol>
            </li>
            <li>
                <h4>AMENDMENTS</h4>

                <ol>
                    We may update this policy by publishing a new version on our website. You should check this
                    page occasionally to ensure you are happy with any changes. We may or may not notify you of changes to
                    this policy by email or through notifications on our website.
                </ol>
            </li>
            <li>
                <h4>YOUR RIGHTS</h4>

                <ol>
                    <li>
                        In this Section, we have summarized the rights that you have under data protection law.
                        Some of the rights are complex, and not all the details have been included in this summary.
                        Accordingly, you should read the relevant laws and guidance from the regulatory authorities for
                        a full explanation of these rights.
                    </li>
                    <li>
                        Your principle rights under data protection law are:
                        <ol>
                            <li>
                                the right to access;
                            </li>
                            <li>
                                the right to rectification;
                            </li>
                            <li>
                                the right to erasure;
                            </li>
                            <li>
                                the right to restrict processing;
                            </li>
                            <li>
                                the right to object to processing;
                            </li>
                            <li>
                                the right to data portability;
                            </li>
                            <li>
                                the right to complain to a supervisory authority; and
                            </li>
                            <li>
                                the right to withdraw consent.
                            </li>
                        </ol>
                    </li>
                    <li>
                        You have the right to confirmation as to whether or not we process your personal data and,
                        where we do, access to the personal data, together with certain additional information.
                        That additional information includes details of the purposes of the processing, the categories
                        of personal data concerned and the recipients of the personal data. Providing the rights and
                        freedoms of others are not affected, we will supply to you a copy of your personal data.
                        You can access your personal data information by visiting <a href="/settings/data">your data settings</a>.
                    </li>
                    <li>
                        You have the right to have any inaccurate personal data about you rectified and, taking into
                        account the purposes of the processing, to have any incomplete personal data about you completed.
                    </li>
                    <li>
                        In some circumstances you have the right to the erasure of your personal data without undue
                        delay. Those circumstances include: the personal data are no longer necessary in relation
                        to the purposes for which they were collected or otherwise processed; you withdraw consent
                        to consent-based processing; you object to the processing under certain rules of applicable
                        data protection law; the processing is for direct marketing purposes; and the personal data
                        have been unlawfully processed. However, there are exclusions of the right to erasure.
                        The general exclusions include where processing is necessary: for exercising the right of
                        freedom of expression and information; for compliance with a legal obligation; or for the
                        establishment, exercise or defense of legal claims.
                    </li>
                    <li>
                        In some circumstances you have the right to restrict the processing of your personal data.
                        Those circumstances are: you contest the accuracy of the personal data; processing is unlawful
                        but you oppose erasure; we no longer need the personal data for the purposes of our processing,
                        but you require personal data for the establishment, exercise or defence of legal claims;
                        and you have objected to processing, pending the verification of that objection. Where
                        processing has been restricted on this basis, we may continue to store your personal data.
                        However, we will only otherwise process it: with your consent; for the establishment, exercise
                        or defense of legal claims; for the protection of the rights of another natural or legal person;
                        or for reasons of important public interest.
                    </li>
                    <li>
                        You have the right to object to our processing of your personal data on grounds relating to
                        your particular situation, but only to the extent that the legal basis for the processing is
                        that the processing is necessary for: the performance of a task carried out in the public
                        interest or in the exercise of any official authority vested in us; or the purposes of the
                        legitimate interests pursued by us or by a third party. If you make such an objection,
                        we will cease to process the personal information unless we can demonstrate compelling
                        legitimate grounds for the processing which override your interests, rights and freedoms,
                        or the processing is for the establishment, exercise or defense of legal claims.
                    </li>
                    <li>
                        You have the right to object to our processing of your personal data for direct marketing
                        purposes (including profiling for direct marketing purposes). If you make such an objection,
                        we will cease to process your personal data for this purpose.
                    </li>
                    <li>
                        You have the right to object to our processing of your personal data for scientific or
                        historical research purposes or statistical purposes on grounds relating to your particular
                        situation, unless the processing is necessary for the performance of a task carried out for
                        reasons of public interest.
                    </li>
                    <li>
                        To the extent that the legal basis for our processing of your personal data is:

                        <ol>
                            <li>
                                consent; or
                            </li>
                            <li>
                                that the processing is necessary for the performance of a contract to which you are
                                party or in order to take steps at your request prior to entering into a contract,
                            </li>
                        </ol>

                        and such processing is carried out by automated means, you have the right to receive your
                        personal data from us in a structured, commonly used and machine-readable format. However,
                        this right does not apply where it would adversely affect the rights and freedoms of others.
                    </li>
                    <li>
                        If you consider that our processing of your personal information infringes data protection laws,
                        you have a legal right to lodge a complaint with a supervisory authority responsible for
                        data protection.
                    </li>
                    <li>
                        To the extent that the legal basis for our processing of your personal information is consent,
                        you have the right to withdraw that consent at any time. Withdrawal will not affect the
                        lawfulness of processing before the withdrawal.
                    </li>
                </ol>
            </li>
            <li>
                <h4>ABOUT COOKIES</h4>

                <ol>
                    <li>
                        A cookie is a file containing an identifier (a string of letters and numbers) that is sent by
                        a web server to a web browser and is stored by the browser. The identifier is then sent back
                        to the server each time the browser requests a page from the server.
                    </li>
                    <li>
                        Cookies may be either "persistent" cookies or "session" cookies: a persistent cookie will
                        be stored by a web browser and will remain valid until its set expiry date, unless deleted
                        by the user before the expiry date; a session cookie, on the other hand, will expire at the
                        end of the user session, when the web browser is closed.
                    </li>
                    <li>
                        Cookies do not typically contain any information that personally identifies a user, but
                        personal information that we store about you may be linked to the information stored in and
                        obtained from cookies.
                    </li>
                </ol>
            </li>
            <li>
                <h4>COOKIES THAT WE USE</h4>

                <ol>
                    <li>
                        We use cookies for the following purposes:

                        <ol>
                            <li>
                                authentication - we use cookies to identify you when you visit our website and as
                                you navigate our website;
                            </li>
                            <li>
                                status - we use cookies to help us to determine if you are logged into our website;
                            </li>
                            <li>
                                personalization - we use cookies to store information about your preferences and
                                to personalize the website for you;
                            </li>
                            <li>
                                security - we use cookies as an element of the security measures used to protect
                                user accounts, including preventing fraudulent use of login credentials, and to
                                protect our website and services generally;
                            </li>
                            <li>
                                analysis - we use cookies to help us to analyse the use and performance of
                                our website and services;
                            </li>
                            <li>
                                cookie consent - we use cookies to store your preferences in relation to the use
                                of cookies more generally.
                            </li>
                        </ol>
                    </li>
                </ol>
            </li>
            <li>
                <h4>COOKIES USED BY OUR SERVICE PROVIDERS</h4>

                <ol>
                    <li>
                        Our service providers use cookies and those cookies may be stored on your computer when
                        you visit our website.
                    </li>
                    <li>
                        We use Google Analytics to analyse the use of our website. Google Analytics gathers
                        information about website use by means of cookies. The information gathered relating to our
                        website is used to create reports about the use of our website. Please review
                        <a href="https://www.google.com/policies/privacy">Google's Privacy Policy</a> for more details.
                    </li>
                </ol>
            </li>
            <li>
                <h4>MANAGING COOKIES</h4>

                <ol>
                    <li>
                        Most browsers allow you to refuse to accept cookies and to delete cookies. The methods for
                        doing so vary from browser to browser, and from version to version.
                    </li>
                    <li>
                        Blocking all cookies will have a negative impact upon the usability of many websites.
                    </li>
                    <li>
                        If you block cookies, you will not be able to use all the features on our website.
                    </li>
                </ol>
            </li>
            <li>
                <h4>OUR DETAILS</h4>

                <ol>
                    <li>
                        This website is owned and operated by Miranda Roberts.
                    </li>
                    <li>
                        We are registered in Spokane, Washington in the United States.
                    </li>
                    <li>
                        You can contact us:

                        <ol>
                            <li>
                                using our website contact form;
                            </li>
                            <li>
                                by email, using the email address published on our website.
                            </li>
                        </ol>
                    </li>
                </ol>
            </li>
        </ol>
    </div>
@endsection