@extends('layouts.app')

@section('header')
    <link href="{{ url('/vendor/devdojo/chatter/assets/vendor/spectrum/spectrum.css') }}" rel="stylesheet">
	<link href="{{ url('/vendor/devdojo/chatter/assets/css/chatter.css') }}" rel="stylesheet">
	@if($chatter_editor == 'simplemde')
		<link href="{{ url('/vendor/devdojo/chatter/assets/css/simplemde.min.css') }}" rel="stylesheet">
	@elseif($chatter_editor == 'trumbowyg')
		<link href="{{ url('/vendor/devdojo/chatter/assets/vendor/trumbowyg/ui/trumbowyg.css') }}" rel="stylesheet">
		<style>
			.trumbowyg-box, .trumbowyg-editor {
				margin: 0px auto;
			}
		</style>
	@endif
@endsection

@section('content')
	<div id="chatter" class="z-1 body">
		<div id="chatter_hero">
			<div id="chatter_hero_dimmer"></div>
            <?php $headline_logo = Config::get('chatter.headline_logo'); ?>
			@if(isset($headline_logo) && !empty($headline_logo ))
				<img src="{{ Config::get('chatter.headline_logo') }}">
			@else
				<h1>Community Discussion</h1>
				<h2>Reptally Community Forums</h2>
			@endif
		</div>

		<div class="flex">
			<div class="bg-alt z-2 relative shadow overflow-hidden">
				<div class="menu" id="menuSidebar">
					<a href="/{{ Config::get('chatter.routes.home') }}"><i class="chatter-bubble"></i> Show all discussions</a>
					{!! $categoriesMenu !!}
				</div>
			</div>

			<div class="box">
				<div class="padding shadow relative z-1">
					<div class="max">
						<div class="flex padded align-middle">
							<div class="box">
								<input type="text" placeholder="Search Discussions">
								<button class="btn btn-primary" id="new_discussion_btn"><i class="chatter-new"></i> Create new discussion</button>
								<ul class="discussions">
									@foreach($discussions as $discussion)
										<li>
											<a class="discussion_list" href="/{{ Config::get('chatter.routes.home') }}/{{ Config::get('chatter.routes.discussion') }}/{{ $discussion->category->slug }}/{{ $discussion->slug }}">
												<div class="chatter_avatar">
												@if(Config::get('chatter.user.avatar_image_database_field'))
                                                    <?php $db_field = Config::get('chatter.user.avatar_image_database_field'); ?>

													<!-- If the user db field contains http:// or https:// we don't need to use the relative path to the image assets -->
														@if( (substr($discussion->user->{$db_field}, 0, 7) == 'http://') || (substr($discussion->user->{$db_field}, 0, 8) == 'https://') )
															<img src="{{ $discussion->user->{$db_field}  }}">
														@else
															<img src="{{ Config::get('chatter.user.relative_url_to_image_assets') . $discussion->user->{$db_field}  }}">
														@endif
													@else
														<span class="chatter_avatar_circle" style="background-color:#<?= \App\Services\Chatter\ChatterService::stringToColorCode($discussion->user->{Config::get('chatter.user.database_field_with_user_name')}) ?>">
													{{ strtoupper(substr($discussion->user->{Config::get('chatter.user.database_field_with_user_name')}, 0, 1)) }}
												</span>
													@endif
												</div>

												<div class="chatter_middle">
													<h3 class="chatter_middle_title">{{ $discussion->title }} <div class="chatter_cat" style="background-color:{{ $discussion->category->color }}">{{ $discussion->category->name }}</div></h3>
													<span class="chatter_middle_details">Posted by <span data-href="/user">{{ ucfirst($discussion->user->{Config::get('chatter.user.database_field_with_user_name')}) }}</span> {{ \Carbon\Carbon::createFromTimeStamp(strtotime($discussion->created_at))->diffForHumans() }}</span>
													@if($discussion->post[0]->markdown)
                                                        <?php $discussion_body = GrahamCampbell\Markdown\Facades\Markdown::convertToHtml( $discussion->post[0]->body ); ?>
													@else
                                                        <?php $discussion_body = $discussion->post[0]->body; ?>
													@endif
													<p>{{ substr(strip_tags($discussion_body), 0, 200) }}@if(strlen(strip_tags($discussion_body)) > 200){{ '...' }}@endif</p>
												</div>

												<div class="chatter_right">
													<div class="chatter_count"><i class="chatter-bubble"></i> {{ $discussion->postsCount[0]->total }}</div>
												</div>

												<div class="chatter_clear"></div>
											</a>
										</li>
									@endforeach
								</ul>
							</div>

							<div id="pagination">
								{{ $discussions->links() }}
							</div>
						</div>

						<div id="new_discussion">
							<div class="chatter_loader dark" id="new_discussion_loader">
								<div></div>
							</div>

							<form id="chatter_form_editor" action="/{{ Config::get('chatter.routes.home') }}/{{ Config::get('chatter.routes.discussion') }}" method="POST">
								<div class="row">
									<div class="col-md-7">
										<!-- TITLE -->
										<input type="text" class="form-control" id="title" name="title" placeholder="Enter your title" value="{{ old('title') }}" >
									</div>

									<div class="col-md-4">
										<!-- CATEGORY -->
										<select id="chatter_category_id" class="form-control" name="chatter_category_id">
											<option value="">Select a Category</option>
											@foreach($categories as $category)
												@if(old('chatter_category_id') == $category->id)
													<option value="{{ $category->id }}" selected>{{ $category->name }}</option>
												@elseif(!empty($current_category_id) && $current_category_id == $category->id)
													<option value="{{ $category->id }}" selected>{{ $category->name }}</option>
												@else
													<option value="{{ $category->id }}">{{ $category->name }}</option>
												@endif
											@endforeach
										</select>
									</div>

									<div class="col-md-1">
										<i class="chatter-close"></i>
									</div>
								</div><!-- .row -->

								<!-- BODY -->
								<div id="editor">
									@if( $chatter_editor == 'tinymce' || empty($chatter_editor) )
										<label id="tinymce_placeholder">Type your Discussion here...</label>
										<textarea id="body" class="richText" name="body" placeholder="">{{ old('body') }}</textarea>
									@elseif($chatter_editor == 'simplemde')
										<textarea id="simplemde" name="body" placeholder="">{{ old('body') }}</textarea>
									@elseif($chatter_editor == 'trumbowyg')
										<textarea class="trumbowyg" name="body" placeholder="Type your Discussion here...">{{ old('body') }}</textarea>
									@endif
								</div>

								<input type="hidden" name="_token" id="csrf_token_field" value="{{ csrf_token() }}">

								<div id="new_discussion_footer">
									<input type='text' id="color" name="color" /><span class="select_color_text">Select Color</span>
									<button id="submit_discussion" class="btn btn-success pull-right"><i class="chatter-new"></i> Create Discussion</button>
									<a href="/{{ Config::get('chatter.routes.home') }}" class="btn btn-default pull-right" id="cancel_discussion">Cancel</a>
									<div style="clear:both"></div>
								</div>
							</form>

						</div><!-- #new_discussion -->
					</div>
				</div>
			</div>
		</div>
	</div>

	@if( $chatter_editor == 'tinymce' || empty($chatter_editor) )
		<input type="hidden" id="chatter_tinymce_toolbar" value="{{ Config::get('chatter.tinymce.toolbar') }}">
		<input type="hidden" id="chatter_tinymce_plugins" value="{{ Config::get('chatter.tinymce.plugins') }}">
	@endif
	<input type="hidden" id="current_path" value="{{ Request::path() }}">
@endsection

@section('scripts')
	@if( $chatter_editor == 'tinymce' || empty($chatter_editor) )
		<script src="{{ url('/vendor/devdojo/chatter/assets/vendor/tinymce/tinymce.min.js') }}"></script>
		<script src="{{ url('/vendor/devdojo/chatter/assets/js/tinymce.js') }}"></script>
		<script>
			var my_tinymce = tinyMCE;
			$('document').ready(function(){
				$('#tinymce_placeholder').click(function(){
					my_tinymce.activeEditor.focus();
				});
			});
		</script>
	@elseif($chatter_editor == 'simplemde')
		<script src="{{ url('/vendor/devdojo/chatter/assets/js/simplemde.min.js') }}"></script>
		<script src="{{ url('/vendor/devdojo/chatter/assets/js/chatter_simplemde.js') }}"></script>
	@elseif($chatter_editor == 'trumbowyg')
		<script src="{{ url('/vendor/devdojo/chatter/assets/vendor/trumbowyg/trumbowyg.min.js') }}"></script>
		<script src="{{ url('/vendor/devdojo/chatter/assets/vendor/trumbowyg/plugins/preformatted/trumbowyg.preformatted.min.js') }}"></script>
		<script src="{{ url('/vendor/devdojo/chatter/assets/js/trumbowyg.js') }}"></script>
	@endif

	<script src="{{ url('/vendor/devdojo/chatter/assets/vendor/spectrum/spectrum.js') }}"></script>
	<script src="{{ url('/vendor/devdojo/chatter/assets/js/chatter.js') }}"></script>
	<script>
		$('document').ready(function(){

			$('.chatter-close, #cancel_discussion').click(function(){
				$('#new_discussion').slideUp();
			});
			$('#new_discussion_btn').click(function(){
				@if(Auth::guest())
					window.location.href = "{{ route('login') }}";
				@else
					$('#new_discussion').slideDown();
					$('#title').focus();
				@endif
			});

			$("#color").spectrum({
				color: "#333639",
				preferredFormat: "hex",
				containerClassName: 'chatter-color-picker',
				cancelText: '',
				chooseText: 'close',
				move: function(color) {
					$("#color").val(color.toHexString());
				}
			});

			@if (count($errors) > 0)
				$('#new_discussion').slideDown();
				$('#title').focus();
			@endif


		});
	</script>
@stop
