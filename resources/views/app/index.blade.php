@extends('layouts.app')

@section('title')
    Robust Reptile Tracking Software - Reptally
@endsection

@section('content')

    @if (!Auth::check())
        <div class="slider" style="height: 100vh;">
            <div class="slide flex justify-center" style="background-image: url('https://static.reptally.com/wallpapers/green-tree.jpg');">
                <div class="slide-content round align-center shadow-xlarge" style="max-width: 500px;">
                    <div class="round-top bg-black-8 padding-x4 align-center">
                        <img src="https://static.reptally.com/logo-bar-lt.png" alt="Reptally - Robust reptile tracking solution" width="300"/>
                    </div>
                    <div class="padding-x4 bg-white-8 round-bottom">
                        <div class="align-left">
                            <p class="justify">
                                Reptally is a reptile and website management platform designed to streamline the record-keeping and sales process for breeders. Whether you are still shopping for your first pet or you're a veteran breeder, Reptally will offer tools to make animal husbandry and sales
                                easier.
                            </p>
                            <ul>
                                <li>Browse community manage info and listings from other users.</li>
                                <li>Keep track of animal feeds, sheds, pairings, weight and more.</li>
                                <li>Easily manage your collection from you computer or mobile device.</li>
                                <li>List reptiles to your site, our master listings, or even to <a href="https://www.morphmarket.com" target="_blank">Morph Market</a>.</li>
                            </ul>
                            <div class="flex padded flow-vertical">
                                <div>
                                    <form action="/subscribe" method="post">
                                        {{ csrf_field() }}
                                        <div class="input text">
                                            <label>Email Address</label>
                                            <input name="email" type="email" />
                                        </div>
                                        <div>
                                            <button class="button large" type="submit">Notify Me!</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="slider" id="slider" style="height: 90vh;">
            <div class="slide flex justify-center" style="background-image: url('https://static.reptally.com/wallpapers/green-tree.jpg');">
                <div class="slide-content">
                    <h2>Robust Reptile Tracking</h2>
                    <ul class="h5">
                        <li>Browse community manage info and listings from other users.</li>
                        <li>Keep track of animal feeds, sheds, pairings, weight and more.</li>
                        <li>Easily manage your collection from you computer or mobile device.</li>
                        <li>List reptiles to your site, our master listings, or even to <a href="https://www.morphmarket.com" target="_blank">Morph Market</a>.</li>
                    </ul>
                    <a class="button large" href="#">Call to Action</a>
                </div>
            </div>
        </div>

        <div class="section" id="what-is-reptally">
            <div class="max">
                <h3>What is Reptally?</h3>
                <p class="h4">
                    Reptally is a reptile husbandry management platform designed to streamline the record-keeping and
                    sales process for breeders. Whether you are still shopping for your first pet or you're a veteran
                    breeder, Reptally will offer tools to make animal husbandry and sales easier.
                </p>
            </div>
        </div>
        <div class="section" id="features">
            <div class="max align-center">
                <h3>Feature Overview</h3>
                <div class="flex padded-x2">
                    <div class="box flex">
                        <div class="shadow bg-white flex flow-vertical round">
                            <div class="bg-green padding-x4 align-center color-white round-top">
                                <svg class="icon-plus size-x8"><use xlink:href="#icon-plus"></use></svg>
                            </div>
                            <div class="padding-x4 round-bottom align-left">
                                <h5>Label Generator</h5>
                                <p>
                                    Use Reptally's built in label generator to create a label for an animal or even a
                                    whole rack. When you are with your reptiles and need to log a feed, shed, weight, or
                                    anything else you can scan the label straight from your phone. You can even list animals
                                    for sale this way to painlessly manage your stock.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="box flex">
                        <div class="shadow bg-white flex flow-vertical round">
                            <div class="bg-green padding-x4 align-center color-white round-top">
                                <svg class="icon-menu size-x8"><use xlink:href="#icon-menu"></use></svg>
                            </div>
                            <div class="padding-x4 round-bottom align-left">
                                <h5>Almanac</h5>
                                <p>
                                    Our <a href="/almanac">Almanac</a> offers live information fueled by the breeders that
                                    use the system. You can browse species and morphs that breeders on Reptally work with
                                    and even read articles written by our users on a wide variety of reptile species.
                                    Anyone can suggest a ratification of these records, ensuring they are always kept up-to-date
                                    and accurate.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="box flex">
                        <div class="shadow bg-white flex flow-vertical round">
                            <div class="bg-green padding-x4 align-center color-white round-top">
                                <svg class="icon-star size-x8"><use xlink:href="#icon-star"></use></svg>
                            </div>
                            <div class="padding-x4 round-bottom align-left">
                                <h5>Automatic Records</h5>
                                <p>
                                    Reptally's husbandry tracker records data of your collection, allowing you to compare
                                    comprehensive data of your collection as a whole as well as individual animal. You can
                                    see weight and feed charts, clutch averages, profit margins, and even track lineages
                                    all just by regularly using the system.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="#" class="margin-top-x2 block">View all features</a>
            </div>
        </div>

        <div style="display: none;" class="section" id="pricing">
            <div class="max">
                <h3>Pricing</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

                <div class="flex padded-x2">
                    <div class="box">
                        <div class="bg-white shadow round">
                            <div class="bg-alt padding-x2 align-center round-top">
                                <div class="h4">Free</div>
                                <div class="subheading">$??.00/month</div>
                            </div>
                            <div class="padding-x2 align-left">
                                <ul>
                                    <li>Example feature 1</li>
                                    <li>Example feature 2</li>
                                    <li>Example feature 3</li>
                                </ul>
                            </div>
                            <div class="padding-x2 round-bottom bg-alt">
                                <a class="button width-12">Sign Up</a>
                            </div>
                        </div>
                    </div>
                    <div class="box">
                        <div class="bg-white shadow round">
                            <div class="bg-green padding-x2 align-center color-white round-top">
                                <div class="h4">Starter</div>
                                <div class="subheading">$??.00/month</div>
                            </div>
                            <div class="padding-x2 align-left">
                                <ul>
                                    <li>Example feature 1</li>
                                    <li>Example feature 2</li>
                                    <li>Example feature 3</li>
                                </ul>
                            </div>
                            <div class="padding-x2 round-bottom bg-alt">
                                <a class="button width-12">Sign Up</a>
                            </div>
                        </div>
                    </div>
                    <div class="box">
                        <div class="bg-white shadow round">
                            <div class="bg-alt padding-x2 align-center round-top">
                                <div class="h4">Pro</div>
                                <div class="subheading">$??.00/month</div>
                            </div>
                            <div class="padding-x2 align-left">
                                <ul>
                                    <li>Example feature 1</li>
                                    <li>Example feature 2</li>
                                    <li>Example feature 3</li>
                                </ul>
                            </div>
                            <div class="padding-x2 round-bottom bg-alt">
                                <a class="button width-12">Sign Up</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection