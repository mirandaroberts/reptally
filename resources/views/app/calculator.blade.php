@extends('layouts.app')

@section('title')
    Calculate morphs and reptile breeding genetics - Reptally
@endsection

@section('content')
    <div class="section align-center">
        <h2>Reptile genetic calculator</h2>
        <p>Predict morph combinations and clutches</p>
    </div>
    <div class="section" id="calculator">
        <div class="max align-center">

            <div class="inline-block align-left">
                <div class="flex flow-vertical padded-x2">
                    <div>
                        <div class="input select">
                            <label>Species</label>
                            <select>
                                <option></option>
                                @foreach ($speciesList as $s)
                                    <option value="{{ $s->id }}" @if ($species && $s->id === $species->id) selected @endif> {{ $s->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div>
                        <p>
                            @if (Auth::check())
                                You can select morphs or animal item number within your collection.
                            @else
                                <a href="/register">Register</a> a free account to use your own animals.
                            @endif
                        </p>
                    </div>
                    <div>
                        <div class="flex padded align-middle">
                            <div>
                                <div class="input select">
                                    <label>Parent 1</label>
                                    <select multiple>
                                        <option></option>
                                        <option selected value="1">Bumblebee</option>
                                    </select>
                                </div>
                            </div>
                            <div>x</div>
                            <div>
                                <div class="input select">
                                    <label>Parent 2</label>
                                    <select multiple>
                                        <option></option>
                                        <option selected value="1">Super Pastel</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <a class="button" href="">Submit</a>
                    </div>
                    <div>
                        @if ($parent1 && $parent2)
                            <div class="max">
                                <h3>{{ $parent1 }} x {{ $parent2 }}</h3>
                                @foreach ($results as $offspring)
                                    <div>
                                        <b>{{ $offspring['percent'] }}%</b> {{ $offspring['combo'] }}
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    </div>
                    <div>
                        <!-- <canvas id="barChart" style="height: 100px; width: 400px;"></canvas> -->
                        <div class="flex">
                            <div style="max-width: 400px">
                                <canvas id="pieChart" style="height: 300px; width: 400px;"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // var ctx = document.getElementById("barChart").getContext('2d');
        // var myChart = new Chart(ctx, {
        //     type: 'bar',
        //     data: {
        //         labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
        //         datasets: [{
        //             label: '# of Votes',
        //             data: [12, 19, 3, 5, 2, 3],
        //             backgroundColor: [
        //                 'rgba(255, 99, 132, 0.2)',
        //                 'rgba(54, 162, 235, 0.2)',
        //                 'rgba(255, 206, 86, 0.2)',
        //                 'rgba(75, 192, 192, 0.2)',
        //                 'rgba(153, 102, 255, 0.2)',
        //                 'rgba(255, 159, 64, 0.2)'
        //             ],
        //             borderColor: [
        //                 'rgba(255,99,132,1)',
        //                 'rgba(54, 162, 235, 1)',
        //                 'rgba(255, 206, 86, 1)',
        //                 'rgba(75, 192, 192, 1)',
        //                 'rgba(153, 102, 255, 1)',
        //                 'rgba(255, 159, 64, 1)'
        //             ],
        //             borderWidth: 1
        //         }]
        //     },
        //     options: {
        //         scales: {
        //             yAxes: [{
        //                 ticks: {
        //                     beginAtZero:true
        //                 }
        //             }]
        //         }
        //     }
        // });
        new Chart(document.getElementById("pieChart"), {
            type: 'pie',
            data: {
              labels: ["Pastel", "Bumblebee", "Super Pastel", "Killer Bee"],
              datasets: [{
                label: "Morph Genetic Distribution",
                backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                data: [25,25,25,25]
              }]
            },
            options: {
              title: {
                display: true,
                text: 'Morph Genetic Distribution'
              }
            }
        });
    </script>
@endsection