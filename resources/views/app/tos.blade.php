@extends('layouts.app')

@section('title')
    Terms of Service - Reptally
@endsection

@section('content')
    <div class="section align-center">
        <h2>Terms of Service</h2>
    </div>
    <div class="section max justify">
        <p>
            Please read the following terms of service agreement carefully. By accessing or using our sites and our
            services, you hereby agree to be bound by the terms and all the terms incorporated herein by reference.
            It is the responsibility of you, the user, customer, or prospective customer to read the terms and
            conditions before proceeding to use this site. If you do not expressly agree to all the terms and
            conditions, then please do not access or use our sites our our services. This terms of service agreement is
            effective as of <b>05/27/2018</b> and was last modified <b>05/27/2018</b>.
        </p>

        <h4>ACCEPTANCE OF TERMS</h4>
        <p>
            The following Terms of Service Agreement (the "TOS") is a legally binding agreement that shall govern
            the relationship with our users and others which may interact or interface with Reptally and our
            subsidiaries and affiliates, in association with the use of the Reptally website, which includes
            https://www.reptally.com, (the "Site") and its Services, which shall be defined below.
        </p>

        <h4>DESCRIPTION OF WEBSITE SERVICES OFFERED</h4>
        <p>
            The Site is a social media and e-commerce which has the following description:
        </p>

        <p>
            Reptally is a reptile and website management platform designed to streamline the record-keeping and sales
            process for breeders. Whether you are still shopping for your first pet or you're a veteran breeder,
            Reptally will offer tools to make animal husbandry and sales easier.
        </p>

        <p>
            Any and all visitors to our site, despite whether they are registered or not, shall be deemed as "users"
            of the herein contained Services provided for the purpose of this TOS. Once and individual registers for
            our Services, through the process of creating an account, the user shall then be considered a "member."
        </p>

        <p>
            The user and/or member acknowledges and agrees that the Services provided and made available through the
            Site and applications, which may include some mobile applications and those applications may be made
            available on carious social media networking sites and numerous other platforms and downloadable programs,
            are the sole property of Reptally. At its discretion, Reptally may offer additional website Services and/or
            products, or update, modify or revise any current content and Services, and this Agreement shall apply to
            any and all additional Services and/or products and any and all updated, modified, or revised Services
            unless otherwise stipulated. Reptally does hereby reserve the right to cancel and cease offering any of the
            aforementioned Services and/or products. You, as the end user and/or member, acknowledge, accept, and agree
            that Reptally shall not be held liable for any such updates, modifications, revisions, suspensions, or
            discontinuance of any of our Services and/or products. Your continued use of the Services provided, after
            such posting of any updates, changes, and/or modifications shall constitute your acceptance of such updates,
            changes and/or modifications, and as such, frequent review of this Agreement and any and all applicable
            terms and policies should be made by you to ensure you are aware of all the terms and policies currently in
            effect. Should you not agree to the updated, revised, or modified terms, you must stop using the provided
            Services forthwith.
        </p>

        <p>
            Furthermore, the user and/or member understands, acknowledges and agrees that the Service offered shall be
            provided "AS IS" and as such Reptally shall not assume any responsibility or obligation for the timeliness,
            missed delivery, deletion and/or any failure to store user content, communication or personalization settings.
        </p>

        <h4>REGISTRATION</h4>
        <p>
            To register and become a "member" of the Site, you must be at least 18 years of age to under into and form
            a legally binding contract. In addition, you must be in good standing and not an individual that has been
            previously barred from receiving Reptally's Services under the laws and statutes of the United States or
            other applicable jurisdiction.
        </p>

        <p>
            When you register, Reptally may collect information such as your name, e-mail address, birth date, gender,
            mailing address, occupation, industry and personal interests. You can edit your account information at any
            time. Once you register with Reptally and sign in to our Services, you are no longer anonymous to us.
        </p>

        <p>
            Furthermore, the registering party hereby acknowledges, understands and agrees to:
        </p>

        <ol>
            <li>
                furnish factual, correct, current and complete information with regards to yourself as may be requested
                by the data registration process, and
            </li>
            <li>
                maintain and promptly update your registration and profile information in an effort to maintain accuracy
                and completeness at all times.
            </li>
        </ol>

        <p>
            If anyone knowingly provides any information of a false, untrue, inaccurate or incomplete nature, Reptally
            will have sufficient grounds and rights to suspend or terminate the member in violation of this aspect of
            the Agreement, as as such refuse any and all current or future use of Reptally Services, or any portion
            thereof.
        </p>

        <p>
            It is Reptally's priority to ensure the safety and privacy of all its visitors, users and members, especially
            that of children. Therefore, it is for that reason that the parents of any child under the age of 13 that
            permit their child or children access to the Reptally website platform Services must create a "family" account,
            which will certify that the individual creation the "family" account is of 18 years of age and as such,
            the parent or legal guardian of any child or children that use this account. As the creator of the "family"
            account, s/he is thereby granting permission for her/her child to access the various Services provided,
            including, but not limited to, message boards, email, and/or instant messaging. It is the paren's and/or
            legal guardian's responsibility to determine whether any of the Services and/or content provided are
            age-appropriate for his/her child.
        </p>

        <h4>PRIVACY POLICY</h4>
        <p>
            Every member's registration data and various other personal information are strictly protected by the
            <a href="/privacy">Reptally Online Privacy Policy</a>. As a member, you herein consent to the collection
            and use of the information provided, including transfer of information within the United States and/or
            other countries for storage, processing, or use by Reptally and/or our subsidiaries and affiliates.
        </p>

        <h4>MEMBER ACCOUNT, USERNAME, PASSWORD AND SECURITY</h4>
        <p>
            When you set up an account, you are the sole authorized user of your account. You shall be responsible for
            maintaining the secrecy and confidentiality of your password and for all activates that transpire on or
            within your account. It is your responsibility for any act or omission of any user(s) that access your
            account information that, if undertaken by you, would be deemed a violation of the TOS. It shall be your
            responsibility to notify Reptally immediately if you notice any unauthorized access or use of your
            account or password ot any other breach of security. Reptally shall not be held liable for any loss and/or
            damage arising from any failure to comply with this term and/or condition of the TOS.
        </p>

        <h4>CONDUCT</h4>
        <p>
            As a user or member of the Site, you herein acknowledge, understand, and agree that all information, text,
            software, data, photographs, music, video, messages, tags or any other content, whether it is publicly or
            privately posted and/or transmitted, is expressed sole responsibility of the individual from whom the
            content originated. In short, this means that you are solely responsible for any and all content posted,
            uploaded, emailed, transmitted or otherwise made available by way of the Reptally Services, and as such,
            we do not guarantee the accuracy, integrity or quality of such content. It is expressly understood that
            by use of out Services, you may be exposed to content including, but not limited to, any errors or
            omissions in any content posted, and/or any loss or damage of any kind incurred as a result of the use of
            any content posted, emailed, transmitted, or otherwise made available by Reptally.
        </p>

        <p>Furthermore, you herein agree to not make use of Reptally's Services for the purpose of:</p>

        <ol>
            <li>
                uploading, posting, emailing, transmitting, or otherwise making available any content that shall be
                deemed unlawful, harmful, threatening, abusive, harassing, tortuous, defamatory, vulgar, obscene,
                libelous, or invasive of another's privacy or which is hateful, and/or racially, ethnically, or
                otherwise objectionable;
            </li>
            <li>
                causing harm to minors in any manner whatsoever;
            </li>
            <li>
                impersonating any individual or entity, including, but not limited to, any Reptally officials, forum
                leaders, event organizers or hosts or falsely stating or otherwise misrepresenting any affiliation
                with an individual or entity;
            </li>
            <li>
                forging captions, headings or titles or otherwise offering any content that you personally have no
                right to pursuant to any law nor having any contractual or fiduciary relationship with;
            </li>
            <li>
                uploading, posting, email, transmitting, or otherwise offering any such content that may infringe
                upon any patent, copyright, trademark, or any other proprietary or intellectual rights of any other
                party;
            </li>
            <li>
                uploading, posting, email, transmitting, or otherwise offering any content that you do not personally
                have any right to offer pursuant to any law or in accordance with any contractual or fiduciary
                relationship;
            </li>
            <li>
                uploading, posting, email, transmitting, or otherwise offering any unsolicited or unauthorized
                advertising, promotional flyers, "junk mail," "spam," or any other form of solicitation, except in
                any such areas that may have been designated for such purpose;
            </li>
            <li>
                uploading, posting, email, transmitting, or otherwise offering any source that may contain a
                software virus or other computer code, any files and/or programs which have been designed to
                interfere, destroy, and/or limit the operation of any computer software, hardware, or
                telecommunication equipment;
            </li>
            <li>
                disrupting the normal flow of communication, or otherwise acting in a manner that would negatively
                affect other users' ability to participate in any real time interactions;
            </li>
            <li>
                interfering with or disrupting any Reptally Services, servers and/or networks that may be connected
                or related to our website, including, but not limited to, the use of any device software and/or
                routine to bypass the robot exclusion headers;
            </li>
            <li>
                intentionally or unintentionally violating any local, state, federal, national, or international law,
                including, but no limited to, rules, guidelines, and/or regulations decreed by the U.S. Securities
                and Exchange Commissions, in addition to any rules of any nation or other securities exchange, that
                would include without limitation, the New York Stock Exchange, the American Stock Exchange, or
                the NASDAQ, and regulations having the force of law;
            </li>
            <li>
                providing informational support or resources, concealing and/or disguising the character, location,
                and or sources to any organization delegated to the United States government as a "foreign terrorist
                organization" in accordance to Section 219 of the Immigration Nationality Act;
            </li>
            <li>
                "stalking" or with the intent to otherwise harass another individual; and/or
            </li>
            <li>
                collecting or storing of any personal data relating to any other member or user in connection with
                the prohibited conduct and/or activities which have been set forth in the aforementioned paragraphs.
            </li>
        </ol>

        <p>
            Reptally herein reserves the right to pre-screen, refuse, and/or delete any content currently available
            through our Services. In addition, we reserve the right to remove and/or delete any such content that
            would violate the TOS or which would otherwise be considered offensive to other visitors, users and/or
            members.
        </p>

        <p>
            Reptally herein reserves the right to access, preserve, and/or disclose member account information
            and/or content if it requested to do so by law or in good faith belief that any such action is deemed
            reasonably necessary for:
        </p>

        <ol>
            <li>
                compliance with any legal process;
            </li>
            <li>
                enforcement of the TOS;
            </li>
            <li>
                responding to any claim that therein contained content is in violation of the rights of any third
                party;
            </li>
            <li>
                responding to the requests for customer service; or
            </li>
            <li>
                protecting the rights, property or the personal safety of Reptally, its visitors, users, and members,
                including the general public.
            </li>
        </ol>

        <p>
            Reptally herein reserves the right to include the use of security components that may permit digital
            information or material to be protected, and that such use of information and/or material is subject to
            usage guidelines and regulations established by Reptally or any other content providers supplying content
            services to Reptally. You are hereby prohibited from making an attempt to override or circumvent any of the
            embedded usage rules in our Services. Furthermore, unauthorized reproduction, publication, distribution, or
            exhibition of any information or materials supplied by our Services, despite whether done so in whole or
            in part, is expressly prohibited.
        </p>

        <h4>INTERSTATE COMMUNICATION</h4>
        <p>
            Upon registration, you hereby acknowledge that by using the Site to send electronic communications,
            which would include, but are not limited to email, searches, instant messages, uploading of files, photos,
            and/or videos, you will be causing communications to be sent through our computer network. Therefore,
            through your use, and thus your agreement with this TOS, you are acknowledging that the use of this Service
            shall result in interstate transmissions.
        </p>

        <h4>CAUTIONS FOR GLOBAL USE AND EXPORT AND IMPORT COMPLIANCE</h4>
        <p>
            Due to the global nature of the internet, through the use of our network you hereby agree to comply with
            all local rules relating to online conduct and that which is considered acceptable Content. Uploading,
            posting and/or transferring of software, technology and other technical data may be subject to the
            export and import laws of the United States and possibly other countries. Through the use of our
            network, you thus agree to comply with all applicable export and import laws, statuses and regulations,
            including, but not limited to, the <a href="http://www.access.gpo.gov/bis/ear/ear_data.html">Export
            Administration Regulations</a>, as well as the sactions control program of the
            <a href="http://www.treasury.gov/resource-center/sanctions/Programs/Pages/Programs.aspx">United States</a>.
            Furthermore, you state and pledge that you:
        </p>
        <ol>
            <li>
                are not on the list of prohibited individuals which may be identified on any government export exclusion
                report nor a member od any other government which may be part of an export-prohibited country
                identified in applicable export and import laws and regulations;
            </li>
            <li>
                agree not to transfer any software, technology or any other technical data through the use of our
                network Services to any export-prohibited country;
            </li>
            <li>
                agree not to use our website network Services for any military, nuclear, missile, chemical or
                biological weaponry end uses that would be a violation of the U.S. export laws; and
            </li>
            <li>
                agree not to post, transfer, nor upload any software, technology or any other technical data which
                would be in violation of the U.S. or any other applicable export and/or import laws.
            </li>
        </ol>

        <h4>CONTENT PLACED OR MADE AVAILABLE FOR COMPANY SERVICES</h4>
        <p>
            Reptally shall not lay claim to ownership of any content submitted by any visitor, member, or user, nor make
            such content available for inclusion on our website Services. Therefor, you hereby grant and allow for Reptally
            the below listed worldwide, royalty-free and non-exclusive licenses, as applicable:
        </p>

        <ol>
            <li>
                The content submitted or made available for inclusion on the publicly accessible areas of Reptally's
                sites, the licence provided to permit use, distribute, reproduce, modify, adapt, publicly
                perform and/or publicly display said Content on our network Services is for the sol purpose of
                providing and promoting the specific area to which this content was place and/or made available for
                viewing. This license shall be available so long as you are a member of Reptally's sites, and shall
                terminate at such a time when you elect to discontinue your membership.
            </li>
            <li>
                Photos, audio, video and/or graphics submitted to made available for inclusion on the publicly
                accessible areas of Reptally's sites, the license provided to permit use, distribute, reproduce, modify, adapt, publicly
                perform and/or publicly display said Content on our network Services is for the sol purpose of
                providing and promoting the specific area to which this content was place and/or made available for
                viewing. This licence shall be available so long as you are a member of Reptally's sites, and shall
                terminate at such a time when you elect to discontinue your membership.
            </li>
            <li>
                For any other content submitted or made available for inclusion on the publicly accessible areas of
                Reptally's sites, the continuous, binding and completely sub-licensable license which is meant to
                permit to user, distribute, reproduce, modify, adapt, publish, translate, publicly perform and/or
                publicly display said content, whether in whole or in part, and the incorporation of any such Content
                into other works in any arrangement or medium currently used or later developed.
            </li>
        </ol>

        <p>
            Those areas which may be deemed "publicly accessible" areas of Reptally's sites are those such areas of
            our network properties which are meant to be available to the general public, and which would include
            message boards and groups that are openly available to both users and members. However, those areas
            which are not open to the public, and thus available to members only, would include instance messaging and
            privately marked animal entries.
        </p>

        <h4>CONTRIBUTIONS TO COMPANY WEBSITE</h4>
        <p>
            Reptally provides and area for our users and members to contribute feedback to our website. When you
            submit ideas, documents, suggestions, and/or proposals ("Contributions") to our site, you acknowledge
            and agree that:
        </p>

        <ol>
            <li>
                your contributions do not contain any type of confidential or proprietary information;
            </li>
            <li>
                Reptally shall not be liable under any obligation to ensure or maintain confidentiality, expressed
                or implied, related to any Contributions;
            </li>
            <li>
                Reptally shall be entitled to make use of and/or disclose any such Contributions in any such manner
                as they may see fit;
            </li>
            <li>
                the contributor's Contributions shall automatically become the sole property of Reptally; and
            </li>
            <li>
                Reptally is under no obligation to either compensate or provide any form of reimbursement in any
                manner or nature.
            </li>
        </ol>

        <h4>INDEMNITY</h4>
        <p>
            All users and/or members herein agree to insure and hold Reptally, our subsidiaries, affiliates, agents,
            employees, officers, partners and/or licensors blameless or not liable for any claim or demand, which may
            include but is not limited to, reasonable attorney fees made by any third party which may arise from any
            content a member or use of our site may submit, post, modify, transmit, or otherwise make available through
            our Services, the use of Reptally Services or your connection with these Services, your violations of
            the Terms of Service and/or your violation of any such rights of another person.
        </p>

        <h4>COMMERCIAL REUSE OF SERVICES</h4>
        <p>
            The member or user herein agrees not to replicate, duplicate, copy, trade, sell, reuse not exploit for any
            commercial reason any part, use of, or access to Reptally's sites.
        </p>

        <h4>USE AND STORAGE GENERAL PRACTICES</h4>
        <p>
            You herein acknowledge that Reptally may set up any such practices and/or limits regarding the use of
            our Services, without limitation of the maximum number of days that any email, message posting or any
            other uploaded content shall be retained by Reptally, nor the maximum number of email messages that
            may be sent and/or received by any member, the maximum volume or size of any email message that may be
            sent and/or received by any member, the maximum volume or size of any email message that may be sent
            from or may be received by an account on our Service, the maximum disk space allowable that shall be
            allocated on Reptally's servers on the member's behalf, and/or the maximum number of times and/or
            duration that any member may access our Services in a given period of time. In addition, you also
            agree that Reptally has absolutely no responsibility or liability for the failure to remove or maintain
            storage of any messages and/or other communication or content maintained or transmitted by our Services.
            You also herein acknowledge that we reserve the right to delete or remove any account that is no longer
            active for an extended period of time. Furthermore, Reptally shall reserve the right to modify, alter
            and/or update these general practices and limits at our discretion.
        </p>

        <p>
            Any messenger service, which may include web-based versions, shall allow you and the individuals with whom
            you communicate with the ability to save your conversations in your account on Reptally's servers. In this
            manner, you will be able to access and search your message history from any computer with internet access.
            You also acknowledge that others have the option to use and save conversations with you in their own
            personal account on the Site. It is your agreement to this TOS which establishes your consent to allow
            Reptally to store any and all communications on its servers.
        </p>

        <h4>MODIFICATIONS</h4>
        <p>
            Reptally shall reserve the right at any time it may deem fit, to modify, alter, and or discontinue,
            whether temporarily or permanently, our service, or any part thereof, with or without prior notice. In
            addition, we shall not be held liable to you or any third party for any such alteration, modification,
            suspension and/or discontinuance of our Services, or any part thereof.
        </p>

        <h4>TERMINATION</h4>
        <p>
            As a member of Reptally, you may cancel or terminate your account, associated email address and/or
            access to our Services by submitting a cancellation or termination request to support@reptally.com.
        </p>

        <p>
            As a member, you agree that Reptally may without any prior written notice, immediately suspend, terminate,
            discontinue and/or limit your account, any email associated with your account, and access to any of our
            Services. The cause for such termination, discontinuance, suspension and/or limitation of accesss shall
            include but is not limited to:
        </p>

        <ol>
            <li>
                any breach or violation of our TOS or any other incorporated agreement, regulation and/or guideline;
            </li>
            <li>
                by way of requests from law enforcement or any other governmental agencies;
            </li>
            <li>
                the discontinuance, alteration and/or material modification to our Services, or any part thereof;
            </li>
            <li>
                unexpected technical or security issues and/or problems;
            </li>
            <li>
                any extended periods of inactivity;
            </li>
            <li>
                any engagement by you in any fraudulent or illegal activities; and/or
            </li>
            <li>
                the nonpayment of any associated fees that may be owed by you in connection with your account Services.
            </li>
        </ol>

        <p>
            Furthermore, you herein agree that any and all terminations, suspensions, discontinuances, and or limitations
            of access for cause shall be made at our sole discretion and that we shall not be liable to you or any other
            third party with regards to the termination of your account, associated email address and/or access to any
            of our Services.
        </p>

        <p>
            The termination of your account with Reptally shall include any and/or all of the following:
        </p>

        <ol>
            <li>
                the removal of any access or part of the service offered within reptally.com;
            </li>
            <li>
                the deletion of your password and any and all related information, files, and any such content that
                may be associated with or inside your account, or any part thereof; and
            </li>
            <li>
                the barring of any future use of all or part of our Services.
            </li>
        </ol>

        <h4>ADVERTISERS</h4>
        <p>
            Any correspondence or business dealings with, or the participation of, advertisers located on through our
            Services, which include the payment and/or delivery of such related good and/or Services, and such other
            term, condition, warranty and/or representation associated with such dealings, are and shall be solely
            between you and any such advertiser. Moreover, you herein agree that Reptally shall not be help responsible
            or liable for any loss or damage of any nature or manner incurred as a direct result of any such dealings
            or as a result of the presence of such advertisers on our website.
        </p>

        <h4>LINKS</h4>
        <p>
            Either Reptally or any third parties may provide links to other websites and/or resources. Thus, you
            acknowledge and agree that we are not responsible for the availability of any such external sites or
            resources, and as such, we do not endorse nor are we responsible or liable for any content, products,
            advertising or any other materials, on or available from such third party sites or resources. Furthermore,
            you acknowledge and agree that Reptally shall not be responsible or liable, directly or indirectly, for
            any such damage or loss that may be a result of, caused or allegedly to be caused by or in connection with
            the use of or reliance on any such content, goods or Services made available on or through the use of
            any such site or resource.
        </p>

        <h4>PROPRIETARY RIGHTS</h4>
        <p>
            You do hereby acknowledge and agree that Reptally's Services and any essential software that may be used
            in connection with our Services ("Software") shall contain proprietary and confidential material that is
            protected by applicable intellectual property rights and other laws. Furthermore, you herein acknowledge
            and agree that any Content which may be contained in any advertisements or information presented by and
            through our Services or by advertisers is protected by copyrights, trademarks, patents or other proprietary
            rights and laws. Therefore, expect for that which is expressly permitted by applicable law or is authorized
            by Reptally or such applicable licensor, you agree not to alter, modify, lease, rent, loan, sell, distribute,
            transmit, broadcast, publicly perform and/or created any plagiaristic works which are based on Reptally
            Services (e.g. Content or Software), in whole or part.
        </p>
        <p>
            Reptally herein has granted you personal, non-transferable and non-exclusive rights and/or license to make
            use of the object code or our Software on a single computer, as long as you do not, and shall now, allow any
            third party to duplicate, alter, modify, create, or plagiarize work from, reverse engineer, reverse
            assemble or otherwise make an attempt to locate or discern any source code, sell, assign, sublicense, grant a
            security interest in and/or otherwise transfer any such right in the Software. Furthermore, you do herein
            agree not to alter or change the Software in any manner, nature or form, and as such, not to use any
            modified versions of the Software, including and without limitation, for the purpose of obtaining
            unauthorized access to our Services. Lastly, you also agree not to access or attempt to access our
            Services through any means other than through the interface which is provided by Reptally for use in
            accessing our Services.
        </p>

        <h4>WARRANTY DISCLAIMERS</h4>
        <p>
            You herein expressly acknowledge and agree that:
        </p>

        <ol>
            <li>
                The use of Reptally Services and Software are at the sole risk by you. Our Services and Software shall
                be provided on an "AS IS" and/or "AS AVAILABLE" basis. Reptally and our subsidiaries, affiliates,
                officers, employees, agents, partners and licensors expressly disclaim any and all warranties of any
                kind whether expressed or implied, including, but not limited to any implies warranties of title,
                merchantability, fitness for a particular purpose and non-infringement.
            </li>
            <li>
                Reptally and our subsidiaries, affiliates, officers, employees, agents, partners and licensors make
                no such warranties that (i) Reptally Services or Software will meet your requirements; (ii) Reptally
                Service or Software shall be uninterrupted, timely, secure or error-free; (iii) that such results
                which may be obtained from the use of Reptally Services or Software will be accurate or reliable; (iv)
                quality of any products, Services, any information or other material which may be purchased or
                obtained by you through our Services or Software will meet your expectations; and (v) that any such
                errors contained in the Software shall be corrected.
            </li>
            <li>
                Any information or material downloaded or otherwise obtained by way or Reptally Services or Software
                shall be accessed by your sole discretion and sole risk, and as such you shall solely be responsible
                for and hereby waive any and all such claims and causes of action with respect to any damage to your
                computer and/or internet access, downloading and/or displaying, or for any loss of data that could
                result from the download of any such information or material.
            </li>
            <li>
                No advice and/or information, despite whether written or oral, that may be obtained by you from
                Reptally or by way of from our Services or Software shall create any warranty not expressly stated
                in the TOS.
            </li>
            <li>
                A small percentage of some users may experience some degree of epileptic seizure when exposed to
                certain light patterns or backgrounds that may be contained on a computer screen or while using
                our Services. Certain conditions may induce a previously unknown condition or undetected epileptic
                symptom in users who have shown no history of any prior seizure or epilepsy. Should you, anyone you
                know or anyone in your family have an epileptic condition, please consult a physician if you
                experience any of the following symptoms while using our Services: dizziness, altered vision, eye or
                muscle twitches, loss of awareness, disorientation, any involuntary movement, or convulsions.
            </li>
        </ol>

        <h4>LIMITATION OF LIABILITY</h4>
        <p>
            You explicitly acknowledge, understand and agree that Reptally and our subsidiaries, affiliates, officers,
            employees, agents, partners, and licensors shall not be liable to you for any punitive, indirect, incidental,
            special, consequential or exemplary damages, including, but not limited to, damages which may be related to
            the loss of profits, goodwill, use, data and/or other intangible losses, even though we may have been
            advised of such possibility that said damages may occur, and result from:
        </p>

        <ol>
            <li>
                the use or inability to use our Service;
            </li>
            <li>
                the cost of procuring substitute goods and services;
            </li>
            <li>
                unauthorized access to or alteration of your transmissions and/pr data;
            </li>
            <li>
                statements or conduct of any such third party on our Service;
            </li>
            <li>
                and any other matter which may be related to our Service.
            </li>
        </ol>

        <h4>RELEASE</h4>
        <p>
            In the event you have a dispute, you agree to release Reptally (and its officers, directors, employees,
            agents, parent subsidiaries, affiliates, co-branders, partners, and any other third parties) from claims,
            demands and damages (actual and consequential) of every kind and nature, known and unknown, suspected or
            unsuspected, disclosed and undisclosed, arising out of or in any way connected to such dispute.
        </p>

        <h4>SPECIAL ADMONITION RELATED TO FINANCIAL MATTERS</h4>
        <p>
            Should you intend to create or join any service, receive or request any such news, messages, alerts, or other
            information from our Services concerning companies, stock quotes, investments or securities, please
            review the above Sections Warranty Disclaimer and Limitation of Liability again. In addition, for this
            particular type of information, the phrase "Let the investor beware" is appropriate. Reptally's content is
            provided primarily for information purposes, and no content that shall be provided or included in our
            Services is intended for trading or investing purposes. Reptally and our licensors shall not be responsible
            or liable for the accuracy, usefulness or availability of any information transmitted and/or made available
            by way of our Services, and shall not be responsible or liable for any trading and/or investment information
            decisions based on any such information.
        </p>

        <h4>EXCLUSION AND LIMITATIONS</h4>
        <p>
            There are some jurisdictions which do not allow the exclusion of certain warranties or the limitation of
            exclusion of liability for incidental or consequential damages. Therefore, some of the above limitations of
            Section Warranty Disclaimers and Limitations of Liability may not apply to you.
        </p>

        <h4>THIRD PARTY BENEFICIARIES</h4>
        <p>
            You herein acknowledge, understand, and agree, unless otherwise expressly provided in this TOS, that there
            shall be no third-party beneficiaries to this agreement.
        </p>

        <h4>NOTICE</h4>
        <p>
            Reptally may furnish you with notices, including those with regard to any changes to the TOS, including but
            not limited to email, regular mail, MMS or SMS, text messaging, postings on our website Services, or other
            reasonable means currently known or any which may be herein after developed. Any such notices may not be
            received if you violate any aspects of the TOS by accessing our Services in an unauthorized manner. Your
            acceptance of this TOS constitutes your agreement that you are deemed to have received any and all notices
            that would have been delivered had you accessed our Services in an authorized manner.
        </p>

        <h4>TRADEMARK INFORMATION</h4>
        <p>
            You herein acknowledge, understand and agree that all of the Reptally trademarks, copyright, trade name,
            service marks, and other Reptally logos and any brand features, and/or product and service names are
            trademarks and as such, are and shall remain the property of Reptally. You herein agree not to display
            and/or use in any manner the Reptally logo or marks without obtaining Reptally's prior consent.
        </p>

        <h4>COPYRIGHT OR INTELLECTUAL PROPERTY INFRINGEMENT CLAIMS NOTICE & PROCEDURES</h4>
        <p>
            Reptally will always respect the intellectual property of others, and we will ask that all of our users do
            the same. With regards to appropriate circumstances and all its sole discretion, Reptally may disable
            and/or terminate the accounts of any user who violates our TOS and/or infringes the rights of others. If
            you feel that your work has been duplicated in such a way that would constitute copyright infringement,
            or if you believe your intellectual property rights have been otherwise violates, you should provide to us
            the following information:
        </p>

        <ol>
            <li>
                The electronic or physical signature of the individual that is authorized on behalf of the owner of
                the copyright or other intellectual property interest;
            </li>
            <li>
                A description of the copyright work or other intellectual property that you believe has been infringed
                upon;
            </li>
            <li>
                A description of the location of the site which you allege has been infringing upon your work;
            </li>
            <li>
                Your physical address, telephone number, and email address;
            </li>
            <li>
                A statement, in which you state that the alleged and disputed use of your work is not authorized by
                the copyright owner, its agents or the law;
            </li>
            <li>
                And finally, a statement, made under penalty of perjury, that the aforementioned information in your
                notice is truthful and accurate, and you are the copyright or intellectual property owner, representative
                or agent authorized to act on the copyright or intellectual property owner's behalf.
            </li>
        </ol>

        <p>
            The Reptally Agent for notice of claims of copyright or other intellectual property infringement can be
            contacted at the following email: support@reptally.com
        </p>

        <h4>VIOLATIONS</h4>
        <p>
            Pleast report any and all violations of this TOS to support@reptally.com
        </p>
    </div>
@endsection