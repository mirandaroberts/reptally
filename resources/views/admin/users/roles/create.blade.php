@extends('layouts.admin')

@section('title')
    Edit User Roles
@endsection

@section('content')
    <div class="">
        <div class="padding shadow relative z-1">
            <div class="max">
                <div class="flex padded align-middle">
                    <div class="box">
                        <div class="h2">Create User Role</div>
                    </div>
                    <div>
                        <a class="button">
                            <span>Back to Roles</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="max padding-top-x4">
            <form method="POST" action="">
                @csrf

                <div class="form-group row">
                    <label for="name" class="col-sm-4 col-form-label text-md-right">Name</label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                        @if ($errors->has('name'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-6 offset-md-4">
                        <div class="checkbox">
                            <label>
                                <input type="hidden" name="superuser" value="0">
                                <input type="checkbox" name="superuser" {{ old('superuser') ? 'checked' : '' }} value="1"> Superuser
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-6 offset-md-4">
                        <div class="checkbox">
                            <label>
                                <input type="hidden" name="can_create_store" value="0">
                                <input type="checkbox" name="can_create_store" {{ old('can_create_store') ? 'checked' : '' }} value="1"> Can create store
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-6 offset-md-4">
                        <div class="checkbox">
                            <label>
                                <input type="hidden" name="can_create_animal" value="0">
                                <input type="checkbox" name="can_create_animal" {{ old('can_create_animal') ? 'checked' : '' }} value="1"> Can create animal
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-6 offset-md-4">
                        <div class="checkbox">
                            <label>
                                <input type="hidden" name="can_list_animal" value="0">
                                <input type="checkbox" name="can_list_animal" {{ old('can_list_animal') ? 'checked' : '' }} value="1"> Can List Animal
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-6 offset-md-4">
                        <div class="checkbox">
                            <label>
                                <input type="hidden" name="can_purchase_animal" value="0">
                                <input type="checkbox" name="can_purchase_animal" {{ old('can_purchase_animal') ? 'checked' : '' }} value="1"> Can Purchase Animal
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-6 offset-md-4">
                        <div class="checkbox">
                            <label>
                                <input type="hidden" name="can_approve_morphs" value="0">
                                <input type="checkbox" name="can_approve_morphs" {{ old('can_approve_morphs') ? 'checked' : '' }} value="1"> Can Approve Morphs
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-8 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            Submit
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection