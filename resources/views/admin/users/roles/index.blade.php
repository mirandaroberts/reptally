@extends('layouts.admin')

@section('title')
    Edit User Roles
@endsection

@section('content')
    <div class="">
        <div class="padding shadow relative z-1">
            <div class="max">
                <div class="flex padded align-middle">
                    <div class="box">
                        <div class="h2">User Roles</div>
                    </div>
                    <div>
                        <a class="button" href="{{ route('admin.user.roles.create') }}">
                            {!! $icon['add'] !!}
                            <span>New Role</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="max padding-top-x4">
            <table class="table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Superuser</th>
                    <th>Can Create Store</th>
                    <th>Can Create Animal</th>
                    <th>Can List Animal</th>
                    <th>Can Purchase Animal</th>
                    <th>Can Approve Morphs</th>
                    <th class="td-static"></th>
                </tr>
                </thead>
                <tbody>
                @foreach ($roles as $role)
                    <tr>
                        <td>{{ $role->id }}</td>
                        <td>{{ $role->name }}</td>
                        <td>{{ $role->superuser ? 'Yes' : 'No' }}</td>
                        <td>{{ $role->can_create_store ? 'Yes' : 'No' }}</td>
                        <td>{{ $role->can_create_animal ? 'Yes' : 'No' }}</td>
                        <td>{{ $role->can_list_animal ? 'Yes' : 'No' }}</td>
                        <td>{{ $role->can_purchase_animal ? 'Yes' : 'No' }}</td>
                        <td>{{ $role->can_approve_morphs ? 'Yes' : 'No' }}</td>
                        <td class="td-static"><a class="button"><svg class="icon-edit"><use xlink:href="#icon-edit"></use></svg></a>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection