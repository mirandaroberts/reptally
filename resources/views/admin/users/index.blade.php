@extends('layouts.admin')

@section('title')
    Edit Users
@endsection

@section('content')
    <div class="">
        <div class="padding shadow relative z-1">
            <div class="max">
                <div class="flex padded align-middle">
                    <div class="box">
                        <div class="h2">Users</div>
                    </div>
                    <div>
                        <a class="button" href="{{ route('dashboard') }}">
                            <span>User Roles</span>
                        </a>

                        <a class="button" href="{{ route('admin.user.plans') }}">
                            <span>User Plans</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="max padding-top-x4">
            <table class="table" id="tblUsers">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Role</th>
                    <th>Plan</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('#tblUsers').table({
            url: "/admin/users/fetch-all",
            columns: [
                { "data": "id", "name": "id" },
                { "data": "first_name", "name": "first_name" },
                { "data": "last_name", "name": "last_name" },
                { "data": "email", "name": "email" },
                { "data": "role_id", "name": "role.name" },
                { "data": "plan_id", "name": "plan.name" }
            ],
            type: "users"
        });
    </script>
@endsection