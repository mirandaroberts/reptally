@extends('layouts.admin')

@section('title')
    Edit User Plans
@endsection

@section('content')
    <div class="">
        <div class="padding shadow relative z-1">
            <div class="max">
                <div class="flex padded align-middle">
                    <div class="box">
                        <div class="h2">User Plans</div>
                    </div>
                    <div>
                        <a class="button" href="">
                            {!! $icon['add'] !!}
                            <span>New Plan</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="max padding-top-x4">
            <table class="table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Monthly Cost</th>
                    <th>Annual Cost</th>
                    <th># of Animals</th>
                    <th># of Active Listings</th>
                    <th class="td-static"></th>
                </tr>
                </thead>
                <tbody>
                @foreach ($plans as $plan)
                    <tr>
                        <td>{{ $plan->id }}</td>
                        <td>{{ $plan->name }}</td>
                        <td>{{ $plan->monthly_cost }}</td>
                        <td>{{ $plan->annual_cost }}</td>
                        <td>{{ $plan->animals }}</td>
                        <td>{{ $plan->active_listings }}</td>
                        <td class="td-static"><a class="button"><svg class="icon-edit"><use xlink:href="#icon-edit"></use></svg></a>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection