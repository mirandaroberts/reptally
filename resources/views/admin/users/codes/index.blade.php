@extends('layouts.admin')

@section('title')
    Manage invite codes - Reptally
@endsection

@section('content')
    <div class="">
        <div class="padding shadow relative z-1">
            <div class="max">
                <div class="flex padded align-middle">
                    <div class="box">
                        <div class="h2">Manage Invite Codes</div>
                    </div>
                    <div>
                        <a class="button" href="">
                            {!! $icon['add'] !!}
                            <span>New Invite Code</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="max padding-top-x4">
            <table class="table">
                <thead>
                <tr>
                    <th>Email</th>
                    <th>Code</th>
                    <th>Used</th>
                    <th>Used At</th>
                    <th class="td-static"></th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
@endsection