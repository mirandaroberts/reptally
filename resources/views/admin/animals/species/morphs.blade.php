@extends('layouts.admin')

@section('title')
    Edit Animal Species
@endsection

@section('navbar')
    @include('layouts.partials._nav')
@endsection

@section('content')
    <div class="z-1 body">
        <div class="flex">
            <div class="box relative">
                <div class="padding shadow relative z-1">
                    <div class="max">
                        <div class="flex padded align-middle">
                            <div class="box">
                                <div class="h4">Species - {{ $species->name }} Morphs</div>
                            </div>
                            <div>
                                <a href="{{ route('admin.animals.species.edit', ['id' => $species->id]) }}" class="button">
                                    {!! $icon['add'] !!}
                                    <span>Back</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="padding">
                    <div class="max padding-top-x2 padding-bottom-x2">
                        Morph stuff here
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection