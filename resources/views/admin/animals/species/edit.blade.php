@extends('layouts.admin')

@section('title')
    Edit Animal Species
@endsection

@section('navbar')
    @include('layouts.partials._nav')
@endsection

@section('content')
<form method="post" action="">
                            @csrf
    <div class="z-1 body">
        <div class="flex">
            <div class="box relative">
                <div class="padding shadow relative z-1">
                    <div class="max">
                        <div class="flex padded align-middle">
                            <div>

                                <a href="{{ route('admin.animals.species') }}" class="button secondary">
                                    <span style="transform: rotate(180deg);">{!! $icon['chevron'] !!}</span>
                                    <span>Back</span>
                                </a>
                            </div>
                            <div class="box align-center">
                                <div class="h5">Species - {{ isset($species->id) ? $species->name : 'Create' }}</div>
                            </div>
                            <div>

                                <button type="submit" class="button">
                                    {!! $icon['save'] !!}
                                    <span>Save</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="padding">
                    <div class="max padding-top-x2 padding-bottom-x2">

                        

                            <div class="tabs">
                                <ul>
                                    <li><a href="#tab1">Details</a></li>
                                    <li><a href="#tab2">Subspecies</a></li>
                                    <li><a href="#tab3">Localities</a></li>
                                </ul>
                                <div id="tab1">
                                    <h4>Details</h4>

                                    <div class="flex padded flow-vertical">
                                        <div>
                                            <div class="input text">
                                                <label for="name">Name</label>
                                                <input type="text" name="name" id="name" value="{{ old('name', $species->name) }}">
                                            </div>
                                        </div>

                                        <div>
                                            <div class="input text">
                                                <label for="scientific_name">Scientific Name</label>
                                                <input type="text" name="scientific_name" id="scientific_name" value="{{ old('scientific_name', $species->scientific_name) }}">
                                            </div>
                                        </div>

                                        <!-- <div>
                                            <div class="input text">
                                                <label for="description">Description (wysiwyg)</label>
                                                <textarea class="textarea" name="description" id="description">{{ old('description', $species->description) }}</textarea>
                                            </div>
                                        </div> -->

                                        <div>
                                            <div class="input checkbox">
                                                <input type="checkbox" value="1" name="venomous" id="venomous">
                                                <label for="venomous">Venomous?</label>
                                            </div>
                                        </div>

                                        <div>
                                            <div class="input select">
                                                <label for="status">Conservation Status</label>
                                                <select name="status" id="status">
                                                    <option value="NE" @if (old('status', $species->status) == 'NE') selected @endif>Not Evaluated</option>
                                                    <option value="DD" @if (old('status', $species->status) == 'DD') selected @endif>Data Deficient</option>
                                                    <option value="LC" @if (old('status', $species->status) == 'LC') selected @endif>Least Concern</option>
                                                    <option value="NT" @if (old('status', $species->status) == 'NT') selected @endif>Near Threatened</option>
                                                    <option value="VU" @if (old('status', $species->status) == 'VU') selected @endif>Vulnerable</option>
                                                    <option value="EN" @if (old('status', $species->status) == 'EN') selected @endif>Endangered</option>
                                                    <option value="CR" @if (old('status', $species->status) == 'CR') selected @endif>Critically Endangered</option>
                                                    <option value="EW" @if (old('status', $species->status) == 'EW') selected @endif>Extinct in the Wild</option>
                                                    <option value="EX" @if (old('status', $species->status) == 'EX') selected @endif>Extinct</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div>
                                            <div class="input select">
                                                <label for="availability">Trade Availability</label>
                                                <select name="availability" id="availability">
                                                    <option value="Common" @if (old('availability', $species->availability) == 'Common') selected @endif>Common</option>
                                                    <option value="Uncommon" @if (old('availability', $species->availability) == 'Uncommon') selected @endif>Uncommon</option>
                                                    <option value="Rare" @if (old('availability', $species->availability) == 'Rare') selected @endif>Rare</option>
                                                    <option value="Very Rare" @if (old('availability', $species->availability) == 'Very Rare') selected @endif>Very Rare</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div>
                                            <div class="input text">
                                                <label for="avg_length">Average Length</label>
                                                <input type="text" name="avg_length" id="avg_length" value="{{ old('avg_length', $species->avg_length) }}">
                                            </div>
                                        </div>

                                        <div>
                                            <div class="input text">
                                                <label for="avg_weight">Average Weight</label>
                                                <input type="text" name="avg_weight" id="avg_weight" value="{{ old('avg_weight', $species->avg_weight) }}">
                                            </div>
                                        </div>

                                        <div>
                                            <div class="input text">
                                                <label for="lifespan">Lifespan</label>
                                                <input type="text" name="lifespan" id="lifespan" value="{{ old('lifespan', $species->lifespan) }}">
                                            </div>
                                        </div>

                                        <div>
                                            <div class="input text">
                                                <label for="diet">Diet</label>
                                                <input type="text" name="diet" id="diet" value="{{ old('diet', $species->diet) }}">
                                            </div>
                                        </div>

                                        <div>
                                            <div class="input text">
                                                <label for="habitat">Habitat</label>
                                                <input type="text" name="habitat" id="habitat" value="{{ old('habitat', $species->habitat) }}">
                                            </div>
                                        </div>

                                        <div>
                                            <div class="input select">
                                                <label for="care_level">Care Level</label>
                                                <select name="care_level" id="care_level">
                                                    <option value="Unknown" @if (old('care_level', $species->care_level) == 'Unknown') selected @endif>Unknown</option>
                                                    <option value="Beginner" @if (old('care_level', $species->care_level) == 'Beginner') selected @endif>Beginner</option>
                                                    <option value="Intermediate" @if (old('care_level', $species->care_level) == 'Intermediate') selected @endif>Intermediate</option>
                                                    <option value="Advanced" @if (old('care_level', $species->care_level) == 'Advanced') selected @endif>Advanced</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="tab2">
                                    <h4>Subspecies</h4>

                                    <button type="button" class="button">Add Subspecies</button>

                                    @if ($species->subspecies->count())
                                        @foreach ($species->subspecies as $subspecies)
                                            <div class="subspeciesForm">
                                                <div>
                                                    <label for="sub_name">Name</label>
                                                    <input type="text" name="sub_name[]" id="sub_name" value="{{ old('sub_name[]', $subspecies->name) }}">
                                                </div>

                                                <div>
                                                    <label for="sub_scientific_name">Scientific Name</label>
                                                    <input type="text" name="sub_scientific_name[]" id="sub_scientific_name" value="{{ old('sub_scientific_name[]', $subspecies->scientific_name) }}">
                                                </div>

                                                <div>
                                                    <label for="sub_description">Description (wysiwyg)</label>
                                                    <textarea name="sub_description[]" id="sub_description">{{ old('sub_description[]', $subspecies->description) }}</textarea>
                                                </div>

                                                <div>
                                                    <label for="sub_status">Conservation Status</label>
                                                    <select name="sub_status[]" id="sub_status">
                                                        <option value="NE" @if (old('sub_status[]', $subspecies->status) == 'NE') selected @endif>Not Evaluated</option>
                                                        <option value="DD" @if (old('sub_status[]', $subspecies->status) == 'DD') selected @endif>Data Deficient</option>
                                                        <option value="LC" @if (old('sub_status[]', $subspecies->status) == 'LC') selected @endif>Least Concern</option>
                                                        <option value="NT" @if (old('sub_status[]', $subspecies->status) == 'NT') selected @endif>Near Threatened</option>
                                                        <option value="VU" @if (old('sub_status[]', $subspecies->status) == 'VU') selected @endif>Vulnerable</option>
                                                        <option value="EN" @if (old('sub_status[]', $subspecies->status) == 'EN') selected @endif>Endangered</option>
                                                        <option value="CR" @if (old('sub_status[]', $subspecies->status) == 'CR') selected @endif>Critically Endangered</option>
                                                        <option value="EW" @if (old('sub_status[]', $subspecies->status) == 'EW') selected @endif>Extinct in the Wild</option>
                                                        <option value="EX" @if (old('sub_status[]', $subspecies->status) == 'EX') selected @endif>Extinct</option>
                                                    </select>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>

                                <div id="tab3">
                                    <h4>Tab 3 - Localities</h4>

                                    <button type="button" class="button">Add Locality</button>

                                    @if ($species->localities->count())
                                        @foreach ($species->localities as $locality)
                                            <div class="localitiesForm">
                                                <div>
                                                    <label for="local_name">Name</label>
                                                    <input type="text" name="local_name[]" id="local_name" value="{{ old('local_name[]', $locality->name) }}">
                                                </div>

                                                <div>
                                                    <label for="local_description">Description (wysiwyg)</label>
                                                    <textarea name="local_description[]" id="local_description">{{ old('local_description[]', $locality->description) }}</textarea>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
@endsection