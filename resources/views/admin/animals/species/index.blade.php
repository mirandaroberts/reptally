@extends('layouts.admin')

@section('title')
    Edit Animal Species
@endsection

@section('navbar')
    @include('layouts.partials._nav')
@endsection

@section('content')
    <div class="z-1 body">
        <div class="flex">
            
            <div class="box relative">
                <div class="padding shadow relative z-1">
                    <div class="max">
                        <div class="flex padded align-middle">
                            <div class="box">
                                <div class="h4">Species</div>
                            </div>
                            <div>
                                <div class="dropdown anchor-right">
                                    <a class="button">
                                        {!! $icon['gear'] !!}
                                    </a>
                                </div>
                            </div>
                            <div>
                                <a class="button btnAddSpecies">
                                    {!! $icon['add'] !!}
                                    <span>New Species</span>
                                </a>
                                
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="padding">
                    <div class="max padding-top-x2 padding-bottom-x2">
                        <table class="table" id="tblSpecies">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Scientific Name</th>
                                <th>Status</th>
                                <th>Habitat</th>
                                <th>Care Level</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function() {
            let table = $('#tblSpecies').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('admin.animals.species.fetch') !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'scientific_name', name: 'scientific_name' },
                    { data: 'status', name: 'status' },
                    { data: 'habitat', name: 'habitat' },
                    { data: 'care_level', name: 'care_level' }
                ]
            });

            $('#tblSpecies tbody').on('click', 'tr', function () {
                let data = table.row( this ).data();
                window.location.href = "/admin/animals/species/" + data.id;
            } );
        });
    </script>
@endsection