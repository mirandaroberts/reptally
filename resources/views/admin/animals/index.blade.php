@extends('layouts.admin')

@section('title')
    Your Animals
@endsection

@section('navbar')
    @include('layouts.partials._nav')
@endsection

@section('content')
    <div class="z-1 body">
        <div class="flex">
            <div class="box relative">
                <div class="padding shadow relative z-1">
                    <div class="max">
                        <div class="flex padded align-middle">
                            <div class="box">
                                <div class="h4">Your Animals</div>
                            </div>
                            <div>
                                <div class="dropdown anchor-right">
                                    <a class="button">
                                        {!! $icon['gear'] !!}
                                    </a>
                                    <div class="menu" style="display: none;">
                                        <ul>
                                            <li><a>{!! $icon['add'] !!}<span>Import</span></a></li>
                                            <li><a>{!! $icon['add'] !!}<span>Export</span></a></li>
                                            <li><hr></li>
                                            <li><a>{!! $icon['add'] !!}<span>Testing 123</span></a></li>
                                            <li>
                                                <a>{!! $icon['add'] !!}<span>Test 2</span></a>
                                                <div class="menu">
                                                    <ul>
                                                        <li><a>{!! $icon['gear'] !!}<span>Test 1</span></a></li>
                                                        <li><a>{!! $icon['gear'] !!}<span>Test 123</span></a></li>
                                                        <li><a>{!! $icon['gear'] !!}<span>Test 12345</span></a></li>
                                                    </ul>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <a class="button btnAddAnimal">
                                    {!! $icon['add'] !!}
                                    <span>Add Animals</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="padding">
                    <div class="max padding-top-x2 padding-bottom-x2">
                        <table class="table" id="tblAnimals">
                            <thead>
                            <tr>
                                <th>Item #</th>
                                <th>Name</th>
                                <th>Species</th>
                                <th>Gender</th>
                                <th>Morphs/Locality</th>
                                <th>Birth Year</th>
                                <th>Sale Price</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // var editor = grapesjs.init({
        //     container : '#gjs',
        //     components: '<div class="txt-red">Hello world!</div>',
        //     style: '.txt-red{color: red}',
        // });
        $('#tblSpecies').table({
            url: window.location.href + "/fetch-all",
            expandUrl: window.location.href + "/fetch",
            columns: [
                { "data": "item_number", "name": "item_number" },
                { "data": "name", "name": "name" },
                { "data": "species", "name": "species" },
                { "data": "gender", "name": "gender" },
                { "data": "morphs", "name": "morphs" },
                { "data": "birth_year", "name": "birth_year" },
                { "data": "sale_price", "name": "sale_price" }
            ],
            type: "animal",
        });

        $('.btnAddAnimal').click(function() {
            var $modal = $('<div><div id="formAddAnimal"></div></div>');
            var $form = $modal.find("#formAddAnimal");
            $form.form({
                type: "animal",
                data: {
                    dropdowns: {

                    },
                    tables: {

                    }
                }
            });
            $modal.modal({
                title: "Add Animal",
                padding: false,
                dialogClass: 'medium',
                buttons: [
                    {
                        text: "Cancel",
                        "class": "btn-cancel button secondary",
                        click: function() {
                            $(this).dialog('close');
                        }
                    },
                    {
                        text: "Save",
                        "class": "btn-save button",
                        click: function() {
                            var data = $form.form('getData').fields;
                            console.log(data);
                            data._token = $('meta[name="csrf-token"]').attr('content');
                            $.ajax({
                                "url": window.location.href + "/create",
                                "method": "POST",
                                "data": data,
                                "success": function (data) {
                                    console.log(data);
                                },
                                "beforeSend": function (request) {
                                    request.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
                                }
                            });
                            $(this).dialog('close');
                        }
                    }
                ]
            });
        });

        $('#menuSidebar').menu({
            drilldown: true,
        });
    </script>
@endsection