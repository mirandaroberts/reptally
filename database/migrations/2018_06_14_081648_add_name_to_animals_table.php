<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNameToAnimalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('aniamls', 'name')) {
            Schema::table('animals', function (Blueprint $table) {
                $table->string('name', 100);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('animals', 'name')) {
            Schema::table('animals', function (Blueprint $table) {
                $table->dropColumn('name');
            });
        }
    }
}
