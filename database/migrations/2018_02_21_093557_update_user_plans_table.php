<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\User\UserPlan;

class UpdateUserPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_plans', function (Blueprint $table) {
            $table->integer('animals');
            $table->integer('active_listings');
            $table->integer('annual_cost');
        });

        // Seed the table
        UserPlan::create([
            'name'                => 'Free',
            'monthly_cost'        => 0,
            'annual_cost'         => 0,
            'animals'             => 10,
            'active_listings'     => 1
        ]);

        UserPlan::create([
            'name'                => 'Neonate',
            'monthly_cost'        => 12,
            'annual_cost'         => 100,
            'animals'             => 50,
            'active_listings'     => 5
        ]);

        UserPlan::create([
            'name'                => 'Juvenile',
            'monthly_cost'        => 25,
            'annual_cost'         => 200,
            'animals'             => 200,
            'active_listings'     => 20
        ]);

        UserPlan::create([
            'name'                => 'Breeder',
            'monthly_cost'        => 50,
            'annual_cost'         => 450,
            'animals'             => 0,
            'active_listings'     => 0
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_plans', function (Blueprint $table) {
            $table->dropColumn('animals');
            $table->dropColumn('active_listings');
            $table->dropColumn('annual_cost');
        });
    }
}
