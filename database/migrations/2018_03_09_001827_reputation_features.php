<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReputationFeatures extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('users', 'reputation')) {
            Schema::table('users', function (Blueprint $table) {
                $table->integer('reputation')->unsigned()->default(1);
            });
        }

        Schema::create('reputation_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('reputation');
            $table->string('url')->nullable();
            $table->string('action');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('reputation_logs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('users', 'reputation')) {
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('reputation');
            });
        }

        Schema::dropIfExists('reputation_logs');
    }
}
