<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\User\UserRole;

class CreateUserRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->boolean('superuser');
            $table->boolean('can_create_store');
            $table->boolean('can_create_animal');
            $table->boolean('can_list_animal');
            $table->boolean('can_purchase_animal');
            $table->boolean('can_approve_morphs');
            $table->timestamps();
        });

        // Seed the table
        UserRole::create([
            'name'                => 'User',
            'superuser'           => 0,
            'can_create_store'    => 0,
            'can_create_animal'   => 1,
            'can_list_animal'     => 0,
            'can_purchase_animal' => 1,
            'can_approve_morphs'  => 0,
        ]);

        UserRole::create([
            'name'                => 'Seller',
            'superuser'           => 0,
            'can_create_store'    => 1,
            'can_create_animal'   => 1,
            'can_list_animal'     => 1,
            'can_purchase_animal' => 1,
            'can_approve_morphs'  => 0,
        ]);

        UserRole::create([
            'name'                => 'Moderator',
            'superuser'           => 0,
            'can_create_store'    => 1,
            'can_create_animal'   => 1,
            'can_list_animal'     => 1,
            'can_purchase_animal' => 1,
            'can_approve_morphs'  => 1,
        ]);

        UserRole::create([
            'name'                => 'Administrator',
            'superuser'           => 1,
            'can_create_store'    => 1,
            'can_create_animal'   => 1,
            'can_list_animal'     => 1,
            'can_purchase_animal' => 1,
            'can_approve_morphs'  => 1,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_roles');
    }
}
