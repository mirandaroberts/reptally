<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExpandingMorphDataCollection extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumns('animal_morphs',
            ['lethal', 'deformities', 'discovered_by', 'discovered_id', 'discovered_date', 'description'])) {
            Schema::table('animal_morphs', function (Blueprint $table) {
                $table->boolean('lethal')->default(0);
                $table->string('deformities', 255)->nullable();
                $table->string('discovered_by', 255)->nullable();
                $table->integer('discovered_id')->nullable()->unsigned();
                $table->date('discovered_date')->nullable();
                $table->text('description')->nullable();

                $table->foreign('discovered_id')->references('id')->on('users');
            });
        }

        if (!Schema::hasColumns('animal_morph_combos',
            ['lethal', 'deformities', 'first_produced_by', 'first_produced_id', 'first_produced_date', 'description'])) {
            Schema::table('animal_morph_combos', function (Blueprint $table) {
                $table->boolean('lethal')->default(0);
                $table->string('deformities', 255)->nullable();
                $table->string('first_produced_by', 255)->nullable();
                $table->integer('first_produced_id')->nullable()->unsigned();
                $table->date('first_produced_date')->nullable();
                $table->text('description')->nullable();

                $table->foreign('first_produced_id')->references('id')->on('users');
            });
        }

        if (!Schema::hasColumns('animal_species', ['venomous', 'description', 'status'])) {
            Schema::table('animal_species', function (Blueprint $table) {
                $table->boolean('venomous')->default(0);
                $table->text('description')->nullable();
                $table->enum('status', ['NE', 'DD', 'LC', 'NT', 'VU', 'EN', 'CR', 'EW', 'EX'])->default('NE');
            });
        }

        if (!Schema::hasColumns('animal_subspecies', ['description', 'status'])) {
            Schema::table('animal_subspecies', function (Blueprint $table) {
                $table->text('description')->nullable();
                $table->enum('status', ['NE', 'DD', 'LC', 'NT', 'VU', 'EN', 'CR', 'EW', 'EX'])->default('NE');
            });
        }

        if (!Schema::hasColumn('animal_localities', 'description')) {
            Schema::table('animal_localities', function (Blueprint $table) {
                $table->text('description')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumns('animal_morphs',
            ['lethal', 'deformities', 'discovered_by', 'discovered_id', 'discovered_date', 'description'])) {
            Schema::table('animal_morphs', function (Blueprint $table) {
                $table->dropColumn('lethal');
                $table->dropColumn('deformities');
                $table->dropColumn('discovered_by');
                $table->dropColumn('discovered_id');
                $table->dropColumn('discovered_date');
                $table->dropColumn('description');

                $table->dropForeign('animal_morphs_discovered_id_foreign');
            });
        }

        if (Schema::hasColumns('animal_morph_combos',
            ['lethal', 'deformities', 'discovered_by', 'discovered_id', 'discovered_date', 'description'])) {
            Schema::table('animal_morph_combos', function (Blueprint $table) {
                $table->dropColumn('lethal');
                $table->dropColumn('deformities');
                $table->dropColumn('first_produced_by');
                $table->dropColumn('first_produced_id');
                $table->dropColumn('first_produced_date');
                $table->dropColumn('description');

                $table->dropForeign('animal_morphs_discovered_id_foreign');
            });
        }

        if (Schema::hasColumns('animal_species', ['venomous', 'status', 'description'])) {
            Schema::table('animal_species', function (Blueprint $table) {
                $table->dropColumn('venomous');
                $table->dropColumn('status');
                $table->dropColumn('description');
            });
        }

        if (Schema::hasColumns('animal_subspecies', ['description', 'status'])) {
            Schema::table('animal_subspecies', function (Blueprint $table) {
                $table->dropColumn('description');
                $table->enum('status', ['NE', 'DD', 'LC', 'NT', 'VU', 'EN', 'CR', 'EW', 'EX'])->default('NE');
            });
        }

        if (Schema::hasColumn('animal_localities', 'description')) {
            Schema::table('animal_localities', function (Blueprint $table) {
                $table->dropColumn('description');
            });
        }
    }
}
