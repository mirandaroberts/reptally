<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnimalMorphCombosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('animal_morph_combos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('species_id')->unsigned();
            $table->text('combo');
            $table->timestamps();

            $table->foreign('species_id')->references('id')->on('animal_species');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animal_morph_combos');
    }
}
