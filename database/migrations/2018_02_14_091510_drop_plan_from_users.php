<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropPlanFromUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('user_settings', 'plan_id')) {
            Schema::table('user_settings', function (Blueprint $table) {
                $table->dropColumn('plan_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasColumn('user_settings', 'plan_id')) {
            Schema::table('user_settings', function (Blueprint $table) {
                $table->integer('plan_id')->unsigned();
            });
        }
    }
}
