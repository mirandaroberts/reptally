<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnimalMorphsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('animal_morphs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->integer('species_id')->unsigned();
            $table->enum('inheritance', ['Recessive', 'Dominant', 'Incomplete-Dominant', 'Codominant', 'Line Bred', 'Unknown']);
            $table->timestamps();

            $table->foreign('species_id')->references('id')->on('animal_species');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animal_morphs');
    }
}
