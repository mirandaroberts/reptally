<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnimalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('animals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('item_number');
            $table->integer('user_id')->unsigned();
            $table->integer('species_id')->unsigned();
            $table->longText('morphs');
            $table->enum('gender', ['Male', 'Female', 'Unknown'])->default('Unknown');
            $table->longText('description')->nullable();
            $table->integer('sale_price')->nullable();
            $table->boolean('public')->default(1);
            $table->date('birth_date')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('species_id')->references('id')->on('animal_species');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animals');
    }
}
