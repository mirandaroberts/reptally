<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnimalMorphComplexesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('animal_morph_complexes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->integer('species_id')->unsigned();
            $table->timestamps();

            $table->foreign('species_id')->references('id')->on('animal_species');
        });

        if (!Schema::hasColumn('animal_morphs', 'complex_id')) {
            Schema::table('animal_morphs', function (Blueprint $table) {
                $table->integer('complex_id')->unsigned()->default(1);
                $table->foreign('complex_id')->references('id')->on('animal_morph_complexes');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animal_morph_complexes');

        if (Schema::hasColumn('animal_morphs', 'complex_id')) {
            Schema::table('animal_morphs', function (Blueprint $table) {
                $table->dropColumn('complex_id');
                $table->dropForeign('animal_morphs_complex_id_foreign');
            });
        }
    }
}
