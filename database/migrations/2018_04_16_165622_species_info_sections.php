<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SpeciesInfoSections extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumns('animal_species', ['avg_length', 'avg_weight', 'lifespan', 'diet', 'habitat', 'care_level'])) {
            Schema::table('animal_species', function (Blueprint $table) {
                $table->text('avg_length')->nullable();
                $table->text('avg_weight')->nullable();
                $table->text('lifespan')->nullable();
                $table->text('diet')->nullable();
                $table->text('habitat')->nullable();
                $table->enum('care_level', ['Beginner', 'Intermediate', 'Advanced', 'Unknown'])->default('Unknown');
                $table->enum('availability', ['Common', 'Uncommon', 'Rare', 'Very Rare'])->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumns('animal_species', ['avg_length', 'avg_weight', 'lifespan', 'diet', 'habitat', 'care_level'])) {
            Schema::table('animal_localities', function (Blueprint $table) {
                $table->dropColumn('avg_length');
                $table->dropColumn('avg_weight');
                $table->dropColumn('lifespan');
                $table->dropColumn('diet');
                $table->dropColumn('habitat');
                $table->dropColumn('care_level');
                $table->dropColumn('availability');
            });
        }
    }
}
