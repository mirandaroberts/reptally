<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubvaluesToAnimalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('aniamls', 'name')) {
            Schema::table('animals', function (Blueprint $table) {
                $table->integer('locality_id')->unsigned()->nullable();
                $table->integer('subspecies_id')->unsigned()->nullable();

                $table->foreign('locality_id')->references('id')->on('animal_localities');
                $table->foreign('subspecies_id')->references('id')->on('animal_subspecies');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('animals', 'name')) {
            Schema::table('animals', function (Blueprint $table) {
                $table->dropColumn('locality_id');
                $table->dropColumn('subspecies_id');
            });
        }
    }
}
