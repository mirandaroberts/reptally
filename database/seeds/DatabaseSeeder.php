<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!\DB::table('users')->find(1)) {
            \DB::table('users')->insert([
                [
                    'id'             => 1,
                    'first_name'     => 'Miranda',
                    'last_name'      => 'Roberts',
                    'email'          => 'himowa@gmail.com',
                    'password'       => '$2y$10$ykmuHOQS8XYjfNYGyepeCeL/pLcFQPlcpPFZLdg4QySPaOB9akxo6',
                    'created_at'     => \Carbon\Carbon::now(),
                    'updated_at'     => \Carbon\Carbon::now(),
                    'role_id'        => 4,
                    'plan_id'        => 1,
                    'reputation'     => 1,
                ], [
                    'id'             => 2,
                    'first_name'     => 'Andrew',
                    'last_name'      => 'Deibel',
                    'email'          => 'adeibel5@gmail.com',
                    'password'       => '$2y$10$6NGBbeWTR4miWhDpImn2z.U6qpAXAKTsE8AyTZk6aO5Sen4xMa7Ji',
                    'created_at'     => \Carbon\Carbon::now(),
                    'updated_at'     => \Carbon\Carbon::now(),
                    'role_id'        => 4,
                    'plan_id'        => 1,
                    'reputation'     => 1,
                ]
            ]);

            \DB::table('user_settings')->insert([
                [
                    'user_id'          => 1,
                    'ip_address'       => '192.168.10.1',
                    'activation_token' => null
                ],
                [
                    'user_id'          => 2,
                    'ip_address'       => '192.168.10.1',
                    'activation_token' => null
                ]
            ]);
        }

        $this->call(AnimalSeeder::class);
        $this->call(ChatterTableSeeder::class);
    }
}
