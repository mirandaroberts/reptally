<?php

use App\Models\Animal\AnimalSpecies;
use Illuminate\Database\Seeder;

class AnimalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** Animal Species */
        // Pythons
        AnimalSpecies::create(['name' => 'Ball Python', 'scientific_name' => 'Python regius']);
        AnimalSpecies::create(['name' => 'Blood Python', 'scientific_name' => 'Python curtus']);
        AnimalSpecies::create(['name' => 'Borneo Short-tailed Python', 'scientific_name' => 'Python breitensteini']);
        AnimalSpecies::create(['name' => 'Sumatran Short-tailed Python', 'scientific_name' => 'Python curtus']);
        AnimalSpecies::create(['name' => 'Carpet Python', 'scientific_name' => 'Morelia spilota']);
        AnimalSpecies::create(['name' => 'Reticulated Python', 'scientific_name' => 'Python reticulatus']);
        AnimalSpecies::create(['name' => 'Burmese Python', 'scientific_name' => 'Python bivittatus']);
        AnimalSpecies::create(['name' => 'Green Tree Python', 'scientific_name' => 'Morelia viridis']);
        AnimalSpecies::create(['name' => 'Black-headed Python', 'scientific_name' => 'Aspidites melanocephalus']);
        AnimalSpecies::create(['name' => 'Woma Python', 'scientific_name' => 'Aspidites ramsayi']);
        AnimalSpecies::create(['name' => 'Children\'s Python', 'scientific_name' => 'Antaresia childreni']);
        AnimalSpecies::create(['name' => 'Spotted Python', 'scientific_name' => 'Anteresia maculosa']);
        AnimalSpecies::create(['name' => 'Stimson\'s Python', 'scientific_name' => 'Anteresia stimsoni']);
        AnimalSpecies::create(['name' => 'Pygmy Python', 'scientific_name' => 'Anteresia perthensis']);
        // Boas
        AnimalSpecies::create(['name' => 'Boa Constrictor', 'scientific_name' => 'Boa constrictor']);
        AnimalSpecies::create(['name' => 'Rainbow Boa', 'scientific_name' => 'Epicrates cenchria']);
        AnimalSpecies::create(['name' => 'Dumeril\'s Boa', 'scientific_name' => 'Acrantophis dumerili']);
        AnimalSpecies::create(['name' => 'Sand Boa', 'scientific_name' => 'Erycinae']);
        AnimalSpecies::create(['name' => 'Ringed Tree Boa', 'scientific_name' => 'Corallus annalatus']);
        AnimalSpecies::create(['name' => 'Amazon Tree Boa', 'scientific_name' => 'Corallus hortulanus']);
        AnimalSpecies::create(['name' => 'Central American Tree Boa', 'scientific_name' => 'Corallus ruschenbergerii']);
        AnimalSpecies::create(['name' => 'Emerald Tree Boa', 'scientific_name' => 'Corallus caninus']);
        AnimalSpecies::create(['name' => 'Indonesian Tree Boa', 'scientific_name' => 'Candoia carinata']);
        AnimalSpecies::create(['name' => 'Rosy Boa', 'scientific_name' => 'Lichanura trivirgata']);
        // Colubrids
        AnimalSpecies::create(['name' => 'Corn Snake', 'scientific_name' => 'Pantherophis guttatus']);
        AnimalSpecies::create(['name' => 'Kingsnake', 'scientific_name' => 'Lampropeltis']);
        AnimalSpecies::create(['name' => 'Milk Snake', 'scientific_name' => 'Lampropeltis triangulum']);
        AnimalSpecies::create(['name' => 'Western Hognose', 'scientific_name' => 'Heterodon nasicus']);
        AnimalSpecies::create(['name' => 'Tri-color Hognose', 'scientific_name' => 'Lystrophis pulcher']);
        AnimalSpecies::create(['name' => 'Garter Snake', 'scientific_name' => 'Thamnophis']);
        AnimalSpecies::create(['name' => 'Pine Snake', 'scientific_name' => 'Pituophis melanoleucus']);
        AnimalSpecies::create(['name' => 'Northern Water Snake', 'scientific_name' => 'Nerodia sipedon']);
        AnimalSpecies::create(['name' => 'False Water Cobra', 'scientific_name' => 'Hydrodynastes gigas']);
        AnimalSpecies::create(['name' => 'Rat Snake', 'scientific_name' => 'Pantherophis obsoletus']);
        // Lizards
        AnimalSpecies::create(['name' => 'Leopard Gecko', 'scientific_name' => 'Eublepharis macularius']);
        AnimalSpecies::create(['name' => 'Crested Gecko', 'scientific_name' => 'Correlophus ciliatus']);
        AnimalSpecies::create(['name' => 'Bearded Dragon', 'scientific_name' => 'Pogona']);
        AnimalSpecies::create(['name' => 'Blue-Tongued Skink', 'scientific_name' => 'Tiliqua']);
        AnimalSpecies::create(['name' => 'Day Gecko', 'scientific_name' => 'Phelsuma madagascariensis madagascariensis']);
        AnimalSpecies::create(['name' => 'Green Iguana', 'scientific_name' => 'Iguana iguana']);
        AnimalSpecies::create(['name' => 'Panther Chameleon', 'scientific_name' => 'Furcifer pardalis']);
    }
}
