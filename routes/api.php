<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/**
 * Users
 * Route::get('users','Api/UserController@index');
 * Route::get('user/{id}','Api/UserController@show');
 * Route::delete('user/{id}','Api/UserController@destroy');
 * Route::put('user','Api/UserController@store');
 * Route::post('user','Api/UserController@store');
 */
