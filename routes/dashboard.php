<?php

// Landing Page (excludes verify middleware)
Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

// User Settings
Route::get('/settings', 'User\UserController@getSettings');
Route::post('/settings', 'User\UserController@postSettings');

// Animals
Route::group(['prefix' => 'animals'], function() {
    Route::get('/', 'Animal\AnimalController@index');
    Route::post('/fetch-all', 'Animal\AnimalController@postFetchAll');
    Route::post('/fetch', 'Animal\AnimalController@postFetch');
    Route::post('/create', 'Animal\AnimalController@postCreate');
});