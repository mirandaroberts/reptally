<?php

Route::post(
    'braintree/webhook',
    '\Laravel\Cashier\Http\Controllers\App\WebhookController@handleWebhook'
);