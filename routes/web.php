<?php

// Landing Page
Route::get('/', 'App\HomeController@index')->name('index');

// TOS / Privacy
Route::get('/tos', 'App\HomeController@tos');
Route::get('/privacy', 'App\HomeController@privacy');

// Calculator Routes
Route::get('calculator/{species?}/{parent1?}/{parent2?}', 'App\CalculatorController@index')->name('calculator');

// Article Routes
Route::get('articles', 'App\ArticleController@index')->name('articles');

// Almanac Routes
Route::get('almanac', 'App\AlmanacController@index')->name('almanac');
Route::get('almanac/{query}', 'App\AlmanacController@page')->name('almanac.page');
