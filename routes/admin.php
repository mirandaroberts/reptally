<?php

// AJAX calls
Route::post('/dropdowns', 'AdminController@postDropdown');

// Admin Routes
Route::group(['prefix' => 'admin', 'middleware' => 'verified'], function() {
    // Index
    Route::get('/', 'AdminController@index')->name('admin');

    // Animal Routes
    Route::group(['prefix' => 'animals'], function () {
        Route::get('/', 'Animal\AnimalController@index')->name('admin.animals');
        Route::post('/fetch-all', 'Animal\AnimalController@postFetch');
        Route::post('/fetch', 'Animal\AnimalController@postFetchOne');
        Route::post('/create', 'Animal\AnimalController@postCreate');
        Route::post('/update', 'Animal\AnimalController@postUpdate');
        Route::post('/delete', 'Animal\AnimalController@postDelete');

        // Animal Species
        Route::group(['prefix' => 'species', 'middleware' => 'role:administrator|moderator'], function () {
            Route::get('/', 'Animal\AnimalSpeciesController@index')->name('admin.animals.species');
            Route::get('/fetch', 'Animal\AnimalSpeciesController@fetch')->name('admin.animals.species.fetch');
            Route::get('/create', 'Animal\AnimalSpeciesController@getCreate')->name('admin.animals.species.create');
            Route::post('/create', 'Animal\AnimalSpeciesController@postCreate');
            Route::get('/{id}', 'Animal\AnimalSpeciesController@getEdit')->name('admin.animals.species.edit');
            Route::post('/{id}', 'Animal\AnimalSpeciesController@postEdit');
            Route::get('/{id}/morphs', 'Animal\AnimalSpeciesController@getMorphs')->name('admin.animals.species.morphs');
            Route::post('/{id}/morphs', 'Animal\AnimalSpeciesController@postMorphs');


            // Animal Species: Morphs
            Route::group(['prefix' => 'morphs', 'middleware' => 'role:administrator|moderator'], function () {
                Route::post('/create', 'Animal\AnimalMorphController@postCreateMorph');
            });

            // Animal Species: Subspecies
            Route::group(['prefix' => 'subspecies', 'middleware' => 'role:administrator|moderator'], function () {
                Route::post('/create', 'Animal\AnimalSubspeciesController@postCreateSubspecies');
            });

            // Animal Species: Localities
            Route::group(['prefix' => 'localities', 'middleware' => 'role:administrator|moderator'], function () {
                Route::post('/create', 'Animal\AnimalLocalityController@postCreateLocality');
            });

            // Animal Species: Morph Combos
            Route::group(['prefix' => 'combos', 'middleware' => 'role:administrator|moderator'], function () {
                Route::post('/create', 'Animal\AnimalMorphComboController@postCreateMorphCombo');
            });

            // Animal Species: Morph Complexes
            Route::group(['prefix' => 'complexes', 'middleware' => 'role:administrator|moderator'], function () {
                Route::post('/create', 'Animal\AnimalMorphComplexController@postCreateMorphComplex');
            });
        });
    });

    // User Routes
    Route::group(['prefix' => 'users', 'middleware' => 'role:administrator'], function() {
        Route::get('/', 'User\UserController@index')->name('admin.users');
        Route::post('/fetch-all', 'User\UserController@postFetchAll');
        Route::post('/fetch', 'User\UserController@postFetch');
        Route::post('/update', 'User\UserController@postUpdate');

        // User Roles
        Route::group(['prefix' => 'roles'], function () {
            Route::get('/', 'User\UserRoleController@index')->name('admin.user.roles');
            Route::get('/create', 'User\UserRoleController@getCreate')->name('admin.user.roles.create');
            Route::post('/create', 'User\UserRoleController@postCreate');
        });

        // User Plans
        Route::group(['prefix' => 'plans'], function () {
            Route::get('/', 'User\UserPlanController@index')->name('admin.user.plans');
            Route::get('/create', 'User\UserPlanController@getCreate')->name('admin.user.plans.create');
            Route::post('/create', 'User\UserPlanController@postCreate');
        });

        // Invite Codes
        Route::group(['prefix' => 'codes'], function () {
            Route::get('/', 'User\UserCodeController@index')->name('admin.user.codes');
            Route::post('/fetch', 'User\UserCodeController@postFetch');
            Route::post('/create', 'User\UserCodeController@postCreate');
            Route::post('/update', 'User\UserCodeController@postUpdate');
            Route::post('/delete', 'User\UserCodeController@postDelete');
        });
    });
});
