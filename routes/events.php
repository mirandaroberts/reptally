<?php

Route::group(['prefix' => 'events'], function () {
    Route::get('/', 'EventController@getCalendar')->name('calendar');
    Route::get('/create', 'EventController@getCreate')->middleware('auth');
    Route::post('/create', 'EventController@postCreate')->middleware('auth');
});